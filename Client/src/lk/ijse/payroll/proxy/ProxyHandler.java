package lk.ijse.payroll.proxy;

import lk.ijse.payroll.service.ServiceFactory;
import lk.ijse.payroll.service.SuperService;
import lk.ijse.payroll.service.custom.AccountantService;
import lk.ijse.payroll.service.custom.AdminService;

import java.rmi.Naming;

public class ProxyHandler implements ServiceFactory {
    private static AdminService adminService;
    private static AccountantService accountantService ;
    private static ProxyHandler proxyHandler;

    public ProxyHandler() throws Exception{
        ServiceFactory serviceFactory = (ServiceFactory) Naming.lookup("rmi://localhost:5050/MainServer");
        adminService=serviceFactory.getService(ServiceTypes.Admin);
        accountantService=serviceFactory.getService(ServiceTypes.Accountant);
    }

    public static ProxyHandler getInstance() throws Exception {
        if (proxyHandler == null) {
            proxyHandler = new ProxyHandler();
        }
        return proxyHandler;
    }

    @Override
    public <T extends SuperService> T getService(ServiceTypes types) throws Exception {
        switch (types) {
            case Admin:
                return (T) adminService;
            case Accountant:
                return (T) accountantService;

            default:
                return null;

        }
    }
}
