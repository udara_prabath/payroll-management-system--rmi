package lk.ijse.payroll.main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    private static Stage loginStage;
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root, 725, 539));
        primaryStage.show();
        loginStage=primaryStage;
    }


    public static void main(String[] args) {
        launch(args);
    }

    public static Stage getLoginStage() {
        return loginStage;
    }
}
