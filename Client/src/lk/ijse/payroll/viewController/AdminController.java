package lk.ijse.payroll.viewController;


import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.enums.ReportTypes;
import lk.ijse.payroll.observer.Observer;
import lk.ijse.payroll.proxy.ProxyHandler;
import lk.ijse.payroll.service.ServiceFactory;
import lk.ijse.payroll.service.custom.AccountantService;
import lk.ijse.payroll.service.custom.AdminService;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AdminController implements Initializable,Observer {
    //  EmployeeDTO
    @FXML
    public JFXComboBox employeeReview_designation;
    @FXML
    public JFXComboBox employeeReview_EmpID;
    @FXML
    public TableView<CustomEmployeeDTO> employeeReview_employeeTable;
    @FXML
    public JFXTextField employeeReview_rowCount;

    @FXML
    public JFXTextField newEmployee_empName;
    @FXML
    public JFXTextField newEmployee_address;
    @FXML
    public JFXTextField newEmployee_nic;
    @FXML
    public JFXTextField newEmployee_telephone;
    @FXML
    public JFXTextField newEmployee_DOB;
    @FXML
    public JFXDatePicker newEmployee_joinDate;



    @FXML
    public JFXComboBox newEmployee_designation;
    @FXML
    public JFXComboBox newEmployee_BasicSalary;

    @FXML
    public JFXRadioButton newEmployee_male;
    @FXML
    public JFXRadioButton newEmployee_female;
    @FXML
    public ToggleGroup Gender;


    @FXML
    public JFXComboBox updateEmployee_BasicSalary;
    @FXML
    public JFXComboBox updateEmployee_designation;
    @FXML
    public JFXComboBox updateEmployee_EmpID;
    @FXML
    public JFXTextField updateEmployee_EmpName;
    @FXML
    public JFXTextField updateEmployee_address;
    @FXML
    public JFXTextField updateEmployee_telephone;
    @FXML
    public JFXTextField updateEmployee_nic;
    @FXML
    public JFXTextField updateEmployee_DOB;
    @FXML
    public JFXDatePicker updateEmployee_joinDate;
    @FXML
    public JFXRadioButton updateEmployee_male;
    @FXML
    public JFXRadioButton updateEmployee_female;
    @FXML
    public ToggleGroup updateGender;

    @FXML
    public JFXComboBox employeeSummery_EmpID;
    @FXML
    public JFXTextField employeeSummery_EmpName;
    
    //LOan & Advances
    @FXML
    public JFXComboBox allLoan_loanName;
    @FXML
    public JFXComboBox allLoan_EmpID;
    @FXML
    public JFXTextField allLoans_rowCount;
    @FXML
    public TableView<CustomLoanDTO> allLoan_loanTable;

    @FXML
    public JFXComboBox allAdvance_EmpID;
    @FXML
    public JFXDatePicker allAdvance_date;
    @FXML
    public JFXTextField allAdvances_rowCount;
    @FXML
    public TableView<CustomAdvanceDTO> allAdvances_advanceTable;


    @FXML
    public JFXComboBox newLoan_loanName;
    @FXML
    public JFXTextField newLoan_amount;
    @FXML
    public JFXDatePicker newLoan_date;
    @FXML
    public JFXComboBox newLoan__EmpID;
    @FXML
    public JFXTextField newLoan__EmpName;
    @FXML
    public JFXTextField newLoan_designation;

    @FXML
    public JFXComboBox newAdvance_EmpID;
    @FXML
    public JFXTextField newAdvance_EmpName;
    @FXML
    public JFXTextField newAdvance_designation;
    @FXML
    public JFXTextField newAdvance_amount;
    @FXML
    public JFXDatePicker newAdvance_date;

    //payroll
    @FXML
    public JFXComboBox payroll_EmpID;
    @FXML
    public JFXTextField payroll_EmpName;
    @FXML
    public JFXTextField payroll_Designation;
    @FXML
    public JFXTextField payrollDate;
    @FXML
    public TableView<AttendanceDTO> payroll_table;
    @FXML
    public JFXTextField p_work;
    @FXML
    public JFXTextField p_dot;
    @FXML
    public JFXTextField p_not;
    @FXML
    public JFXTextField p_bonusName;
    @FXML
    public JFXTextField p_bonusAmount;
    @FXML
    public JFXTextField p_adv;
    @FXML
    public JFXTextField p_loanName;
    @FXML
    public JFXTextField p_loanAmount;
    @FXML
    public JFXTextField e12;
    @FXML
    public JFXTextField e8;
    @FXML
    public JFXTextField e3;
    @FXML
    public JFXTextField pleave;
    @FXML
    public JFXTextField pshort;
    @FXML
    public JFXTextField gross;
    @FXML
    public JFXTextField deduc;
    @FXML
    public JFXTextField net;

    //payrollReview
    @FXML
    public JFXComboBox payrollReview_EmpID;
    @FXML
    public TableView<PayrollDTO> pr_table;
    @FXML
    public JFXDatePicker pr_date;

    //attendance
    @FXML
    public JFXButton saveAttendance;
    @FXML
    public JFXButton saveAttendance1;
    @FXML
    public JFXDatePicker inDate;
    @FXML
    public JFXDatePicker outDate;
    @FXML
    public JFXComboBox inEmp;
    @FXML
    public JFXComboBox outEmp;
    @FXML
    public JFXTimePicker inTime;
    @FXML
    public JFXTimePicker outTime;

    //shortLeaves
    @FXML
    public JFXComboBox shourt_empid;
    @FXML
    public JFXDatePicker short_date;
    @FXML
    public JFXTextField short_noOfHours;
    @FXML
    public JFXButton short_save;
    @FXML
    public TableView<ShortLeaveDTO> short_table;
    @FXML
    public JFXTextField short_total;

    //Leaves
    @FXML
    public JFXComboBox leave_empid;
    @FXML
    public JFXDatePicker leave_date;
    @FXML
    public JFXTextField leave_noOfHours;
    @FXML
    public JFXButton leave_save;
    @FXML
    public TableView<LeaveDTO> leave_table;
    @FXML
    public JFXTextField leave_total;

    //Admin Tool
    @FXML
    public TableView<BasicSalaryDTO> bsTable;
    @FXML
    public TableView<DesignationDTO> deTable;
    @FXML
    public TableView<LoanDTO> lTable;
    @FXML
    public JFXTextField sid;
    @FXML
    public JFXTextField samount;
    @FXML
    public JFXTextField did;
    @FXML
    public JFXTextField dname;
    @FXML
    public JFXTextField lid;
    @FXML
    public JFXTextField lname;
    @FXML
    public JFXTextField lamount;
    @FXML
    public JFXComboBox rc1;
    @FXML
    public JFXComboBox rc2;
    @FXML
    public JFXComboBox rc3;
    @FXML
    public JFXComboBox rc4;
    @FXML
    public JFXComboBox rc5;
    @FXML
    public JFXComboBox rc6;
    @FXML
    public JFXComboBox rc7;


    public AdminService adminService;
    private AccountantService accountantService;
    private String res="";
    private String att="";
    private String pay="";
    private String Short="";
    private String Leave="";
    private String Loan="";
    private String Advance="";
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        employeeReview_employeeTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        employeeReview_employeeTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("Name"));
        employeeReview_employeeTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("Designation"));
        employeeReview_employeeTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("nic"));
        employeeReview_employeeTable.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("joindate"));
        employeeReview_employeeTable.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("BasicSalary"));

        allLoan_loanTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        allLoan_loanTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("loan"));
        allLoan_loanTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("date"));
        allLoan_loanTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("install"));
        allLoan_loanTable.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("amount"));

        allAdvances_advanceTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        allAdvances_advanceTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("year"));
        allAdvances_advanceTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("month"));
        allAdvances_advanceTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("amount"));

        leave_table.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        leave_table.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("date"));
        leave_table.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("no"));

        short_table.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        short_table.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("date"));
        short_table.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("no"));

        payroll_table.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        payroll_table.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("date"));
        payroll_table.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("working"));
        payroll_table.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("not"));
        payroll_table.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("dot"));

        pr_table.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("date"));
        pr_table.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
        pr_table.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("Day_worked"));
        pr_table.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("normal_OT"));
        pr_table.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("double_OT"));
        pr_table.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("total_Leaves"));
        pr_table.getColumns().get(6).setCellValueFactory(new PropertyValueFactory<>("short_Leaves"));
        pr_table.getColumns().get(7).setCellValueFactory(new PropertyValueFactory<>("EPF_8"));
        pr_table.getColumns().get(8).setCellValueFactory(new PropertyValueFactory<>("EPF_12"));
        pr_table.getColumns().get(9).setCellValueFactory(new PropertyValueFactory<>("ETF_3"));
        pr_table.getColumns().get(10).setCellValueFactory(new PropertyValueFactory<>("loan"));
        pr_table.getColumns().get(11).setCellValueFactory(new PropertyValueFactory<>("advances"));
        pr_table.getColumns().get(12).setCellValueFactory(new PropertyValueFactory<>("bonus"));
        pr_table.getColumns().get(13).setCellValueFactory(new PropertyValueFactory<>("gross_Salary"));
        pr_table.getColumns().get(14).setCellValueFactory(new PropertyValueFactory<>("total_Deduction"));
        pr_table.getColumns().get(15).setCellValueFactory(new PropertyValueFactory<>("salary"));

        bsTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        bsTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("amount"));


        lTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        lTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
        lTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("amount"));

        deTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        deTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("amount"));



        payrollDate.setText(LocalDate.now()+"");


        try {
            UnicastRemoteObject.exportObject(this, 0);
            adminService=ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.Admin);
            adminService.register(this);
            reload();
        } catch (Exception e) {
            e.printStackTrace();
        }




    }
    public void close(MouseEvent mouseEvent){
        try {
            adminService.unregister(this);
            adminService.release("Short"+Short);
            adminService.release("Leave"+Leave);
            adminService.release("Advance" + Advance);
            adminService.release("Loan" + Loan);
            adminService.release("Emp"+res);
            adminService.release("Att"+att);
            adminService.release("Pay"+pay);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    public void loadAllBasicSalaryAmount() throws Exception {
        ArrayList<String> allBasicSalaryAmount = adminService.getAllBasicSalaryAmount();
        if (allBasicSalaryAmount != null) {
            newEmployee_BasicSalary.setItems(FXCollections.observableArrayList(allBasicSalaryAmount));
            updateEmployee_BasicSalary.setItems(FXCollections.observableArrayList(allBasicSalaryAmount));
        }
    }
    public void loadAllDesignations() throws Exception{

        ArrayList<String> allDesignation = adminService.getAllDesignation();
        if (allDesignation!=null){
            employeeReview_designation.setItems(FXCollections.observableArrayList(allDesignation));
            newEmployee_designation.setItems(FXCollections.observableArrayList(allDesignation));
            updateEmployee_designation.setItems(FXCollections.observableArrayList(allDesignation));
        }
    }
    public void loadAllLoanNames() throws Exception {

        ArrayList<String> allLoanNames = adminService.getAllLoanNames();
        if (allLoanNames!=null){
            allLoan_loanName.setItems(FXCollections.observableArrayList(allLoanNames));
            newLoan_loanName.setItems(FXCollections.observableArrayList(allLoanNames));

        }
    }
    public void loadAllEmployeeIDs() throws Exception {

        ArrayList<String> allEmployeeIDs = adminService.getAllEmployeeIDs();
        if (allEmployeeIDs!=null){
            employeeReview_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            updateEmployee_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            employeeSummery_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            allLoan_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            allAdvance_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            newLoan__EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            newAdvance_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            payrollReview_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            leave_empid.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            shourt_empid.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            rc1.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            rc2.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            rc3.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            rc4.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            rc5.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            rc6.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            rc7.setItems(FXCollections.observableArrayList(allEmployeeIDs));

        }
    }
    public void loadAllEmployeesForEmployeeReview()throws Exception{

        ArrayList<CustomEmployeeDTO> employeeDTOS=adminService.getAllEmployeesForEmployeeReview();

        ObservableList<CustomEmployeeDTO> employee=FXCollections.observableArrayList();
        employee.addAll(employeeDTOS);
        employeeReview_employeeTable.setItems(employee);
        employeeReview_rowCount.setPromptText("Count of Employees : "+employeeReview_employeeTable.getItems().size());
    }
    public void loadAllLoans()throws Exception{

        ArrayList<CustomLoanDTO> loanDTOS=adminService.getAllLoanForAllLoans();

        ObservableList<CustomLoanDTO> loan=FXCollections.observableArrayList();
        loan.addAll(loanDTOS);
        allLoan_loanTable.setItems(loan);
        allLoans_rowCount.setPromptText("Count of Loans : "+allLoan_loanTable.getItems().size());
    }
    public void loadAllAdvances()throws Exception{
        ArrayList<CustomAdvanceDTO> AdvanceDTOS=adminService.getAllAdvancesForAllAdvances();

        ObservableList<CustomAdvanceDTO> adv=FXCollections.observableArrayList();
        adv.addAll(AdvanceDTOS);
        allAdvances_advanceTable.setItems(adv);
        allAdvances_rowCount.setPromptText("Count of Loans : "+allAdvances_advanceTable.getItems().size());

    }
    public void loadAllEmployeeForAttendance()throws Exception{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy.MM.dd");
        LocalDate localDate = LocalDate.now();
        String date=dtf.format(localDate);
//        attendace_date.setValue(LocalDate.parse(date));

        DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime now = LocalDateTime.now();
        String time=dtf1.format(now);


        ArrayList<String> names=adminService.getAllEmployeeNames();
        inEmp.setItems(FXCollections.observableArrayList(names));
    }
    public void loadAllEmployeesForAttendanceOUT()throws Exception{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy.MM.dd");
        LocalDate localDate = LocalDate.now();
        String date=dtf.format(localDate);
//        attendace_date.setValue(LocalDate.parse(date));

        DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime now = LocalDateTime.now();
        String time=dtf1.format(now);

        ArrayList<String> names=adminService.getAllEmployeeNamesForAtt();
        outEmp.setItems(FXCollections.observableArrayList(names));
    }
    public void loadAllEmployeeForPayroll()throws Exception{
        ArrayList<String> names=adminService.getAllEmployeeNamesForpay();
        payroll_EmpID.setItems(FXCollections.observableArrayList(names));
    }
    public void loadAllShortLeaves()throws Exception{
        ArrayList<ShortLeaveDTO> l=adminService.getAllShortLeaves();

        ObservableList<ShortLeaveDTO> a=FXCollections.observableArrayList();
        a.addAll(l);
        short_table.setItems(a);
        short_total.setPromptText("Count of Loans : "+short_table.getItems().size());
    }
    public void loadAllLeaves()throws Exception{
        ArrayList<LeaveDTO> DTOS=adminService.getAllLeaves();

        ObservableList<LeaveDTO> l=FXCollections.observableArrayList();
        l.addAll(DTOS);
        leave_table.setItems(l);
        leave_total.setPromptText("Count of Loans : "+leave_table.getItems().size());
    }
    public void loadAllPayrolls()throws Exception{
        ArrayList<Object[]>allpayroll=adminService.getAllPayroll();
        ArrayList<PayrollDTO>all=new ArrayList<>();
        for (Object[] p:allpayroll) {
            all.add(new PayrollDTO(p[2]+"",p[3]+"",p[4]+"",p[6]+"",p[7]+"",p[8]+"",p[9]+"",p[10]+"",p[11]+"",p[12]+"",p[13]+"",p[14]+"",p[15]+"",p[16]+"",p[17]+"",p[18]+""));
        }
        ObservableList<PayrollDTO>l=FXCollections.observableArrayList();
        l.addAll(all);
        pr_table.setItems(l);
    }

    public void saveAttendanceIN(ActionEvent e){
        if (inEmp.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Employee");
            alert.showAndWait();
        }else if (inDate.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Date");
            alert.showAndWait();
        }else if (inTime.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Time");
            alert.showAndWait();
        }else{
            try {
                boolean mark = false;
                String name=inEmp.getSelectionModel().getSelectedItem().toString();
                String date=inDate.getValue().toString();
                String time=inTime.getValue().toString();
//                System.out.println(name+" "+date+" "+time);
                mark = adminService.markAttendanceIN(name, date, time, "present");

                if (mark) {
                    reload();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Saved");
                    alert.setHeaderText("Attendance marked Successfully");
                    alert.showAndWait();
                    inEmp.getSelectionModel().select(-1);
                    inDate.setValue(null);
                    inTime.setValue(null);
                    adminService.release("Att"+att);
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
    public void saveAttendanceOUT(ActionEvent e){
        if (outEmp.getSelectionModel().getSelectedIndex() == -1) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Employee");
            alert.showAndWait();
        }else if (outDate.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Date");
            alert.showAndWait();
        }else if (outTime.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Time");
            alert.showAndWait();
        }else{
            try {
                String name = outEmp.getSelectionModel().getSelectedItem().toString();
                String date = outDate.getValue().toString();
                String time = outTime.getValue().toString();
                Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(date);

                DateFormat format2 = new SimpleDateFormat("EEEE");
                String day = format2.format(date1);
                boolean save = adminService.markAttendanceOUT(name, date, time,day);
                if (save) {
                    reload();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Saved");
                    alert.setHeaderText("Attendance marked Successfully");
                    alert.showAndWait();
                    outEmp.getSelectionModel().select(-1);
                    outDate.setValue(null);
                    outTime.setValue(null);
                    adminService.release("Att"+att);
                }

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }
    public void empInCombo(ActionEvent actionEvent) throws Exception {
        if (inEmp.getSelectionModel().getSelectedIndex()!=-1){
            adminService.release("Att"+att);
            att=inEmp.getSelectionModel().getSelectedIndex()+"";
            if (adminService.reserve("Att"+att)){

            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Attendance in progress");
                alert.showAndWait();
                reset();
            }
        }
    }
    public void empOutCombo(ActionEvent actionEvent) throws Exception {
        if (outEmp.getSelectionModel().getSelectedIndex()!=-1){
            adminService.release("Att"+att);
            att=outEmp.getSelectionModel().getSelectedIndex()+"";
            if (adminService.reserve("Att"+att)){

            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Attendance in progress");
                alert.showAndWait();
                reset();
            }
        }
    }

    public void NICAction(ActionEvent a){
        if (!(newEmployee_nic.getText().matches("^[0-9]{9}[vVxX]$"))) {

            newEmployee_nic.selectAll();
            newEmployee_nic.requestFocus();
        } else if (newEmployee_nic.getText().isEmpty()) {
            newEmployee_nic.selectAll();
            newEmployee_nic.requestFocus();
        }else {
            DOB d = new DOB(newEmployee_nic.getText());
            String year = d.getYear();
            String sex = d.getSex();
            newEmployee_DOB.setText(year);
            if (sex=="Male"){
                Gender.selectToggle(newEmployee_male);
            }else if (sex=="Female"){
                Gender.selectToggle(newEmployee_female);
            }

        }
    }
    public void NICAction2(ActionEvent a){
        if (!(updateEmployee_nic.getText().matches("^[0-9]{9}[vVxX]$"))) {

            updateEmployee_nic.selectAll();
            updateEmployee_nic.requestFocus();
        } else if (updateEmployee_nic.getText().isEmpty()) {
            updateEmployee_nic.selectAll();
            updateEmployee_nic.requestFocus();
        }else {
            DOB d = new DOB(updateEmployee_nic.getText());
            String year = d.getYear();
            String sex = d.getSex();
            updateEmployee_DOB.setText(year);
            if (sex=="Male"){
                updateGender.selectToggle(updateEmployee_male);
            }else if (sex=="Female"){
                updateGender.selectToggle(updateEmployee_female);
            }

        }
    }
    public void saveEmployee(ActionEvent actionEvent) throws MalformedURLException {

        if(!validateLetters(newEmployee_empName.getText())){
            newEmployee_empName.requestFocus();
            newEmployee_empName.setText("Invalid Input");
            newEmployee_empName.selectAll();
        }else if (newEmployee_empName.getText().isEmpty()){
            newEmployee_empName.requestFocus();
            newEmployee_empName.setText("Please Enter Name");
            newEmployee_empName.selectAll();
        }else if (!(newEmployee_nic.getText().matches("^[0-9]{9}[vVxX]$"))){
            newEmployee_nic.requestFocus();
            newEmployee_nic.setText("Invalid Input");
            newEmployee_nic.selectAll();
        }else if (newEmployee_nic.getText().isEmpty()){
            newEmployee_nic.requestFocus();
            newEmployee_nic.setText("Please Enter NIC");
            newEmployee_nic.selectAll();
        }else if (!validateLetters(newEmployee_address.getText())){
            newEmployee_address.requestFocus();
            newEmployee_address.setText("Invalid Input");
            newEmployee_address.selectAll();
        }else if (newEmployee_address.getText().isEmpty()){
            newEmployee_address.requestFocus();
            newEmployee_address.setText("Please Enter Address");
            newEmployee_address.selectAll();
        }else if (!(newEmployee_telephone.getText().matches("^[0-9]{10}"))){
            newEmployee_telephone.requestFocus();
            newEmployee_telephone.setText("Invalid Input");
            newEmployee_telephone.selectAll();
        }else if (newEmployee_telephone.getText().isEmpty()){
            newEmployee_telephone.requestFocus();
            newEmployee_telephone.setText("Please Enter Telephone no");
            newEmployee_telephone.selectAll();
        }else if (newEmployee_joinDate.getValue()==null){
            newEmployee_joinDate.requestFocus();
        }else if (newEmployee_BasicSalary.getSelectionModel().getSelectedIndex()==-1){
            newEmployee_BasicSalary.requestFocus();
        }else if (newEmployee_designation.getSelectionModel().getSelectedIndex()==-1){
            newEmployee_designation.requestFocus();
        }else{
            String name=newEmployee_empName.getText();
            String address=newEmployee_address.getText();
            String DOB=newEmployee_DOB.getText();
            String nic=newEmployee_nic.getText();
            String tel=newEmployee_telephone.getText();
            String joindate=newEmployee_joinDate.getValue().toString();
            String basicSalary=newEmployee_BasicSalary.getSelectionModel().getSelectedItem().toString();
            String designation=newEmployee_designation.getSelectionModel().getSelectedItem().toString();
            String gender=((RadioButton)Gender.getSelectedToggle()).getText();

            try {

                boolean saved=adminService.saveEmployee(name,address,gender,nic,DOB,tel,joindate,designation,21,basicSalary);
                if (saved){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Saved");
                    alert.setHeaderText("Employee Saved Successfully");
                    alert.showAndWait();
                    reload();

                }else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are Some Problem");
                    alert.showAndWait();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }




    }
    public void updateEmployee(ActionEvent actionEvent)  {
        if(!validateLetters(updateEmployee_EmpName.getText())){
            updateEmployee_EmpName.requestFocus();
            updateEmployee_EmpName.setText("Invalid Input");
            updateEmployee_EmpName.selectAll();
        }else if (updateEmployee_EmpName.getText().isEmpty()){
            updateEmployee_EmpName.requestFocus();
            updateEmployee_EmpName.setText("Please Enter Name");
            updateEmployee_EmpName.selectAll();
        }else if (!(updateEmployee_nic.getText().matches("^[0-9]{9}[vVxX]$"))){
            updateEmployee_nic.requestFocus();
            updateEmployee_nic.setText("Invalid Input");
            updateEmployee_nic.selectAll();
        }else if (updateEmployee_nic.getText().isEmpty()){
            updateEmployee_nic.requestFocus();
            updateEmployee_nic.setText("Please Enter NIC");
            updateEmployee_nic.selectAll();
        }else if (!validateLetters(updateEmployee_address.getText())){
            updateEmployee_address.requestFocus();
            updateEmployee_address.setText("Invalid Input");
            updateEmployee_address.selectAll();
        }else if (updateEmployee_address.getText().isEmpty()){
            updateEmployee_address.requestFocus();
            updateEmployee_address.setText("Please Enter Address");
            updateEmployee_address.selectAll();
        }else if (!(updateEmployee_telephone.getText().matches("^[0-9]{10}"))){
            updateEmployee_telephone.requestFocus();
            updateEmployee_telephone.setText("Invalid Input");
            updateEmployee_telephone.selectAll();
        }else if (updateEmployee_telephone.getText().isEmpty()){
            updateEmployee_telephone.requestFocus();
            updateEmployee_telephone.setText("Please Enter Telephone no");
            updateEmployee_telephone.selectAll();
        }else if (updateEmployee_joinDate.getValue()==null){
            updateEmployee_joinDate.requestFocus();
        }else if (updateEmployee_BasicSalary.getSelectionModel().getSelectedIndex()==-1){
            updateEmployee_BasicSalary.requestFocus();
        }else if (updateEmployee_designation.getSelectionModel().getSelectedIndex()==-1){
            updateEmployee_designation.requestFocus();
        }else {
            String id = updateEmployee_EmpID.getSelectionModel().getSelectedItem().toString();
            String name=updateEmployee_EmpName.getText();
            String address=updateEmployee_address.getText();
            String DOB=updateEmployee_DOB.getText();
            String nic=updateEmployee_nic.getText();
            String tel=updateEmployee_telephone.getText();
            String joindate=updateEmployee_joinDate.getValue().toString();
            String basicSalary=updateEmployee_BasicSalary.getSelectionModel().getSelectedItem().toString();
            String designation=updateEmployee_designation.getSelectionModel().getSelectedItem().toString();
            String gender=((RadioButton)updateGender.getSelectedToggle()).getText();

            try {

                boolean updated=adminService.updateEmployee(id,name,address,gender,nic,DOB,tel,joindate,designation,basicSalary);
                if (updated){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Updated");
                    alert.setHeaderText("EmployeeDTO Updated Successfully");
                    alert.showAndWait();
                    adminService.release("Emp"+res);
                    reload();
                }else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are Some Problem");
                    alert.showAndWait();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void searchEmployee(ActionEvent actionEvent) throws Exception {

        if (updateEmployee_EmpID.getSelectionModel().getSelectedIndex()!=-1){
            adminService.release("Emp"+res);
            res=updateEmployee_EmpID.getSelectionModel().getSelectedIndex()+"";
            if (adminService.reserve("Emp"+res)){
                String id = updateEmployee_EmpID.getSelectionModel().getSelectedItem().toString();
                try {
                    CustomEmployeeDTO emp=adminService.searchEmployee(id);
                    String name=emp.getName();
                    String address=emp.getAddress();
                    String gender=emp.getGender();
                    String nic=emp.getNic();
                    String DOB=emp.getDOB();
                    String tel=emp.getTel();
                    String joindate=emp.getJoindate();
                    String designation=emp.getDesignation();
                    String basicSalary=emp.getBasicSalary();

                    updateEmployee_EmpName.setText(name);
                    updateEmployee_address.setText(address);
                    updateEmployee_DOB.setText(DOB);
                    updateEmployee_nic.setText(nic);
                    updateEmployee_telephone.setText(tel);
                    updateEmployee_designation.setValue(designation);
                    updateEmployee_BasicSalary.setValue(basicSalary);
                    updateEmployee_joinDate.setValue(LocalDate.parse(joindate));
                    if ((gender).equalsIgnoreCase("Male")){
                        Gender.selectToggle(updateEmployee_male);
                    }else if ((gender).equalsIgnoreCase("female")){
                        Gender.selectToggle(updateEmployee_female);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }


        }

    }
    public void DeleteEmployee(ActionEvent evt){
        if (employeeReview_employeeTable.getSelectionModel().getSelectedItem() != null) {
            CustomEmployeeDTO selectedPerson = employeeReview_employeeTable.getSelectionModel().getSelectedItem();
            String id=selectedPerson.getId();
            boolean deleted= false;
            try {
                deleted = adminService.deleteEmployee(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (deleted){
                try {
                    reload();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
    public void searchEmployeeForEmployeeReview(ActionEvent actionEvent) {
        if (employeeReview_EmpID.getSelectionModel().getSelectedIndex()!=-1 && employeeReview_designation.getSelectionModel().getSelectedIndex()!=-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
            employeeReview_EmpID.getSelectionModel().select(-1);
            employeeReview_designation.getSelectionModel().select(-1);
        }else if(employeeReview_EmpID.getSelectionModel().getSelectedIndex()==-1 && employeeReview_designation.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
        }else{
            if (employeeReview_EmpID.getSelectionModel().getSelectedIndex()!=-1){
                String id=employeeReview_EmpID.getSelectionModel().getSelectedItem().toString();

                try {
                    ArrayList<CustomEmployeeDTO> employeeDTOS=adminService.getAllEmployeesForEmployeeReviewByID(id);
                    ObservableList<CustomEmployeeDTO> employee=FXCollections.observableArrayList();
                    employee.addAll(employeeDTOS);
                    employeeReview_employeeTable.setItems(employee);
                    employeeReview_EmpID.getSelectionModel().select(-1);
                    employeeReview_designation.getSelectionModel().select(-1);
                    employeeReview_rowCount.setPromptText("Count of Employees : "+employeeReview_employeeTable.getItems().size());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (employeeReview_designation.getSelectionModel().getSelectedIndex()!=-1){
                String des=employeeReview_designation.getSelectionModel().getSelectedItem().toString();

                try {

                    ArrayList<CustomEmployeeDTO> employeeDTOS=adminService.getAllEmployeesForEmployeeReviewByDesignation(des);
                    ObservableList<CustomEmployeeDTO> employee=FXCollections.observableArrayList();
                    employee.addAll(employeeDTOS);
                    employeeReview_employeeTable.setItems(employee);
                    employeeReview_EmpID.getSelectionModel().select(-1);
                    employeeReview_designation.getSelectionModel().select(-1);
                    employeeReview_rowCount.setPromptText("Count of Employees : "+employeeReview_employeeTable.getItems().size());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }

    }


    public void newLoan_searchEmployee(ActionEvent actionEvent) throws Exception {
        if (newLoan__EmpID.getSelectionModel().getSelectedIndex()!=-1){
            adminService.release("Loan" + Loan);
            Loan=newLoan__EmpID.getSelectionModel().getSelectedIndex()+"";
            if (adminService.reserve("Loan"+Loan)){
                String id=newLoan__EmpID.getSelectionModel().getSelectedItem().toString();
                try {
                    CustomEmployeeDTO emp=adminService.searchEmployee(id);
                    newLoan__EmpName.setText(emp.getName());
                    newLoan_designation.setText(emp.getDesignation());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }

        }
    }
    public void newLoan_searchLoan(ActionEvent actionEvent){
        if (newLoan_loanName.getSelectionModel().getSelectedIndex()!=-1){
            String name=newLoan_loanName.getSelectionModel().getSelectedItem().toString();
            try {
                String amount=adminService.searchLoan(name);
                newLoan_amount.setText(amount);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void newAdvance_searchEmployee(ActionEvent a) throws Exception {
        if (newAdvance_EmpID.getSelectionModel().getSelectedIndex()!=-1){
            adminService.release("Advance" + Advance);
            Advance=newAdvance_EmpID.getSelectionModel().getSelectedIndex()+"";
            if (adminService.reserve("Advance" + Advance)){
                String id=newAdvance_EmpID.getSelectionModel().getSelectedItem().toString();
                try {
                    CustomEmployeeDTO emp=adminService.searchEmployee(id);
                    newAdvance_EmpName.setText(emp.getName());
                    newAdvance_designation.setText(emp.getDesignation());
                    double amount=Double.parseDouble(emp.getBasicSalary());
                    newAdvance_amount.setText((amount/2)+"");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }

        }
    }
    public void saveLoan(ActionEvent a) throws ParseException {

        if (newLoan__EmpID.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("Please Select EmployeeDTO");
            alert.showAndWait();
        }else if(newLoan_loanName.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("Please Select Loan");
            alert.showAndWait();
        }else{
            try {
                boolean available=adminService.checkAvailabilityForLoan(newLoan__EmpID.getSelectionModel().getSelectedItem().toString(),newLoan_date.getValue().toString());
                if (available){
                    String empid=newLoan__EmpID.getSelectionModel().getSelectedItem().toString();
                    String loan = newLoan_loanName.getSelectionModel().getSelectedItem().toString();
                    String date=newLoan_date.getValue().toString();
                    double amount = Double.parseDouble(newLoan_amount.getText());
                    String installment=(amount/12)+"";

                    boolean saved=adminService.saveLoan(empid,loan,date,installment);
                    if (saved){
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Waring");
                        alert.setHeaderText("Loan Saved");
                        alert.showAndWait();
                        adminService.release("Loan" + Loan);
                        reload();
                    }else{
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Waring");
                        alert.setHeaderText("There are some problem");
                        alert.showAndWait();
                    }
                }else{
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Cannot launch Loan for this employee");
                    alert.showAndWait();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    public void saveAdvance(ActionEvent a){
        if (newAdvance_EmpID.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("Please Select EmployeeDTO");
            alert.showAndWait();
        }else{
            String empid=newAdvance_EmpID.getSelectionModel().getSelectedItem().toString();
            String date=newAdvance_date.getValue().toString();
            String amount=newAdvance_amount.getText();
            try {
                boolean available=adminService.checkAvailabilityForAdvance(empid,date,amount);
                if (available){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Advance Saved");
                    alert.showAndWait();
                    adminService.release("Advance" + Advance);
                    reload();
                }else{
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Cannot launch Advance for this employee for this month");
                    alert.showAndWait();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void searchLoanForAllLoans(ActionEvent a){
        if (allLoan_EmpID.getSelectionModel().getSelectedIndex() != -1 && allLoan_loanName.getSelectionModel().getSelectedIndex() != -1) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
            allLoan_EmpID.getSelectionModel().select(-1);
            allLoan_loanName.getSelectionModel().select(-1);

        }else if(allLoan_EmpID.getSelectionModel().getSelectedIndex()==-1 && allLoan_loanName.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
        }else{
            if (allLoan_EmpID.getSelectionModel().getSelectedIndex()!=-1){
                String id=allLoan_EmpID.getSelectionModel().getSelectedItem().toString();

                try {
                    ArrayList<CustomLoanDTO> loanDTOS=adminService.getAllLoanForAllLoansByID(id);
                    ObservableList<CustomLoanDTO> loan=FXCollections.observableArrayList();
                    loan.addAll(loanDTOS);
                    allLoan_loanTable.setItems(loan);
                    allLoan_EmpID.getSelectionModel().select(-1);
                    allLoan_loanName.getSelectionModel().select(-1);
                    allLoans_rowCount.setPromptText("Count of Employees : "+allLoan_loanTable.getItems().size());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (allLoan_loanName.getSelectionModel().getSelectedIndex()!=-1){
                String Name=allLoan_loanName.getSelectionModel().getSelectedItem().toString();

                try {

                    ArrayList<CustomLoanDTO> loanDTOS=adminService.getAllLoanForAllLoansByName(Name);
                    ObservableList<CustomLoanDTO> loan=FXCollections.observableArrayList();
                    loan.addAll(loanDTOS);
                    allLoan_loanTable.setItems(loan);
                    allLoan_EmpID.getSelectionModel().select(-1);
                    allLoan_loanName.getSelectionModel().select(-1);
                    allLoans_rowCount.setPromptText("Count of Employees : "+allLoan_loanTable.getItems().size());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void searchAdvanceForAllAdvance(ActionEvent a) {
        if (allAdvance_EmpID.getSelectionModel().getSelectedIndex() != -1 && allAdvance_date.getValue()!=null) {
            String empid=allAdvance_EmpID.getSelectionModel().getSelectedItem().toString();
            String date=allAdvance_date.getValue().toString();
            try {
                ArrayList<CustomAdvanceDTO> AdvanceDTOS = adminService.searchAdvanceByEmpidAndDate(empid, date);
                ObservableList<CustomAdvanceDTO> adv = FXCollections.observableArrayList();
                adv.addAll(AdvanceDTOS);
                allAdvances_advanceTable.setItems(adv);
                allAdvances_rowCount.setPromptText("Count of Loans : " + allAdvances_advanceTable.getItems().size());

            }catch (Exception e){
                e.printStackTrace();
            }
            allAdvance_EmpID.getSelectionModel().select(-1);
            allAdvance_date.setValue(null);

        }else if(allAdvance_EmpID.getSelectionModel().getSelectedIndex()==-1 && allAdvance_date.getValue()!=null){
            String date=allAdvance_date.getValue().toString();
            System.out.println(date);
            try {
                ArrayList<CustomAdvanceDTO> AdvanceDTOS = adminService.searchAdvanceByDate(date);
                ObservableList<CustomAdvanceDTO> adv = FXCollections.observableArrayList();
                adv.addAll(AdvanceDTOS);
                allAdvances_advanceTable.setItems(adv);
                allAdvances_rowCount.setPromptText("Count of Loans : " + allAdvances_advanceTable.getItems().size());

            }catch (Exception e){
                e.printStackTrace();
            }
            allAdvance_EmpID.getSelectionModel().select(-1);
            allAdvance_date.setValue(null);

        }else if(allAdvance_EmpID.getSelectionModel().getSelectedIndex()!=-1 && allAdvance_date.getValue()==null){
            String empid=allAdvance_EmpID.getSelectionModel().getSelectedItem().toString();
            try {
                ArrayList<CustomAdvanceDTO> AdvanceDTOS = adminService.searchAdvanceByEmpID(empid);
                ObservableList<CustomAdvanceDTO> adv = FXCollections.observableArrayList();
                adv.addAll(AdvanceDTOS);
                allAdvances_advanceTable.setItems(adv);
                allAdvances_rowCount.setPromptText("Count of Loans : " + allAdvances_advanceTable.getItems().size());

            }catch (Exception e){
                e.printStackTrace();
            }
            allAdvance_EmpID.getSelectionModel().select(-1);
            allAdvance_date.setValue(null);

        }else if(allAdvance_EmpID.getSelectionModel().getSelectedIndex()==-1 && allAdvance_date.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
        }
    }

    public void shrotAvtion(ActionEvent actionEvent) throws Exception {
        if (shourt_empid.getSelectionModel().getSelectedIndex()!=-1){
            adminService.release("Short"+Short);
            Short=shourt_empid.getSelectionModel().getSelectedIndex()+"";
            if (adminService.reserve(Short)){

            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }
        }
    }
    public void LeaveAction(ActionEvent actionEvent) throws Exception {
        if (leave_empid.getSelectionModel().getSelectedIndex()!=-1){
            adminService.release("Leave"+Leave);
            Leave=leave_empid.getSelectionModel().getSelectedIndex()+"";
            if (adminService.reserve(Leave)){

            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }
        }
    }
    public void saveShortLeave(ActionEvent a){
        if (shourt_empid.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select EmployeeDTO");
            alert.showAndWait();
        }else if((short_noOfHours.getText()).equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select no Of Hours");
            alert.showAndWait();
        }else if(short_date.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select date");
            alert.showAndWait();
        }else if(validateLetters(short_noOfHours.getText())){
            short_noOfHours.requestFocus();
            short_noOfHours.setText("Invalid Input");
            short_noOfHours.selectAll();
        }else {
            try {
                String emp = shourt_empid.getSelectionModel().getSelectedItem().toString();
                String date = short_date.getValue().toString();
                int no = Integer.parseInt(short_noOfHours.getText());
                boolean save = adminService.saveShortLeave(emp, date, no);
                if (save) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Waring");
                    alert.setHeaderText("leave Saved");
                    alert.showAndWait();
                    short_date.setValue(null);
                    short_noOfHours.setText("");
                    shourt_empid.getSelectionModel().select(-1);
                    adminService.release("Short"+Short);
                    reload();
                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are some problem");
                    alert.showAndWait();
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }

        }
    }
    public void saveLeaves(ActionEvent a){
        if (leave_empid.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select EmployeeDTO");
            alert.showAndWait();
        }else if((leave_noOfHours.getText()).equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select no Of Dates");
            alert.showAndWait();
        }else if(leave_date.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select date");
            alert.showAndWait();
        }else if(validateLetters(leave_noOfHours.getText())){
            leave_noOfHours.requestFocus();
            leave_noOfHours.setText("Invalid Input");
            leave_noOfHours.selectAll();
        }else{
            try {
                String emp = leave_empid.getSelectionModel().getSelectedItem().toString();
                String date = leave_date.getValue().toString();
                int no = Integer.parseInt(leave_noOfHours.getText());
                boolean save = adminService.saveEmployeeLeave(emp, date, no);
                if (save) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Leave Saved");
                    alert.showAndWait();
                    leave_date.setValue(null);
                    leave_noOfHours.setText("");
                    leave_empid.getSelectionModel().select(-1);
                    adminService.release("Leave"+Leave);
                    reload();

                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are Some problem");
                    alert.showAndWait();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void getDetails(ActionEvent actionEvent) throws Exception {

        if (payroll_EmpID.getSelectionModel().getSelectedIndex()!=-1){
            adminService.release("Pay"+pay);
            pay=payroll_EmpID.getSelectionModel().getSelectedIndex()+"";
            if (adminService.reserve(pay)){
                try {
                    String empid = payroll_EmpID.getSelectionModel().getSelectedItem().toString();
                    String date=payrollDate.getText();

                    String[] employee = adminService.getEmpNameAndDesignation(empid);
                    ArrayList<Object[]> list=adminService.getAllAttendance(empid,date);
                    ArrayList<AttendanceDTO> dtos=new ArrayList<>();
                    String amount=adminService.getLastMonthAdvance(date,empid);
                    Object[] loan=adminService.getLoanDetails(date,empid);
                    p_adv.setText(amount);
                    p_loanName.setText(loan[0]+"");
                    p_loanAmount.setText(loan[1]+"");
                    pleave.setText(loan[2]+"");
                    pshort.setText(loan[3]+"");
                    if (employee != null) {
                        payroll_EmpName.setText(employee[0]);
                        payroll_Designation.setText(employee[1]);
                        double bas=Double.parseDouble(employee[1]);
                        double a = bas * 12 / 100;
                        double b = bas * 8 / 100;
                        double c = bas * 3 / 100;
                        e12.setText(a+"");
                        e8.setText(b+"");
                        e3.setText(c+"");
                    }
                    if (list!=null){
                        for (Object[] ob: list) {
                            dtos.add(new AttendanceDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+""));
                        }
                        ObservableList<AttendanceDTO> l=FXCollections.observableArrayList();
                        l.addAll(dtos);
                        payroll_table.setItems(l);
                        p_work.setText(payroll_table.getItems().size()+"");
                        int not=0;
                        int dot=0;
                        for (int i = 0; i < payroll_table.getItems().size(); i++) {
                            AttendanceDTO attendanceDTO=payroll_table.getItems().get(i);
                            not+=Integer.parseInt(attendanceDTO.getNot());
                            dot+=Integer.parseInt(attendanceDTO.getDot());
                        }
                        p_not.setText((not*90)+"");
                        p_dot.setText((dot*180)+"");

                        if (payroll_table.getItems().size()>=21){
                            p_bonusName.setText("Attending Bonus");
                            p_bonusAmount.setText("1000.00");
                        }else{
                            p_bonusName.setText("Attending Bonus unavailable");
                            p_bonusAmount.setText("0.00");
                        }
                        int leaves= Integer.parseInt(pleave.getText());
                        int shrt= Integer.parseInt(pshort.getText());
                        double bs= Double.parseDouble(employee[1]);
                        double ep8= Double.parseDouble(e8.getText());
                        double loa= Double.parseDouble(p_loanAmount.getText());
                        double adv= Double.parseDouble(p_adv.getText());
                        double bo= Double.parseDouble(p_bonusAmount.getText());
                        Object[]val=adminService.calculateSalary(not,dot,leaves,shrt,bs,ep8,loa,adv,bo);
                        gross.setText(val[0]+"");
                        deduc.setText(val[1]+"");
                        net.setText(val[2]+"");
                    }



                }catch (Exception e){
                    e.printStackTrace();
                }
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }

        }
    }
    public void savePayroll(ActionEvent actionEvent) throws Exception {
        if (payroll_EmpID.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select Employee");
            alert.showAndWait();
        }else {
            int empid = Integer.parseInt(payroll_EmpID.getSelectionModel().getSelectedItem().toString());
            String date=payrollDate.getText();
            String name=payroll_EmpName.getText();
            double bs= Double.parseDouble(payroll_Designation.getText());
            String work= p_work.getText();
            double not= Double.parseDouble(p_not.getText());
            double dot= Double.parseDouble(p_dot.getText());
            String leaves= pleave.getText();
            String shrt= pshort.getText();
            double ep8= Double.parseDouble(e8.getText());
            double ep12= Double.parseDouble(e12.getText());
            double ep3= Double.parseDouble(e3.getText());
            double loa= Double.parseDouble(p_loanAmount.getText());
            double adv= Double.parseDouble(p_adv.getText());
            double bo= Double.parseDouble(p_bonusAmount.getText());
            double gro= Double.parseDouble(gross.getText());
            double de= Double.parseDouble(deduc.getText());
            double tot= Double.parseDouble(net.getText());

            System.out.println(empid+" "+date+" "+name+" "+bs+" "+work+" "+not+" "+dot+" "+leaves+" "+shrt+" "+ep8+" "+ep12+" "+ep3+" "+loa+" "+adv+" "+bo+" "+gro+" "+de+" "+tot);
            boolean saved=adminService.savePayroll(empid,date,name,bs,work,not,dot,leaves,shrt,ep8,ep12,ep3,loa,adv,bo,gro,de,tot);
            if (saved){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Waring");
                alert.setHeaderText("Saved");
                alert.showAndWait();
                accountantService.release("Pay"+pay);
                reload();
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("There are some problem");
                alert.showAndWait();
            }
        }
    }
    public void searchPayroll(ActionEvent actionEvent)throws Exception{

        if (payrollReview_EmpID.getSelectionModel().getSelectedIndex() != -1 && pr_date.getValue()!=null) {
            String empid=payrollReview_EmpID.getSelectionModel().getSelectedItem().toString();
            String date=pr_date.getValue().toString();
            try {
                ArrayList<Object[]>allpayroll=adminService.searchPayroll(empid,date);
                ArrayList<PayrollDTO>all=new ArrayList<>();
                for (Object[] p:allpayroll) {
                    all.add(new PayrollDTO(p[2]+"",p[3]+"",p[4]+"",p[6]+"",p[7]+"",p[8]+"",p[9]+"",p[10]+"",p[11]+"",p[12]+"",p[13]+"",p[14]+"",p[15]+"",p[16]+"",p[17]+"",p[18]+""));
                }
                ObservableList<PayrollDTO>l=FXCollections.observableArrayList();
                l.addAll(all);
                pr_table.setItems(l);
            }catch (Exception e){
                e.printStackTrace();
            }
            payrollReview_EmpID.getSelectionModel().select(-1);
            pr_date.setValue(null);

        }else if(payrollReview_EmpID.getSelectionModel().getSelectedIndex()==-1 && pr_date.getValue()!=null){
            String date=pr_date.getValue().toString();
            System.out.println(date);
            try {
                ArrayList<Object[]>allpayroll=adminService.searchPayrollbyDate(date);
                ArrayList<PayrollDTO>all=new ArrayList<>();
                for (Object[] p:allpayroll) {
                    all.add(new PayrollDTO(p[2]+"",p[3]+"",p[4]+"",p[6]+"",p[7]+"",p[8]+"",p[9]+"",p[10]+"",p[11]+"",p[12]+"",p[13]+"",p[14]+"",p[15]+"",p[16]+"",p[17]+"",p[18]+""));
                }
                ObservableList<PayrollDTO>l=FXCollections.observableArrayList();
                l.addAll(all);
                pr_table.setItems(l);
            }catch (Exception e){
                e.printStackTrace();
            }
            payrollReview_EmpID.getSelectionModel().select(-1);
            pr_date.setValue(null);

        }else if(payrollReview_EmpID.getSelectionModel().getSelectedIndex()!=-1 && pr_date.getValue()==null){

            try {
                String id=payrollReview_EmpID.getSelectionModel().getSelectedItem().toString();
                ArrayList<Object[]>allpayroll=adminService.searchPayroll(id);
                ArrayList<PayrollDTO>all=new ArrayList<>();
                for (Object[] p:allpayroll) {
                    all.add(new PayrollDTO(p[2]+"",p[3]+"",p[4]+"",p[6]+"",p[7]+"",p[8]+"",p[9]+"",p[10]+"",p[11]+"",p[12]+"",p[13]+"",p[14]+"",p[15]+"",p[16]+"",p[17]+"",p[18]+""));
                }
                ObservableList<PayrollDTO>l=FXCollections.observableArrayList();
                l.addAll(all);
                pr_table.setItems(l);
            }catch (Exception e){
                e.printStackTrace();
            }
            payrollReview_EmpID.getSelectionModel().select(-1);
            pr_date.setValue(null);

        }else if(payrollReview_EmpID.getSelectionModel().getSelectedIndex()==-1 && pr_date.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
        }



    }

    public void loadAllBasicSalary()throws Exception{
        ArrayList<Object[]> get=adminService.getAllBasicSalary();
        ArrayList<BasicSalaryDTO> basic=new ArrayList<>();
        for (Object[] p:get) {
            basic.add(new BasicSalaryDTO(p[0]+"",p[1]+""));
        }

        ObservableList<BasicSalaryDTO> l=FXCollections.observableArrayList();
        l.addAll(basic);
        bsTable.setItems(l);
    }
    public void loadAllDesignation()throws Exception{
        ArrayList<Object[]> get=adminService.getAllDesignatinos();
        ArrayList<DesignationDTO> basic=new ArrayList<>();
        for (Object[] p:get) {
            basic.add(new DesignationDTO(p[0]+"",p[1]+""));
        }
        ObservableList<DesignationDTO> l=FXCollections.observableArrayList();
        l.addAll(basic);
        deTable.setItems(l);
    }
    public void loadAllloans()throws Exception{
        ArrayList<Object[]> get=adminService.getAllLoans();
        ArrayList<LoanDTO> basic=new ArrayList<>();
        for (Object[] p:get) {
            basic.add(new LoanDTO(p[0]+"",p[1]+"",p[2]+""));
        }
        ObservableList<LoanDTO> l=FXCollections.observableArrayList();
        l.addAll(basic);
        lTable.setItems(l);
    }

    public String status=null;
    public void addBasicSalary(ActionEvent actionEvent)throws Exception{
        if (status==null){
            if (!samount.getText().isEmpty()){
                boolean save=adminService.saveBasicSalary(samount.getText());
                if (save){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Basic Salary Saved");
                    alert.showAndWait();
                    reload();
                }else{
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are Some Problem");
                    alert.showAndWait();
                }
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Please Enter Correct Values");
                alert.showAndWait();
                samount.requestFocus();
            }

        }else if (status=="update"){
            boolean update=adminService.updateBasic(sid.getText(),samount.getText());
            if (update){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Waring");
                alert.setHeaderText("Basic Salary Updated");
                alert.showAndWait();
                status=null;
                reload();
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("There are Some Problem");
                alert.showAndWait();
            }
        }
    }
    public void addDesignation(ActionEvent actionEvent)throws Exception{
        if (status==null){
            if (!dname.getText().isEmpty()){
                boolean save=adminService.saveDesignation(dname.getText());
                if (save){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Designation Saved");
                    alert.showAndWait();
                    reload();
                }else{
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are Some Problem");
                    alert.showAndWait();
                }
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Please Enter Correct Values");
                alert.showAndWait();
                dname.requestFocus();
            }

        }else if (status=="update"){
            boolean update=adminService.updateDesignation(did.getText(),dname.getText());
            if (update){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Waring");
                alert.setHeaderText("Designation Updated");
                alert.showAndWait();
                status=null;
                reload();
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("There are Some Problem");
                alert.showAndWait();
            }
        }
    }
    public void addloans(ActionEvent actionEvent)throws Exception{
        if (status==null){
            if (!lname.getText().isEmpty() && !lamount.getText().isEmpty()){
                boolean save=adminService.saveNeLoan(lname.getText(),lamount.getText());
                if (save){
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Loan Saved");
                    alert.showAndWait();
                    reload();
                }else{
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are Some Problem");
                    alert.showAndWait();
                }
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Please Enter Correct Values");
                alert.showAndWait();
                lname.requestFocus();
            }

        }else if (status=="update"){
            boolean update=adminService.updateLoan(lid.getText(),lname.getText(),lamount.getText());
            if (update){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Waring");
                alert.setHeaderText("Loan Updated");
                alert.showAndWait();
                status=null;
                reload();
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("There are Some Problem");
                alert.showAndWait();
            }
        }
    }

    public void deleteBasicSalary(ActionEvent actionEvent)throws Exception{
        if (bsTable.getSelectionModel().getSelectedItem()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("Please Select Item From Table");
            alert.showAndWait();
        }else{
            boolean delete=adminService.deleteBasic(sid.getText());
            if (delete){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Basic Salary Deleted");
                alert.showAndWait();
                reload();
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("There are some problem");
                alert.showAndWait();
            }
        }
    }
    public void deleteDesignation(ActionEvent actionEvent)throws Exception{
        if (deTable.getSelectionModel().getSelectedItem()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("Please Select Item From Table");
            alert.showAndWait();
        }else{
            boolean delete=adminService.deleteDesignatino(did.getText());
            if (delete){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Designation Deleted");
                alert.showAndWait();
                reload();
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("There are some problem");
                alert.showAndWait();
            }
        }
    }
    public void deleteloans(ActionEvent actionEvent)throws Exception{
        if (lTable.getSelectionModel().getSelectedItem()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("Please Select Item From Table");
            alert.showAndWait();
        }else{
            boolean delete=adminService.deleteLoan(lid.getText());
            if (delete){
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Loan Deleted");
                alert.showAndWait();
                reload();
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("There are some problem");
                alert.showAndWait();
            }
        }
    }

    public void bsclick(MouseEvent event){
        if (bsTable.getSelectionModel().getSelectedItem()!=null){
            sid.setText(bsTable.getSelectionModel().getSelectedItem().getId());
            samount.setText(bsTable.getSelectionModel().getSelectedItem().getAmount());
            status="update";
        }
    }
    public void declick(MouseEvent event){
        if (deTable.getSelectionModel().getSelectedItem()!=null){
            did.setText(deTable.getSelectionModel().getSelectedItem().getId());
            dname.setText(deTable.getSelectionModel().getSelectedItem().getAmount());
            status="update";
        }
    }
    public void lclick(MouseEvent event){
        if (lTable.getSelectionModel().getSelectedItem()!=null){
            lid.setText(lTable.getSelectionModel().getSelectedItem().getId());
            lname.setText(lTable.getSelectionModel().getSelectedItem().getName());
            lamount.setText(lTable.getSelectionModel().getSelectedItem().getAmount());
            status="update";
        }
    }

    public void reload() throws Exception {
        loadAllBasicSalaryAmount();
        loadAllDesignations();
        loadAllLoanNames();
        loadAllEmployeeIDs();
        loadAllEmployeesForEmployeeReview();
        loadAllLoans();
        loadAllAdvances();
        loadAllEmployeeForAttendance();
        loadAllEmployeesForAttendanceOUT();
        loadAllShortLeaves();
        loadAllLeaves();
        loadAllEmployeeForPayroll();
        loadAllPayrolls();
        loadAllBasicSalary();
        loadAllDesignation();
        loadAllloans();
        status=null;
        reset();
        adminService.notyfyAllObservers();
        accountantService=ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.Accountant);
        accountantService.notyfyAllObservers();
    }
    public void reset(){

        employeeReview_designation.getSelectionModel().select(-1);
        
        employeeReview_EmpID.getSelectionModel().select(-1);

        employeeReview_rowCount.setText("");


        newEmployee_empName.setText("") ;

        newEmployee_address.setText("") ;

        newEmployee_nic.setText("") ;

        newEmployee_telephone.setText("") ;

        newEmployee_DOB.setText("") ;
        
         newEmployee_joinDate.setValue(null);

        
        newEmployee_designation.getSelectionModel().select(-1);
        
        newEmployee_BasicSalary.getSelectionModel().select(-1);
        
        Gender.selectToggle(null);


        
        updateEmployee_BasicSalary.getSelectionModel().select(-1);
        
        updateEmployee_designation.getSelectionModel().select(-1);
        
        updateEmployee_EmpID.getSelectionModel().select(-1);
        
         updateEmployee_EmpName.setText("");
        
         updateEmployee_address.setText("");
        
         updateEmployee_telephone.setText("");
        
         updateEmployee_nic.setText("");
        
         updateEmployee_DOB.setText("");
        
         updateEmployee_joinDate.setValue(null);
        
        updateGender.selectToggle(null);

        
        employeeSummery_EmpID.getSelectionModel().select(-1);
        
         employeeSummery_EmpName.setText("");

        //LOan & Advances
        
        allLoan_loanName.getSelectionModel().select(-1);
        
        allLoan_EmpID.getSelectionModel().select(-1);
        
         allLoans_rowCount.setText("");

        
        allAdvance_EmpID.getSelectionModel().select(-1);
        
         allAdvance_date.setValue(null);
        
         allAdvances_rowCount.setText("");


        
        newLoan_loanName.getSelectionModel().select(-1);
        
         newLoan_amount.setText("");
        
         newLoan_date.setValue(null);
        
        newLoan__EmpID.getSelectionModel().select(-1);
        
         newLoan__EmpName.setText("");
        
         newLoan_designation.setText("");

        
        newAdvance_EmpID.getSelectionModel().select(-1);
        
         newAdvance_EmpName.setText("");
        
         newAdvance_designation.setText("");
        
         newAdvance_amount.setText("");
        
         newAdvance_date.setValue(null);

        //payroll
        
        payroll_EmpID.getSelectionModel().select(-1);
        
         payroll_EmpName.setText("");
        
         payroll_Designation.setText("");

        payrollDate.setText(LocalDate.now()+"");
        
         p_work.setText("");
        
         p_dot.setText("");
        
         p_not.setText("");
        
         p_bonusName.setText("");
        
         p_bonusAmount.setText("");
        
         p_adv.setText("");
        
         p_loanName.setText("");
        
         p_loanAmount.setText("");
        
         e12.setText("");
        
         e8.setText("");
        
         e3.setText("");
        
         pleave.setText("");
        
         pshort.setText("");
        
         gross.setText("");
        
         deduc.setText("");
        
         net.setText("");

        //payrollReview
        
        payrollReview_EmpID.getSelectionModel().select(-1);
        
         pr_date.setValue(null);

        //attendance
        
        
         inDate.setValue(null);
        
         outDate.setValue(null);
        
        inEmp.getSelectionModel().select(-1);
        
        outEmp.getSelectionModel().select(-1);
        
        inTime.setValue(null);
        
        outTime.setValue(null);

        //shortLeaves
        
        shourt_empid.getSelectionModel().select(-1);
        
         short_date.setValue(null);
        
         short_noOfHours.setText("");
        
         short_total.setText("");

        //Leaves
        
        leave_empid.getSelectionModel().select(-1);
        
         leave_date.setValue(null);
        
         leave_noOfHours.setText("");
        
        
         leave_total.setText("");

        
        
         sid.setText("");
        
         samount.setText("");
        
         did.setText("");
        
         dname.setText("");
        
         lid.setText("");
        
         lname.setText("");
        
         lamount.setText("");

        rc1.getSelectionModel().select(-1);
        rc2.getSelectionModel().select(-1);
        rc3.getSelectionModel().select(-1);
        rc4.getSelectionModel().select(-1);
        rc5.getSelectionModel().select(-1);
        rc6.getSelectionModel().select(-1);
        rc7.getSelectionModel().select(-1);

    }

    public void reLoadAll(ActionEvent actionEvent) {
        try {
            reload();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void update() throws Exception {
        Platform.runLater(()->{
            try {
                loadAllBasicSalaryAmount();
                loadAllDesignations();
                loadAllLoanNames();
                loadAllEmployeeIDs();
                loadAllEmployeesForEmployeeReview();
                loadAllLoans();
                loadAllAdvances();
                loadAllEmployeeForAttendance();
                loadAllEmployeesForAttendanceOUT();
                loadAllShortLeaves();
                loadAllLeaves();
                loadAllEmployeeForPayroll();
                loadAllPayrolls();
                loadAllBasicSalary();
                loadAllDesignation();
                loadAllloans();
                status=null;
                reset();

            }catch (Exception e){
                e.printStackTrace();
            }
        });

        System.out.println("notyfy");
    }


    public void tab(Event event) {
        try {
            reset();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean validateLetters(String txt) {

        String regx = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();

    }

    public void rep1(MouseEvent mouseEvent) {
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.EmpMonthlyPayroll);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void rep2(MouseEvent actionEvent){
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.BestEmp);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public void rep3(MouseEvent actionEvent){
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.EmpLeave);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public void rep4(MouseEvent actionEvent){
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.EmpShort);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    public void rep5(MouseEvent actionEvent){
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.EmpAdv);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public void rep6(MouseEvent actionEvent){
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.EmpLoan);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public void rep7(MouseEvent actionEvent){
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.AllPayroll);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public void rep8(MouseEvent actionEvent){
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.Funds);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public void rep9(MouseEvent actionEvent){
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.Emp);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    public void rep10(MouseEvent actionEvent){
        try {
            JasperPrint jasperPrint = adminService.getReport(ReportTypes.EmpHeadCount);
            JasperViewer.viewReport(jasperPrint, false);
        }  catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void r13(ActionEvent actionEvent){
        if (rc4.getSelectionModel().getSelectedIndex()!=-1){
            int id= (int) rc4.getSelectionModel().getSelectedItem();
            try {
                JasperPrint jp = adminService.getReport(ReportTypes.r13, id);
                if (!jp.getPages().toString().equals("[]")) {
                    JasperViewer.viewReport(jp, false);
                    reset();
                } else {
                    new Alert(Alert.AlertType.ERROR, "Nothing").show();
                }
            }catch (Exception c){
                c.printStackTrace();
            }
        }
    }
    public void r14(ActionEvent actionEvent){
        if (rc1.getSelectionModel().getSelectedIndex()!=-1){
            int id= (int)rc1.getSelectionModel().getSelectedItem();
            try {
                JasperPrint jp = adminService.getReport(ReportTypes.r14, id);
                if (!jp.getPages().toString().equals("[]")) {
                    JasperViewer.viewReport(jp, false);reset();
                } else {
                    new Alert(Alert.AlertType.ERROR, "Nothing").show();
                }
            }catch (Exception c){
                c.printStackTrace();
            }
        }
    }
    public void r15(ActionEvent actionEvent){
        if (rc3.getSelectionModel().getSelectedIndex()!=-1){
            int id= (int)rc3.getSelectionModel().getSelectedItem();
            try {
                JasperPrint jp = adminService.getReport(ReportTypes.r15, id);
                if (!jp.getPages().toString().equals("[]")) {
                    JasperViewer.viewReport(jp, false);reset();
                } else {
                    new Alert(Alert.AlertType.ERROR, "Nothing").show();
                }
            }catch (Exception c){
                c.printStackTrace();
            }
        }
    }
    public void r16(ActionEvent actionEvent){
        if (rc2.getSelectionModel().getSelectedIndex()!=-1){
            int id= (int)rc2.getSelectionModel().getSelectedItem();
            try {
                JasperPrint jp = adminService.getReport(ReportTypes.r16, id);
                if (!jp.getPages().toString().equals("[]")) {
                    JasperViewer.viewReport(jp, false);reset();
                } else {
                    new Alert(Alert.AlertType.ERROR, "Nothing").show();
                }
            }catch (Exception c){
                c.printStackTrace();
            }
        }
    }
    public void r18(ActionEvent actionEvent){
        if (rc6.getSelectionModel().getSelectedIndex()!=-1){
            int id= (int)rc6.getSelectionModel().getSelectedItem();
            try {
                JasperPrint jp = adminService.getReport(ReportTypes.r18, id);
                if (!jp.getPages().toString().equals("[]")) {
                    JasperViewer.viewReport(jp, false);reset();
                } else {
                    new Alert(Alert.AlertType.ERROR, "Nothing").show();
                }
            }catch (Exception c){
                c.printStackTrace();
            }
        }
    }
    public void r19(ActionEvent actionEvent){
        if (rc7.getSelectionModel().getSelectedIndex()!=-1){
            int id= (int)rc7.getSelectionModel().getSelectedItem();
            try {
                JasperPrint jp = adminService.getReport(ReportTypes.r19, id);
                if (!jp.getPages().toString().equals("[]")) {
                    JasperViewer.viewReport(jp, false);reset();
                } else {
                    new Alert(Alert.AlertType.ERROR, "Nothing").show();
                }
            }catch (Exception c){
                c.printStackTrace();
            }
        }
    }
    public void r20(ActionEvent actionEvent){
        if (rc5.getSelectionModel().getSelectedIndex()!=-1){
            int id= (int)rc5.getSelectionModel().getSelectedItem();
            try {
                JasperPrint jp = adminService.getReport(ReportTypes.r20, id);
                if (!jp.getPages().toString().equals("[]")) {
                    JasperViewer.viewReport(jp, false);reset();
                } else {
                    new Alert(Alert.AlertType.ERROR, "Nothing").show();
                }
            }catch (Exception c){
                c.printStackTrace();
            }
        }
    }



}

class DOB {

    String id;
    int month[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public DOB(String id) {
        this.id = id;
    }

    public String getYear() {
        int year = 1900 + Integer.parseInt(id.substring(0, 2));
        String month = setMonth();
        String DOB = year + "." + month;
        return DOB;
    }

    public int getDays() {
        int d = Integer.parseInt(id.substring(2, 5));
        if (d > 500) {
            return (d - 500);
        } else {
            return d;
        }
    }

    public String setMonth() {
        int mo = 0, da = 0;
        int days = getDays();

        for (int i = 0; i < month.length; i++) {
            if (days < month[i]) {
                mo = i + 1;
                da = days;
                break;
            } else {
                days = days - month[i];
            }
        }
        //System.out.println("Month : " + mo + "\nDate : " + da);
        String month = mo + "." + da;
        return month;
    }

    public String getSex() {
        String M = "Male", F = "Female";
        int d = Integer.parseInt(id.substring(2, 5));
        if (d > 500) {
            return F;
        } else {
            return M;
        }
    }

}