package lk.ijse.payroll.viewController;

import javafx.stage.Stage;

import java.util.ArrayList;

public class StageStore {
    private static ArrayList<Stage> stage=new ArrayList<>();
    private static String userType;

    public static ArrayList<Stage> getStage() {
        return stage;
    }

    public static String getUserType() {
        return userType;
    }

    public static void setUserType(String userType2) {
        userType = userType2;
    }

    public static void setStage(Stage stage1) {
        stage.add(stage1);
    }
}
