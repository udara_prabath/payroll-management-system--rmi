package lk.ijse.payroll.viewController;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lk.ijse.payroll.main.Main;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller  implements Initializable {
    @FXML
    public JFXPasswordField password;
    @FXML
    public JFXTextField userName;
    @FXML
    private JFXButton login;
    private static Stage DashboardStage;

    public static Stage getDashboardStage(){
        return DashboardStage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void loginDashBoard(ActionEvent actionEvent) {
        if ("Admin".equalsIgnoreCase(userName.getText()) && "admin".equalsIgnoreCase(password.getText())){
            Parent root = null;
            var primaryStage=new Stage();
            try {
                root = FXMLLoader.load(getClass().getResource("../view/Admin.fxml"));
                primaryStage.setTitle("DashBoard");
                primaryStage.setScene(new Scene(root, 1530, 855));

                primaryStage.initStyle(StageStyle.UNDECORATED);
                primaryStage.show();
                Main.getLoginStage().close();
                DashboardStage=primaryStage;
                StageStore.setUserType("Admin");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if ("User".equalsIgnoreCase(userName.getText()) && "user".equalsIgnoreCase(password.getText())){
            Parent root = null;
            var primaryStage=new Stage();
            try {
                root = FXMLLoader.load(getClass().getResource("../view/Client.fxml"));
                primaryStage.setTitle("DashBoard");
                primaryStage.setScene(new Scene(root, 1530, 855));

                primaryStage.initStyle(StageStyle.UNDECORATED);
                primaryStage.show();
                Main.getLoginStage().close();
                StageStore.setUserType("User");
                DashboardStage=primaryStage;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Your user name are password are incorrect");
            alert.setContentText("Please check and try again");
            alert.showAndWait();
        }




    }




}
