package lk.ijse.payroll.viewController;

import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.observer.Observer;
import lk.ijse.payroll.proxy.ProxyHandler;
import lk.ijse.payroll.service.ServiceFactory;
import lk.ijse.payroll.service.custom.AccountantService;
import lk.ijse.payroll.service.custom.AdminService;


import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClientController implements Initializable,Observer {
    //  EmployeeDTO
    @FXML
    public JFXComboBox employeeReview_designation;
    @FXML
    public JFXComboBox employeeReview_EmpID;
    @FXML
    public TableView<CustomEmployeeDTO> employeeReview_employeeTable;
    @FXML
    public JFXTextField employeeReview_rowCount;

    @FXML
    public JFXTextField newEmployee_empName;
    @FXML
    public JFXTextField newEmployee_address;
    @FXML
    public JFXTextField newEmployee_nic;
    @FXML
    public JFXTextField newEmployee_telephone;
    @FXML
    public JFXTextField newEmployee_DOB;
    @FXML
    public JFXDatePicker newEmployee_joinDate;

    @FXML
    public JFXComboBox newEmployee_designation;
    @FXML
    public JFXComboBox newEmployee_BasicSalary;

    @FXML
    public JFXRadioButton newEmployee_male;
    @FXML
    public JFXRadioButton newEmployee_female;
    @FXML
    public ToggleGroup Gender;


    @FXML
    public JFXComboBox updateEmployee_BasicSalary;
    @FXML
    public JFXComboBox updateEmployee_designation;
    @FXML
    public JFXComboBox updateEmployee_EmpID;
    @FXML
    public JFXTextField updateEmployee_EmpName;
    @FXML
    public JFXTextField updateEmployee_address;
    @FXML
    public JFXTextField updateEmployee_telephone;
    @FXML
    public JFXTextField updateEmployee_nic;
    @FXML
    public JFXTextField updateEmployee_DOB;
    @FXML
    public JFXDatePicker updateEmployee_joinDate;
    @FXML
    public JFXRadioButton updateEmployee_male;
    @FXML
    public JFXRadioButton updateEmployee_female;
    @FXML
    public ToggleGroup updateGender;

    @FXML
    public JFXComboBox employeeSummery_EmpID;
    @FXML
    public JFXTextField employeeSummery_EmpName;

    //LOan & Advances
    @FXML
    public JFXComboBox allLoan_loanName;
    @FXML
    public JFXComboBox allLoan_EmpID;
    @FXML
    public JFXTextField allLoans_rowCount;
    @FXML
    public TableView<CustomLoanDTO> allLoan_loanTable;

    @FXML
    public JFXComboBox allAdvance_EmpID;
    @FXML
    public JFXDatePicker allAdvance_date;
    @FXML
    public JFXTextField allAdvances_rowCount;
    @FXML
    public TableView<CustomAdvanceDTO> allAdvances_advanceTable;


    @FXML
    public JFXComboBox newLoan_loanName;
    @FXML
    public JFXTextField newLoan_amount;
    @FXML
    public JFXDatePicker newLoan_date;
    @FXML
    public JFXComboBox newLoan__EmpID;
    @FXML
    public JFXTextField newLoan__EmpName;
    @FXML
    public JFXTextField newLoan_designation;

    @FXML
    public JFXComboBox newAdvance_EmpID;
    @FXML
    public JFXTextField newAdvance_EmpName;
    @FXML
    public JFXTextField newAdvance_designation;
    @FXML
    public JFXTextField newAdvance_amount;
    @FXML
    public JFXDatePicker newAdvance_date;

    //payroll
    @FXML
    public JFXComboBox payroll_EmpID;
    @FXML
    public JFXTextField payroll_EmpName;
    @FXML
    public JFXTextField payroll_Designation;
    @FXML
    public JFXTextField payrollDate;
    @FXML
    public TableView<AttendanceDTO> payroll_table;
    @FXML
    public JFXTextField p_work;
    @FXML
    public JFXTextField p_dot;
    @FXML
    public JFXTextField p_not;
    @FXML
    public JFXTextField p_bonusName;
    @FXML
    public JFXTextField p_bonusAmount;
    @FXML
    public JFXTextField p_adv;
    @FXML
    public JFXTextField p_loanName;
    @FXML
    public JFXTextField p_loanAmount;
    @FXML
    public JFXTextField e12;
    @FXML
    public JFXTextField e8;
    @FXML
    public JFXTextField e3;
    @FXML
    public JFXTextField pleave;
    @FXML
    public JFXTextField pshort;
    @FXML
    public JFXTextField gross;
    @FXML
    public JFXTextField deduc;
    @FXML
    public JFXTextField net;

    //payrollReview
    @FXML
    public JFXComboBox payrollReview_EmpID;
    @FXML
    public TableView<PayrollDTO> pr_table;
    @FXML
    public JFXDatePicker pr_date;

    //attendance
    @FXML
    public JFXButton saveAttendance;
    @FXML
    public JFXButton saveAttendance1;
    @FXML
    public JFXDatePicker inDate;
    @FXML
    public JFXDatePicker outDate;
    @FXML
    public JFXComboBox inEmp;
    @FXML
    public JFXComboBox outEmp;
    @FXML
    public JFXTimePicker inTime;
    @FXML
    public JFXTimePicker outTime;

    //shortLeaves
    @FXML
    public JFXComboBox shourt_empid;
    @FXML
    public JFXDatePicker short_date;
    @FXML
    public JFXTextField short_noOfHours;
    @FXML
    public JFXButton short_save;
    @FXML
    public TableView<ShortLeaveDTO> short_table;
    @FXML
    public JFXTextField short_total;

    //Leaves
    @FXML
    public JFXComboBox leave_empid;
    @FXML
    public JFXDatePicker leave_date;
    @FXML
    public JFXTextField leave_noOfHours;
    @FXML
    public JFXButton leave_save;
    @FXML
    public TableView<LeaveDTO> leave_table;
    @FXML
    public JFXTextField leave_total;
    private String res="";
    private String att="";
    private String pay="";
    private String Short="";
    private String Leave="";
    private String Loan="";
    private String Advance="";

    private AccountantService accountantService;
    private AdminService adminService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        employeeReview_employeeTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        employeeReview_employeeTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("Name"));
        employeeReview_employeeTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("Designation"));
        employeeReview_employeeTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("nic"));
        employeeReview_employeeTable.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("joindate"));
        employeeReview_employeeTable.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("BasicSalary"));

        allLoan_loanTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        allLoan_loanTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("loan"));
        allLoan_loanTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("date"));
        allLoan_loanTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("install"));
        allLoan_loanTable.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("amount"));

        allAdvances_advanceTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        allAdvances_advanceTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("year"));
        allAdvances_advanceTable.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("month"));
        allAdvances_advanceTable.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("amount"));

        leave_table.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        leave_table.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("date"));
        leave_table.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("no"));

        short_table.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        short_table.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("date"));
        short_table.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("no"));

        payroll_table.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        payroll_table.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("date"));
        payroll_table.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("working"));
        payroll_table.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("not"));
        payroll_table.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("dot"));

        pr_table.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("date"));
        pr_table.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
        pr_table.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("Day_worked"));
        pr_table.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("normal_OT"));
        pr_table.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("double_OT"));
        pr_table.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("total_Leaves"));
        pr_table.getColumns().get(6).setCellValueFactory(new PropertyValueFactory<>("short_Leaves"));
        pr_table.getColumns().get(7).setCellValueFactory(new PropertyValueFactory<>("EPF_8"));
        pr_table.getColumns().get(8).setCellValueFactory(new PropertyValueFactory<>("EPF_12"));
        pr_table.getColumns().get(9).setCellValueFactory(new PropertyValueFactory<>("ETF_3"));
        pr_table.getColumns().get(10).setCellValueFactory(new PropertyValueFactory<>("loan"));
        pr_table.getColumns().get(11).setCellValueFactory(new PropertyValueFactory<>("advances"));
        pr_table.getColumns().get(12).setCellValueFactory(new PropertyValueFactory<>("bonus"));
        pr_table.getColumns().get(13).setCellValueFactory(new PropertyValueFactory<>("gross_Salary"));
        pr_table.getColumns().get(14).setCellValueFactory(new PropertyValueFactory<>("total_Deduction"));
        pr_table.getColumns().get(15).setCellValueFactory(new PropertyValueFactory<>("salary"));


        payrollDate.setText(LocalDate.now()+"");

        try {
            UnicastRemoteObject.exportObject(this, 0);
            accountantService=ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.Accountant);
            accountantService.register(this);
            reload();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void close(MouseEvent mouseEvent){
        try {
            accountantService=ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.Accountant);
            accountantService.unregister(this);
            accountantService.release("Short"+Short);
            accountantService.release("Leave"+Leave);
            accountantService.release("Advance" + Advance);
            accountantService.release("Loan" + Loan);
            accountantService.release("Emp"+res);
            accountantService.release("Att"+att);
            accountantService.release("Pay"+pay);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    public void loadAllBasicSalaryAmount() throws Exception {
        ArrayList<String> allBasicSalaryAmount = accountantService.getAllBasicSalaryAmount();
        if (allBasicSalaryAmount != null) {
            newEmployee_BasicSalary.setItems(FXCollections.observableArrayList(allBasicSalaryAmount));
            updateEmployee_BasicSalary.setItems(FXCollections.observableArrayList(allBasicSalaryAmount));
        }
    }
    public void loadAllDesignations() throws Exception{

        ArrayList<String> allDesignation = accountantService.getAllDesignation();
        if (allDesignation!=null){
            employeeReview_designation.setItems(FXCollections.observableArrayList(allDesignation));
            newEmployee_designation.setItems(FXCollections.observableArrayList(allDesignation));
            updateEmployee_designation.setItems(FXCollections.observableArrayList(allDesignation));
        }
    }
    public void loadAllLoanNames() throws Exception {

        ArrayList<String> allLoanNames = accountantService.getAllLoanNames();
        if (allLoanNames!=null){
            allLoan_loanName.setItems(FXCollections.observableArrayList(allLoanNames));
            newLoan_loanName.setItems(FXCollections.observableArrayList(allLoanNames));

        }
    }
    public void loadAllEmployeeIDs() throws Exception {

        ArrayList<String> allEmployeeIDs = accountantService.getAllEmployeeIDs();
        if (allEmployeeIDs!=null){
            employeeReview_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            updateEmployee_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            employeeSummery_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            allLoan_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            allAdvance_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            newLoan__EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            newAdvance_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            payrollReview_EmpID.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            leave_empid.setItems(FXCollections.observableArrayList(allEmployeeIDs));
            shourt_empid.setItems(FXCollections.observableArrayList(allEmployeeIDs));
        }
    }
    public void loadAllEmployeesForEmployeeReview()throws Exception{

        ArrayList<CustomEmployeeDTO> employeeDTOS=accountantService.getAllEmployeesForEmployeeReview();

        ObservableList<CustomEmployeeDTO> employee=FXCollections.observableArrayList();
        employee.addAll(employeeDTOS);
        employeeReview_employeeTable.setItems(employee);
        employeeReview_rowCount.setPromptText("Count of Employees : "+employeeReview_employeeTable.getItems().size());
    }
    public void loadAllLoans()throws Exception{

        ArrayList<CustomLoanDTO> loanDTOS=accountantService.getAllLoanForAllLoans();

        ObservableList<CustomLoanDTO> loan=FXCollections.observableArrayList();
        loan.addAll(loanDTOS);
        allLoan_loanTable.setItems(loan);
        allLoans_rowCount.setPromptText("Count of Loans : "+allLoan_loanTable.getItems().size());
    }
    public void loadAllAdvances()throws Exception{
        ArrayList<CustomAdvanceDTO> AdvanceDTOS=accountantService.getAllAdvancesForAllAdvances();

        ObservableList<CustomAdvanceDTO> adv=FXCollections.observableArrayList();
        adv.addAll(AdvanceDTOS);
        allAdvances_advanceTable.setItems(adv);
        allAdvances_rowCount.setPromptText("Count of Loans : "+allAdvances_advanceTable.getItems().size());

    }
    public void loadAllEmployeeForAttendance()throws Exception{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy.MM.dd");
        LocalDate localDate = LocalDate.now();
        String date=dtf.format(localDate);
//        attendace_date.setValue(LocalDate.parse(date));

        DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime now = LocalDateTime.now();
        String time=dtf1.format(now);


        ArrayList<String> names=accountantService.getAllEmployeeNames();
        inEmp.setItems(FXCollections.observableArrayList(names));
    }
    public void loadAllEmployeesForAttendanceOUT()throws Exception{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy.MM.dd");
        LocalDate localDate = LocalDate.now();
        String date=dtf.format(localDate);
//        attendace_date.setValue(LocalDate.parse(date));

        DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime now = LocalDateTime.now();
        String time=dtf1.format(now);

        ArrayList<String> names=accountantService.getAllEmployeeNamesForAtt();
        outEmp.setItems(FXCollections.observableArrayList(names));
    }
    public void loadAllEmployeeForPayroll()throws Exception{
        ArrayList<String> names=accountantService.getAllEmployeeNamesForpay();
        payroll_EmpID.setItems(FXCollections.observableArrayList(names));
    }
    public void loadAllShortLeaves()throws Exception{
        ArrayList<ShortLeaveDTO> l=accountantService.getAllShortLeaves();

        ObservableList<ShortLeaveDTO> a=FXCollections.observableArrayList();
        a.addAll(l);
        short_table.setItems(a);
        short_total.setPromptText("Count of Loans : "+short_table.getItems().size());
    }
    public void loadAllLeaves()throws Exception{
        ArrayList<LeaveDTO> DTOS=accountantService.getAllLeaves();

        ObservableList<LeaveDTO> l=FXCollections.observableArrayList();
        l.addAll(DTOS);
        leave_table.setItems(l);
        leave_total.setPromptText("Count of Loans : "+leave_table.getItems().size());
    }
    public void loadAllPayrolls()throws Exception{
        ArrayList<Object[]>allpayroll=accountantService.getAllPayroll();
        ArrayList<PayrollDTO>all=new ArrayList<>();
        for (Object[] p:allpayroll) {
            all.add(new PayrollDTO(p[2]+"",p[3]+"",p[4]+"",p[6]+"",p[7]+"",p[8]+"",p[9]+"",p[10]+"",p[11]+"",p[12]+"",p[13]+"",p[14]+"",p[15]+"",p[16]+"",p[17]+"",p[18]+""));
        }
        ObservableList<PayrollDTO>l=FXCollections.observableArrayList();
        l.addAll(all);
        pr_table.setItems(l);
    }

    public void saveAttendanceIN(ActionEvent e){
        if (inEmp.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Employee");
            alert.showAndWait();
        }else if (inDate.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Date");
            alert.showAndWait();
        }else if (inTime.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Time");
            alert.showAndWait();
        }else{
            try {
                boolean mark = false;
                String name=inEmp.getSelectionModel().getSelectedItem().toString();
                String date=inDate.getValue().toString();
                String time=inTime.getValue().toString();
//                System.out.println(name+" "+date+" "+time);
                mark = accountantService.markAttendanceIN(name, date, time, "present");

                if (mark) {
                    reload();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Saved");
                    alert.setHeaderText("Attendance marked Successfully");
                    alert.showAndWait();
                    inEmp.getSelectionModel().select(-1);
                    inDate.setValue(null);
                    inTime.setValue(null);
                    accountantService.release("Att"+att);
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
    public void saveAttendanceOUT(ActionEvent e){
        if (outEmp.getSelectionModel().getSelectedIndex() == -1) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Employee");
            alert.showAndWait();
        }else if (outDate.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Date");
            alert.showAndWait();
        }else if (outTime.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Please Select Time");
            alert.showAndWait();
        }else{
            try {
                String name = outEmp.getSelectionModel().getSelectedItem().toString();
                String date = outDate.getValue().toString();
                String time = outTime.getValue().toString();
                Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(date);

                DateFormat format2 = new SimpleDateFormat("EEEE");
                String day = format2.format(date1);
                boolean save = accountantService.markAttendanceOUT(name, date, time,day);
                if (save) {
                    reload();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Saved");
                    alert.setHeaderText("Attendance marked Successfully");
                    alert.showAndWait();
                    outEmp.getSelectionModel().select(-1);
                    outDate.setValue(null);
                    outTime.setValue(null);
                    accountantService.release("Att"+att);
                }

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

    }
    public void empInCombo(ActionEvent actionEvent) throws Exception {
        if (inEmp.getSelectionModel().getSelectedIndex()!=-1){
            accountantService.release("Att"+att);
            att=inEmp.getSelectionModel().getSelectedIndex()+"";
            if (accountantService.reserve("Att"+att)){

            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Attendance in progress");
                alert.showAndWait();
                reset();
            }
        }
    }
    public void empOutCombo(ActionEvent actionEvent) throws Exception {
        if (outEmp.getSelectionModel().getSelectedIndex()!=-1){
            accountantService.release("Att"+att);
            att=outEmp.getSelectionModel().getSelectedIndex()+"";
            if (accountantService.reserve("Att"+att)){

            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Attendance in progress");
                alert.showAndWait();
                reset();
            }
        }
    }


    public void NICAction(ActionEvent a){
        if (!(newEmployee_nic.getText().matches("^[0-9]{9}[vVxX]$"))) {

            newEmployee_nic.selectAll();
            newEmployee_nic.requestFocus();
        } else if (newEmployee_nic.getText().isEmpty()) {
            newEmployee_nic.selectAll();
            newEmployee_nic.requestFocus();
        }else {
            DOB d = new DOB(newEmployee_nic.getText());
            String year = d.getYear();
            String sex = d.getSex();
            newEmployee_DOB.setText(year);
            if (sex=="Male"){
                Gender.selectToggle(newEmployee_male);
            }else if (sex=="Female"){
                Gender.selectToggle(newEmployee_female);
            }

        }
    }
    public void NICAction2(ActionEvent a){
        if (!(updateEmployee_nic.getText().matches("^[0-9]{9}[vVxX]$"))) {

            updateEmployee_nic.selectAll();
            updateEmployee_nic.requestFocus();
        } else if (updateEmployee_nic.getText().isEmpty()) {
            updateEmployee_nic.selectAll();
            updateEmployee_nic.requestFocus();
        }else {
            DOB d = new DOB(updateEmployee_nic.getText());
            String year = d.getYear();
            String sex = d.getSex();
            updateEmployee_DOB.setText(year);
            if (sex=="Male"){
                updateGender.selectToggle(updateEmployee_male);
            }else if (sex=="Female"){
                updateGender.selectToggle(updateEmployee_female);
            }

        }
    }
    public void saveEmployee(ActionEvent actionEvent) throws MalformedURLException {

        if(!validateLetters(newEmployee_empName.getText())){
            newEmployee_empName.requestFocus();
            newEmployee_empName.setText("Invalid Input");
            newEmployee_empName.selectAll();
        }else if (newEmployee_empName.getText().isEmpty()){
            newEmployee_empName.requestFocus();
            newEmployee_empName.setText("Please Enter Name");
            newEmployee_empName.selectAll();
        }else if (!(newEmployee_nic.getText().matches("^[0-9]{9}[vVxX]$"))){
            newEmployee_nic.requestFocus();
            newEmployee_nic.setText("Invalid Input");
            newEmployee_nic.selectAll();
        }else if (newEmployee_nic.getText().isEmpty()){
            newEmployee_nic.requestFocus();
            newEmployee_nic.setText("Please Enter NIC");
            newEmployee_nic.selectAll();
        }else if (!validateLetters(newEmployee_address.getText())){
            newEmployee_address.requestFocus();
            newEmployee_address.setText("Invalid Input");
            newEmployee_address.selectAll();
        }else if (newEmployee_address.getText().isEmpty()){
            newEmployee_address.requestFocus();
            newEmployee_address.setText("Please Enter Address");
            newEmployee_address.selectAll();
        }else if (!(newEmployee_telephone.getText().matches("^[0-9]{10}"))){
            newEmployee_telephone.requestFocus();
            newEmployee_telephone.setText("Invalid Input");
            newEmployee_telephone.selectAll();
        }else if (newEmployee_telephone.getText().isEmpty()){
            newEmployee_telephone.requestFocus();
            newEmployee_telephone.setText("Please Enter Telephone no");
            newEmployee_telephone.selectAll();
        }else if (newEmployee_joinDate.getValue()==null){
            newEmployee_joinDate.requestFocus();
        }else if (newEmployee_BasicSalary.getSelectionModel().getSelectedIndex()==-1){
            newEmployee_BasicSalary.requestFocus();
        }else if (newEmployee_designation.getSelectionModel().getSelectedIndex()==-1){
            newEmployee_designation.requestFocus();
        }else{
            String name=newEmployee_empName.getText();
            String address=newEmployee_address.getText();
            String DOB=newEmployee_DOB.getText();
            String nic=newEmployee_nic.getText();
            String tel=newEmployee_telephone.getText();
            String joindate=newEmployee_joinDate.getValue().toString();
            String basicSalary=newEmployee_BasicSalary.getSelectionModel().getSelectedItem().toString();
            String designation=newEmployee_designation.getSelectionModel().getSelectedItem().toString();
            String gender=((RadioButton)Gender.getSelectedToggle()).getText();

            try {

                boolean saved=adminService.saveEmployee(name,address,gender,nic,DOB,tel,joindate,designation,21,basicSalary);
                if (saved){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Saved");
                    alert.setHeaderText("Employee Saved Successfully");
                    alert.showAndWait();
                    reload();

                }else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are Some Problem");
                    alert.showAndWait();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }




    }
    public void updateEmployee(ActionEvent actionEvent)  {
        if(!validateLetters(updateEmployee_EmpName.getText())){
            updateEmployee_EmpName.requestFocus();
            updateEmployee_EmpName.setText("Invalid Input");
            updateEmployee_EmpName.selectAll();
        }else if (updateEmployee_EmpName.getText().isEmpty()){
            updateEmployee_EmpName.requestFocus();
            updateEmployee_EmpName.setText("Please Enter Name");
            updateEmployee_EmpName.selectAll();
        }else if (!(updateEmployee_nic.getText().matches("^[0-9]{9}[vVxX]$"))){
            updateEmployee_nic.requestFocus();
            updateEmployee_nic.setText("Invalid Input");
            updateEmployee_nic.selectAll();
        }else if (updateEmployee_nic.getText().isEmpty()){
            updateEmployee_nic.requestFocus();
            updateEmployee_nic.setText("Please Enter NIC");
            updateEmployee_nic.selectAll();
        }else if (!validateLetters(updateEmployee_address.getText())){
            updateEmployee_address.requestFocus();
            updateEmployee_address.setText("Invalid Input");
            updateEmployee_address.selectAll();
        }else if (updateEmployee_address.getText().isEmpty()){
            updateEmployee_address.requestFocus();
            updateEmployee_address.setText("Please Enter Address");
            updateEmployee_address.selectAll();
        }else if (!(updateEmployee_telephone.getText().matches("^[0-9]{10}"))){
            updateEmployee_telephone.requestFocus();
            updateEmployee_telephone.setText("Invalid Input");
            updateEmployee_telephone.selectAll();
        }else if (updateEmployee_telephone.getText().isEmpty()){
            updateEmployee_telephone.requestFocus();
            updateEmployee_telephone.setText("Please Enter Telephone no");
            updateEmployee_telephone.selectAll();
        }else if (updateEmployee_joinDate.getValue()==null){
            updateEmployee_joinDate.requestFocus();
        }else if (updateEmployee_BasicSalary.getSelectionModel().getSelectedIndex()==-1){
            updateEmployee_BasicSalary.requestFocus();
        }else if (updateEmployee_designation.getSelectionModel().getSelectedIndex()==-1){
            updateEmployee_designation.requestFocus();
        }else {
            String id = updateEmployee_EmpID.getSelectionModel().getSelectedItem().toString();
            String name=updateEmployee_EmpName.getText();
            String address=updateEmployee_address.getText();
            String DOB=updateEmployee_DOB.getText();
            String nic=updateEmployee_nic.getText();
            String tel=updateEmployee_telephone.getText();
            String joindate=updateEmployee_joinDate.getValue().toString();
            String basicSalary=updateEmployee_BasicSalary.getSelectionModel().getSelectedItem().toString();
            String designation=updateEmployee_designation.getSelectionModel().getSelectedItem().toString();
            String gender=((RadioButton)updateGender.getSelectedToggle()).getText();

            try {

                boolean updated=accountantService.updateEmployee(id,name,address,gender,nic,DOB,tel,joindate,designation,basicSalary);
                if (updated){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Updated");
                    alert.setHeaderText("EmployeeDTO Updated Successfully");
                    alert.showAndWait();
                    accountantService.release("Emp"+res);
                    reload();
                }else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are Some Problem");
                    alert.showAndWait();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void searchEmployee(ActionEvent actionEvent) throws Exception {

        if (updateEmployee_EmpID.getSelectionModel().getSelectedIndex()!=-1){
            accountantService.release("Emp"+res);
            res=updateEmployee_EmpID.getSelectionModel().getSelectedIndex()+"";
            if (accountantService.reserve("Emp"+res)){
                String id = updateEmployee_EmpID.getSelectionModel().getSelectedItem().toString();
                try {
                    CustomEmployeeDTO emp=accountantService.searchEmployee(id);
                    String name=emp.getName();
                    String address=emp.getAddress();
                    String gender=emp.getGender();
                    String nic=emp.getNic();
                    String DOB=emp.getDOB();
                    String tel=emp.getTel();
                    String joindate=emp.getJoindate();
                    String designation=emp.getDesignation();
                    String basicSalary=emp.getBasicSalary();

                    updateEmployee_EmpName.setText(name);
                    updateEmployee_address.setText(address);
                    updateEmployee_DOB.setText(DOB);
                    updateEmployee_nic.setText(nic);
                    updateEmployee_telephone.setText(tel);
                    updateEmployee_designation.setValue(designation);
                    updateEmployee_BasicSalary.setValue(basicSalary);
                    updateEmployee_joinDate.setValue(LocalDate.parse(joindate));
                    if ((gender).equalsIgnoreCase("Male")){
                        Gender.selectToggle(updateEmployee_male);
                    }else if ((gender).equalsIgnoreCase("female")){
                        Gender.selectToggle(updateEmployee_female);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }


        }

    }
    public void DeleteEmployee(ActionEvent evt){
        if (employeeReview_employeeTable.getSelectionModel().getSelectedItem() != null) {
            CustomEmployeeDTO selectedPerson = employeeReview_employeeTable.getSelectionModel().getSelectedItem();
            String id=selectedPerson.getId();
            boolean deleted= false;
            try {
                deleted = accountantService.deleteEmployee(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (deleted){
                try {
                    reload();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
    public void searchEmployeeForEmployeeReview(ActionEvent actionEvent) {
        if (employeeReview_EmpID.getSelectionModel().getSelectedIndex()!=-1 && employeeReview_designation.getSelectionModel().getSelectedIndex()!=-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
            employeeReview_EmpID.getSelectionModel().select(-1);
            employeeReview_designation.getSelectionModel().select(-1);
        }else if(employeeReview_EmpID.getSelectionModel().getSelectedIndex()==-1 && employeeReview_designation.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
        }else{
            if (employeeReview_EmpID.getSelectionModel().getSelectedIndex()!=-1){
                String id=employeeReview_EmpID.getSelectionModel().getSelectedItem().toString();

                try {
                    ArrayList<CustomEmployeeDTO> employeeDTOS=accountantService.getAllEmployeesForEmployeeReviewByID(id);
                    ObservableList<CustomEmployeeDTO> employee=FXCollections.observableArrayList();
                    employee.addAll(employeeDTOS);
                    employeeReview_employeeTable.setItems(employee);
                    employeeReview_EmpID.getSelectionModel().select(-1);
                    employeeReview_designation.getSelectionModel().select(-1);
                    employeeReview_rowCount.setPromptText("Count of Employees : "+employeeReview_employeeTable.getItems().size());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (employeeReview_designation.getSelectionModel().getSelectedIndex()!=-1){
                String des=employeeReview_designation.getSelectionModel().getSelectedItem().toString();

                try {

                    ArrayList<CustomEmployeeDTO> employeeDTOS=accountantService.getAllEmployeesForEmployeeReviewByDesignation(des);
                    ObservableList<CustomEmployeeDTO> employee=FXCollections.observableArrayList();
                    employee.addAll(employeeDTOS);
                    employeeReview_employeeTable.setItems(employee);
                    employeeReview_EmpID.getSelectionModel().select(-1);
                    employeeReview_designation.getSelectionModel().select(-1);
                    employeeReview_rowCount.setPromptText("Count of Employees : "+employeeReview_employeeTable.getItems().size());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }

    }


    public void newLoan_searchEmployee(ActionEvent actionEvent) throws Exception {
        if (newLoan__EmpID.getSelectionModel().getSelectedIndex()!=-1){
            accountantService.release("Loan" + Loan);
            Loan=newLoan__EmpID.getSelectionModel().getSelectedIndex()+"";
            if (accountantService.reserve("Loan"+Loan)){
                String id=newLoan__EmpID.getSelectionModel().getSelectedItem().toString();
                try {
                    CustomEmployeeDTO emp=accountantService.searchEmployee(id);
                    newLoan__EmpName.setText(emp.getName());
                    newLoan_designation.setText(emp.getDesignation());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();reset();
            }

        }
    }
    public void newLoan_searchLoan(ActionEvent actionEvent){
        if (newLoan_loanName.getSelectionModel().getSelectedIndex()!=-1){
            String name=newLoan_loanName.getSelectionModel().getSelectedItem().toString();
            try {
                String amount=accountantService.searchLoan(name);
                newLoan_amount.setText(amount);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void newAdvance_searchEmployee(ActionEvent a) throws Exception {
        if (newAdvance_EmpID.getSelectionModel().getSelectedIndex()!=-1){
            accountantService.release("Advance" + Advance);
            Advance=newAdvance_EmpID.getSelectionModel().getSelectedIndex()+"";
            if (accountantService.reserve("Advance" + Advance)){
                String id=newAdvance_EmpID.getSelectionModel().getSelectedItem().toString();
                try {
                    CustomEmployeeDTO emp=accountantService.searchEmployee(id);
                    newAdvance_EmpName.setText(emp.getName());
                    newAdvance_designation.setText(emp.getDesignation());
                    double amount=Double.parseDouble(emp.getBasicSalary());
                    newAdvance_amount.setText((amount/2)+"");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }

        }
    }
    public void saveLoan(ActionEvent a) throws ParseException {

        if (newLoan__EmpID.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("Please Select EmployeeDTO");
            alert.showAndWait();
        }else if(newLoan_loanName.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("Please Select Loan");
            alert.showAndWait();
        }else{
            try {
                boolean available=accountantService.checkAvailabilityForLoan(newLoan__EmpID.getSelectionModel().getSelectedItem().toString(),newLoan_date.getValue().toString());
                if (available){
                    String empid=newLoan__EmpID.getSelectionModel().getSelectedItem().toString();
                    String loan = newLoan_loanName.getSelectionModel().getSelectedItem().toString();
                    String date=newLoan_date.getValue().toString();
                    double amount = Double.parseDouble(newLoan_amount.getText());
                    String installment=(amount/12)+"";

                    boolean saved=accountantService.saveLoan(empid,loan,date,installment);
                    if (saved){
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Waring");
                        alert.setHeaderText("Loan Saved");
                        alert.showAndWait();
                        accountantService.release("Loan" + Loan);
                        reload();
                    }else{
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Waring");
                        alert.setHeaderText("There are some problem");
                        alert.showAndWait();
                    }
                }else{
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Cannot launch Loan for this employee");
                    alert.showAndWait();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    public void saveAdvance(ActionEvent a){
        if (newAdvance_EmpID.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("Please Select EmployeeDTO");
            alert.showAndWait();
        }else{
            String empid=newAdvance_EmpID.getSelectionModel().getSelectedItem().toString();
            String date=newAdvance_date.getValue().toString();
            String amount=newAdvance_amount.getText();
            try {
                boolean available=accountantService.checkAvailabilityForAdvance(empid,date,amount);
                if (available){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Advance Saved");
                    alert.showAndWait();
                    accountantService.release("Advance" + Advance);
                    reload();
                }else{
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Cannot launch Advance for this employee for this month");
                    alert.showAndWait();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void searchLoanForAllLoans(ActionEvent a){
        if (allLoan_EmpID.getSelectionModel().getSelectedIndex() != -1 && allLoan_loanName.getSelectionModel().getSelectedIndex() != -1) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
            allLoan_EmpID.getSelectionModel().select(-1);
            allLoan_loanName.getSelectionModel().select(-1);

        }else if(allLoan_EmpID.getSelectionModel().getSelectedIndex()==-1 && allLoan_loanName.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
        }else{
            if (allLoan_EmpID.getSelectionModel().getSelectedIndex()!=-1){
                String id=allLoan_EmpID.getSelectionModel().getSelectedItem().toString();

                try {
                    ArrayList<CustomLoanDTO> loanDTOS=accountantService.getAllLoanForAllLoansByID(id);
                    ObservableList<CustomLoanDTO> loan=FXCollections.observableArrayList();
                    loan.addAll(loanDTOS);
                    allLoan_loanTable.setItems(loan);
                    allLoan_EmpID.getSelectionModel().select(-1);
                    allLoan_loanName.getSelectionModel().select(-1);
                    allLoans_rowCount.setPromptText("Count of Employees : "+allLoan_loanTable.getItems().size());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            if (allLoan_loanName.getSelectionModel().getSelectedIndex()!=-1){
                String Name=allLoan_loanName.getSelectionModel().getSelectedItem().toString();

                try {

                    ArrayList<CustomLoanDTO> loanDTOS=accountantService.getAllLoanForAllLoansByName(Name);
                    ObservableList<CustomLoanDTO> loan=FXCollections.observableArrayList();
                    loan.addAll(loanDTOS);
                    allLoan_loanTable.setItems(loan);
                    allLoan_EmpID.getSelectionModel().select(-1);
                    allLoan_loanName.getSelectionModel().select(-1);
                    allLoans_rowCount.setPromptText("Count of Employees : "+allLoan_loanTable.getItems().size());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void searchAdvanceForAllAdvance(ActionEvent a) {
        if (allAdvance_EmpID.getSelectionModel().getSelectedIndex() != -1 && allAdvance_date.getValue()!=null) {
            String empid=allAdvance_EmpID.getSelectionModel().getSelectedItem().toString();
            String date=allAdvance_date.getValue().toString();
            try {
                ArrayList<CustomAdvanceDTO> AdvanceDTOS = accountantService.searchAdvanceByEmpidAndDate(empid, date);
                ObservableList<CustomAdvanceDTO> adv = FXCollections.observableArrayList();
                adv.addAll(AdvanceDTOS);
                allAdvances_advanceTable.setItems(adv);
                allAdvances_rowCount.setPromptText("Count of Loans : " + allAdvances_advanceTable.getItems().size());

            }catch (Exception e){
                e.printStackTrace();
            }
            allAdvance_EmpID.getSelectionModel().select(-1);
            allAdvance_date.setValue(null);

        }else if(allAdvance_EmpID.getSelectionModel().getSelectedIndex()==-1 && allAdvance_date.getValue()!=null){
            String date=allAdvance_date.getValue().toString();
            System.out.println(date);
            try {
                ArrayList<CustomAdvanceDTO> AdvanceDTOS = accountantService.searchAdvanceByDate(date);
                ObservableList<CustomAdvanceDTO> adv = FXCollections.observableArrayList();
                adv.addAll(AdvanceDTOS);
                allAdvances_advanceTable.setItems(adv);
                allAdvances_rowCount.setPromptText("Count of Loans : " + allAdvances_advanceTable.getItems().size());

            }catch (Exception e){
                e.printStackTrace();
            }
            allAdvance_EmpID.getSelectionModel().select(-1);
            allAdvance_date.setValue(null);

        }else if(allAdvance_EmpID.getSelectionModel().getSelectedIndex()!=-1 && allAdvance_date.getValue()==null){
            String empid=allAdvance_EmpID.getSelectionModel().getSelectedItem().toString();
            try {
                ArrayList<CustomAdvanceDTO> AdvanceDTOS = accountantService.searchAdvanceByEmpID(empid);
                ObservableList<CustomAdvanceDTO> adv = FXCollections.observableArrayList();
                adv.addAll(AdvanceDTOS);
                allAdvances_advanceTable.setItems(adv);
                allAdvances_rowCount.setPromptText("Count of Loans : " + allAdvances_advanceTable.getItems().size());

            }catch (Exception e){
                e.printStackTrace();
            }
            allAdvance_EmpID.getSelectionModel().select(-1);
            allAdvance_date.setValue(null);

        }else if(allAdvance_EmpID.getSelectionModel().getSelectedIndex()==-1 && allAdvance_date.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
        }
    }

    public void shrotAvtion(ActionEvent actionEvent) throws Exception {
        if (shourt_empid.getSelectionModel().getSelectedIndex()!=-1){
            accountantService.release("Short"+Short);
            Short=shourt_empid.getSelectionModel().getSelectedIndex()+"";
            if (accountantService.reserve(Short)){

            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }
        }
    }
    public void LeaveAction(ActionEvent actionEvent) throws Exception {
        if (leave_empid.getSelectionModel().getSelectedIndex()!=-1){
            accountantService.release("Leave"+Leave);
            Leave=leave_empid.getSelectionModel().getSelectedIndex()+"";
            if (accountantService.reserve(Leave)){

            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }
        }
    }
    public void saveShortLeave(ActionEvent a){
        if (shourt_empid.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select EmployeeDTO");
            alert.showAndWait();
        }else if((short_noOfHours.getText()).equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select no Of Hours");
            alert.showAndWait();
        }else if(short_date.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select date");
            alert.showAndWait();
        }else if(validateLetters(short_noOfHours.getText())){
            short_noOfHours.requestFocus();
            short_noOfHours.setText("Invalid Input");
            short_noOfHours.selectAll();
        }else {
            try {
                String emp = shourt_empid.getSelectionModel().getSelectedItem().toString();
                String date = short_date.getValue().toString();
                int no = Integer.parseInt(short_noOfHours.getText());
                boolean save = adminService.saveShortLeave(emp, date, no);
                if (save) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Waring");
                    alert.setHeaderText("leave Saved");
                    alert.showAndWait();
                    short_date.setValue(null);
                    short_noOfHours.setText("");
                    shourt_empid.getSelectionModel().select(-1);
                    accountantService.release("Short"+Short);
                    reload();
                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are some problem");
                    alert.showAndWait();
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }

        }
    }
    public void saveLeaves(ActionEvent a){
        if (leave_empid.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select EmployeeDTO");
            alert.showAndWait();
        }else if((leave_noOfHours.getText()).equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select no Of Dates");
            alert.showAndWait();
        }else if(leave_date.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select date");
            alert.showAndWait();
        }else if(validateLetters(leave_noOfHours.getText())){
            leave_noOfHours.requestFocus();
            leave_noOfHours.setText("Invalid Input");
            leave_noOfHours.selectAll();
        }else{
            try {
                String emp = leave_empid.getSelectionModel().getSelectedItem().toString();
                String date = leave_date.getValue().toString();
                int no = Integer.parseInt(leave_noOfHours.getText());
                boolean save = adminService.saveEmployeeLeave(emp, date, no);
                if (save) {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Waring");
                    alert.setHeaderText("Leave Saved");
                    alert.showAndWait();
                    leave_date.setValue(null);
                    leave_noOfHours.setText("");
                    leave_empid.getSelectionModel().select(-1);
                    accountantService.release("Leave"+Leave);
                    reload();

                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Waring");
                    alert.setHeaderText("There are Some problem");
                    alert.showAndWait();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void getDetails(ActionEvent actionEvent) throws Exception {

        if (payroll_EmpID.getSelectionModel().getSelectedIndex()!=-1){
            accountantService.release("Pay"+pay);
            pay=payroll_EmpID.getSelectionModel().getSelectedIndex()+"";
            if (accountantService.reserve(pay)){
                try {
                    String empid = payroll_EmpID.getSelectionModel().getSelectedItem().toString();
                    String date=payrollDate.getText();

                    String[] employee = accountantService.getEmpNameAndDesignation(empid);
                    ArrayList<Object[]> list=accountantService.getAllAttendance(empid,date);
                    ArrayList<AttendanceDTO> dtos=new ArrayList<>();
                    String amount=accountantService.getLastMonthAdvance(date,empid);
                    Object[] loan=accountantService.getLoanDetails(date,empid);
                    p_adv.setText(amount);
                    p_loanName.setText(loan[0]+"");
                    p_loanAmount.setText(loan[1]+"");
                    pleave.setText(loan[2]+"");
                    pshort.setText(loan[3]+"");
                    if (employee != null) {
                        payroll_EmpName.setText(employee[0]);
                        payroll_Designation.setText(employee[1]);
                        double bas=Double.parseDouble(employee[1]);
                        double a = bas * 12 / 100;
                        double b = bas * 8 / 100;
                        double c = bas * 3 / 100;
                        e12.setText(a+"");
                        e8.setText(b+"");
                        e3.setText(c+"");
                    }
                    if (list!=null){
                        for (Object[] ob: list) {
                            dtos.add(new AttendanceDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+""));
                        }
                        ObservableList<AttendanceDTO> l=FXCollections.observableArrayList();
                        l.addAll(dtos);
                        payroll_table.setItems(l);
                        p_work.setText(payroll_table.getItems().size()+"");
                        int not=0;
                        int dot=0;
                        for (int i = 0; i < payroll_table.getItems().size(); i++) {
                            AttendanceDTO attendanceDTO=payroll_table.getItems().get(i);
                            not+=Integer.parseInt(attendanceDTO.getNot());
                            dot+=Integer.parseInt(attendanceDTO.getDot());
                        }
                        p_not.setText((not*90)+"");
                        p_dot.setText((dot*180)+"");

                        if (payroll_table.getItems().size()>=21){
                            p_bonusName.setText("Attending Bonus");
                            p_bonusAmount.setText("1000.00");
                        }else{
                            p_bonusName.setText("Attending Bonus unavailable");
                            p_bonusAmount.setText("0.00");
                        }
                        int leaves= Integer.parseInt(pleave.getText());
                        int shrt= Integer.parseInt(pshort.getText());
                        double bs= Double.parseDouble(employee[1]);
                        double ep8= Double.parseDouble(e8.getText());
                        double loa= Double.parseDouble(p_loanAmount.getText());
                        double adv= Double.parseDouble(p_adv.getText());
                        double bo= Double.parseDouble(p_bonusAmount.getText());
                        Object[]val=accountantService.calculateSalary(not,dot,leaves,shrt,bs,ep8,loa,adv,bo);
                        gross.setText(val[0]+"");
                        deduc.setText(val[1]+"");
                        net.setText(val[2]+"");
                    }



                }catch (Exception e){
                    e.printStackTrace();
                }
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("Employee Reserved");
                alert.showAndWait();
                reset();
            }

        }
    }
    public void savePayroll(ActionEvent actionEvent) throws Exception {
        if (payroll_EmpID.getSelectionModel().getSelectedIndex()==-1){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please s10t Employee");
            alert.showAndWait();
        }else {
            int empid = Integer.parseInt(payroll_EmpID.getSelectionModel().getSelectedItem().toString());
            String date=payrollDate.getText();
            String name=payroll_EmpName.getText();
            double bs= Double.parseDouble(payroll_Designation.getText());
            String work= p_work.getText();
            double not= Double.parseDouble(p_not.getText());
            double dot= Double.parseDouble(p_dot.getText());
            String leaves= pleave.getText();
            String shrt= pshort.getText();
            double ep8= Double.parseDouble(e8.getText());
            double ep12= Double.parseDouble(e12.getText());
            double ep3= Double.parseDouble(e3.getText());
            double loa= Double.parseDouble(p_loanAmount.getText());
            double adv= Double.parseDouble(p_adv.getText());
            double bo= Double.parseDouble(p_bonusAmount.getText());
            double gro= Double.parseDouble(gross.getText());
            double de= Double.parseDouble(deduc.getText());
            double tot= Double.parseDouble(net.getText());

            System.out.println(empid+" "+date+" "+name+" "+bs+" "+work+" "+not+" "+dot+" "+leaves+" "+shrt+" "+ep8+" "+ep12+" "+ep3+" "+loa+" "+adv+" "+bo+" "+gro+" "+de+" "+tot);
            boolean saved=accountantService.savePayroll(empid,date,name,bs,work,not,dot,leaves,shrt,ep8,ep12,ep3,loa,adv,bo,gro,de,tot);
            if (saved){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Waring");
                alert.setHeaderText("Saved");
                alert.showAndWait();
                accountantService.release("Pay"+pay);
                reload();
            }else{
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Waring");
                alert.setHeaderText("There are some problem");
                alert.showAndWait();
            }
        }
    }

    public void searchPayroll(ActionEvent actionEvent)throws Exception{

        if (payrollReview_EmpID.getSelectionModel().getSelectedIndex() != -1 && pr_date.getValue()!=null) {
            String empid=payrollReview_EmpID.getSelectionModel().getSelectedItem().toString();
            String date=pr_date.getValue().toString();
            try {
                ArrayList<Object[]>allpayroll=accountantService.searchPayroll(empid,date);
                ArrayList<PayrollDTO>all=new ArrayList<>();
                for (Object[] p:allpayroll) {
                    all.add(new PayrollDTO(p[2]+"",p[3]+"",p[4]+"",p[6]+"",p[7]+"",p[8]+"",p[9]+"",p[10]+"",p[11]+"",p[12]+"",p[13]+"",p[14]+"",p[15]+"",p[16]+"",p[17]+"",p[18]+""));
                }
                ObservableList<PayrollDTO>l=FXCollections.observableArrayList();
                l.addAll(all);
                pr_table.setItems(l);
            }catch (Exception e){
                e.printStackTrace();
            }
            payrollReview_EmpID.getSelectionModel().select(-1);
            pr_date.setValue(null);

        }else if(payrollReview_EmpID.getSelectionModel().getSelectedIndex()==-1 && pr_date.getValue()!=null){
            String date=pr_date.getValue().toString();
            System.out.println(date);
            try {
                ArrayList<Object[]>allpayroll=accountantService.searchPayrollbyDate(date);
                ArrayList<PayrollDTO>all=new ArrayList<>();
                for (Object[] p:allpayroll) {
                    all.add(new PayrollDTO(p[2]+"",p[3]+"",p[4]+"",p[6]+"",p[7]+"",p[8]+"",p[9]+"",p[10]+"",p[11]+"",p[12]+"",p[13]+"",p[14]+"",p[15]+"",p[16]+"",p[17]+"",p[18]+""));
                }
                ObservableList<PayrollDTO>l=FXCollections.observableArrayList();
                l.addAll(all);
                pr_table.setItems(l);
            }catch (Exception e){
                e.printStackTrace();
            }
            payrollReview_EmpID.getSelectionModel().select(-1);
            pr_date.setValue(null);

        }else if(payrollReview_EmpID.getSelectionModel().getSelectedIndex()!=-1 && pr_date.getValue()==null){

            try {
                String id=payrollReview_EmpID.getSelectionModel().getSelectedItem().toString();
                ArrayList<Object[]>allpayroll=accountantService.searchPayroll(id);
                ArrayList<PayrollDTO>all=new ArrayList<>();
                for (Object[] p:allpayroll) {
                    all.add(new PayrollDTO(p[2]+"",p[3]+"",p[4]+"",p[6]+"",p[7]+"",p[8]+"",p[9]+"",p[10]+"",p[11]+"",p[12]+"",p[13]+"",p[14]+"",p[15]+"",p[16]+"",p[17]+"",p[18]+""));
                }
                ObservableList<PayrollDTO>l=FXCollections.observableArrayList();
                l.addAll(all);
                pr_table.setItems(l);
            }catch (Exception e){
                e.printStackTrace();
            }
            payrollReview_EmpID.getSelectionModel().select(-1);
            pr_date.setValue(null);

        }else if(payrollReview_EmpID.getSelectionModel().getSelectedIndex()==-1 && pr_date.getValue()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Waring");
            alert.setHeaderText("please select one Category");
            alert.showAndWait();
        }



    }

    public void reload() throws Exception {
        loadAllBasicSalaryAmount();
        loadAllDesignations();
        loadAllLoanNames();
        loadAllEmployeeIDs();
        loadAllEmployeesForEmployeeReview();
        loadAllLoans();
        loadAllAdvances();
        loadAllEmployeeForAttendance();
        loadAllEmployeesForAttendanceOUT();
        loadAllShortLeaves();
        loadAllLeaves();
        loadAllEmployeeForPayroll();
        loadAllPayrolls();
        reset();
        accountantService.notyfyAllObservers();
        adminService=ProxyHandler.getInstance().getService(ServiceFactory.ServiceTypes.Admin);
        adminService.notyfyAllObservers();

    }

    public void reset(){

        employeeReview_designation.getSelectionModel().select(-1);

        employeeReview_EmpID.getSelectionModel().select(-1);

        employeeReview_rowCount.setText("");


        newEmployee_empName.setText("") ;

        newEmployee_address.setText("") ;

        newEmployee_nic.setText("") ;

        newEmployee_telephone.setText("") ;

        newEmployee_DOB.setText("") ;

        newEmployee_joinDate.setValue(null);


        newEmployee_designation.getSelectionModel().select(-1);

        newEmployee_BasicSalary.getSelectionModel().select(-1);

        Gender.selectToggle(null);



        updateEmployee_BasicSalary.getSelectionModel().select(-1);

        updateEmployee_designation.getSelectionModel().select(-1);

        updateEmployee_EmpID.getSelectionModel().select(-1);

        updateEmployee_EmpName.setText("");

        updateEmployee_address.setText("");

        updateEmployee_telephone.setText("");

        updateEmployee_nic.setText("");

        updateEmployee_DOB.setText("");

        updateEmployee_joinDate.setValue(null);

        updateGender.selectToggle(null);


        employeeSummery_EmpID.getSelectionModel().select(-1);

        employeeSummery_EmpName.setText("");

        //LOan & Advances

        allLoan_loanName.getSelectionModel().select(-1);

        allLoan_EmpID.getSelectionModel().select(-1);

        allLoans_rowCount.setText("");


        allAdvance_EmpID.getSelectionModel().select(-1);

        allAdvance_date.setValue(null);

        allAdvances_rowCount.setText("");



        newLoan_loanName.getSelectionModel().select(-1);

        newLoan_amount.setText("");

        newLoan_date.setValue(null);

        newLoan__EmpID.getSelectionModel().select(-1);

        newLoan__EmpName.setText("");

        newLoan_designation.setText("");


        newAdvance_EmpID.getSelectionModel().select(-1);

        newAdvance_EmpName.setText("");

        newAdvance_designation.setText("");

        newAdvance_amount.setText("");

        newAdvance_date.setValue(null);

        //payroll

        payroll_EmpID.getSelectionModel().select(-1);

        payroll_EmpName.setText("");

        payroll_Designation.setText("");

        payrollDate.setText(LocalDate.now()+"");

        p_work.setText("");

        p_dot.setText("");

        p_not.setText("");

        p_bonusName.setText("");

        p_bonusAmount.setText("");

        p_adv.setText("");

        p_loanName.setText("");

        p_loanAmount.setText("");

        e12.setText("");

        e8.setText("");

        e3.setText("");

        pleave.setText("");

        pshort.setText("");

        gross.setText("");

        deduc.setText("");

        net.setText("");

        //payrollReview

        payrollReview_EmpID.getSelectionModel().select(-1);

        pr_date.setValue(null);

        //attendance


        inDate.setValue(null);

        outDate.setValue(null);

        inEmp.getSelectionModel().select(-1);

        outEmp.getSelectionModel().select(-1);

        inTime.setValue(null);

        outTime.setValue(null);

        //shortLeaves

        shourt_empid.getSelectionModel().select(-1);

        short_date.setValue(null);

        short_noOfHours.setText("");

        short_total.setText("");

        //Leaves

        leave_empid.getSelectionModel().select(-1);

        leave_date.setValue(null);

        leave_noOfHours.setText("");


        leave_total.setText("");




    }

    public void reLoadAll(ActionEvent actionEvent) {
        try {
            reload();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update() throws Exception {
        Platform.runLater(()->{
            try {
                loadAllBasicSalaryAmount();
                loadAllDesignations();
                loadAllLoanNames();
                loadAllEmployeeIDs();
                loadAllEmployeesForEmployeeReview();
                loadAllLoans();
                loadAllAdvances();
                loadAllEmployeeForAttendance();
                loadAllEmployeesForAttendanceOUT();
                loadAllShortLeaves();
                loadAllLeaves();
                loadAllEmployeeForPayroll();
                loadAllPayrolls();
                reset();
            }catch (Exception e){
                e.printStackTrace();
            }
        });

    }
    public void tab(Event event) {
        try {
            reset();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static boolean validateLetters(String txt) {

        String regx = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();

    }
}

