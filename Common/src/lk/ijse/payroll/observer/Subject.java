package lk.ijse.payroll.observer;

public interface Subject {
    boolean register(Observer observer)throws Exception;
    boolean unregister(Observer observer)throws Exception;
    void notyfyAllObservers()throws Exception;
}
