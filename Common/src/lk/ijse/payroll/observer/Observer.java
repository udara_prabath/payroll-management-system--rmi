package lk.ijse.payroll.observer;

import java.rmi.Remote;

public interface Observer extends Remote {
    void update()throws Exception;
}
