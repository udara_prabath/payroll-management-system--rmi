package lk.ijse.payroll.dto;


public class CustomEmployeeDTO implements SuperDTO {
    String id;
    String name;
    String address;
    String gender;
    String nic;
    String DOB;
    String tel;
    String joindate;
    String designation;
    String basicSalary;

    public CustomEmployeeDTO(String id, String name, String designation, String nic, String joindate, String basicSalary) {
        this.id = id;
        this.name = name;
        this.nic = nic;
        this.joindate = joindate;
        this.designation = designation;
        this.basicSalary = basicSalary;
    }

    public CustomEmployeeDTO(String id, String name, String address, String gender, String nic, String DOB, String tel, String joindate, String designation, String basicSalary) {
        this.id=id;
        this.name = name;
        this.address = address;
        this.gender = gender;
        this.nic = nic;
        this.DOB = DOB;
        this.tel = tel;
        this.joindate = joindate;
        this.designation = designation;
        this.basicSalary = basicSalary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getJoindate() {
        return joindate;
    }

    public void setJoindate(String joindate) {
        this.joindate = joindate;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(String basicSalary) {
        this.basicSalary = basicSalary;
    }
}
