package lk.ijse.payroll.dto;



public class CustomLoanDTO implements SuperDTO {
    String name;
    String loan;
    String date;
    String install;
    String amount;

    public CustomLoanDTO(String name, String loan, String date, String install, String amount) {
        this.name = name;
        this.loan = loan;
        this.date = date;
        this.install = install;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoan() {
        return loan;
    }

    public void setLoan(String loan) {
        this.loan = loan;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInstall() {
        return install;
    }

    public void setInstall(String install) {
        this.install = install;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
