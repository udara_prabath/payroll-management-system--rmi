package lk.ijse.payroll.dto;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Serializable;

public class AttendanceDTO implements SuperDTO{
    String date;
    String name;
    String time;
    String st;
    String working;
    String not;
    String dot;

    public AttendanceDTO(String name, String date, String working, String not, String dot) {
        this.date = date;
        this.name = name;
        this.working = working;
        this.not = not;
        this.dot = dot;
    }

    public String getWorking() {
        return working;
    }

    public void setWorking(String working) {
        this.working = working;
    }

    public String getNot() {
        return not;
    }

    public void setNot(String not) {
        this.not = not;
    }

    public String getDot() {
        return dot;
    }

    public void setDot(String dot) {
        this.dot = dot;
    }

    private final StringProperty status = new SimpleStringProperty();

    public AttendanceDTO(String st) {
        this.st = st;
    }

    public AttendanceDTO(String date, String name, String time, String st) {
        this.date = date;
        this.name = name;
        this.time = time;
        this.st = st;
    }



    public AttendanceDTO(String date, String name, String time) {
        this.date = date;
        this.name = name;
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCategory() {
        return status.get();
    }

    public StringProperty categoryProperty() {
        return status;
    }

    public void setCategory(String category) {
        this.status.set(category);
    }
    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

}
