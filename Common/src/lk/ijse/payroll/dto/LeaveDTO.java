package lk.ijse.payroll.dto;

public class LeaveDTO implements SuperDTO {
    String name;
    String date;
    String no;

    public LeaveDTO(String name, String date, String no) {
        this.name = name;
        this.date = date;
        this.no = no;
    }

    public LeaveDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
}
