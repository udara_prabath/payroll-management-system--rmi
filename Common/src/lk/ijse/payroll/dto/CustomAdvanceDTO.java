package lk.ijse.payroll.dto;


public class CustomAdvanceDTO implements SuperDTO {
    String name;
    String year;
    String month;
    String amount;

    public CustomAdvanceDTO(String name, String year, String month, String amount) {
        this.name = name;
        this.year = year;
        this.month = month;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
