package lk.ijse.payroll.dto;

public class BasicSalaryDTO implements SuperDTO{
    String id;
    String amount;

    public BasicSalaryDTO(String id, String amount) {
        this.id = id;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
