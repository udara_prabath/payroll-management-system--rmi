package lk.ijse.payroll.dto;

public class ShortLeaveDTO implements SuperDTO {
    String name;
    String date;
    String no;

    public ShortLeaveDTO(String name, String date, String no) {
        this.name = name;
        this.date = date;
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }
}
