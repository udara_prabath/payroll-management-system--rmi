package lk.ijse.payroll.dto;

public class PayrollDTO implements SuperDTO{

    private int EmpId ;
    private String Date ;
    private String name ;
    private String ba ;
    private String Day_worked;
    private String Normal_OT ;
    private String Double_OT;
    private String Total_Leaves ;
    private String Short_Leaves ;
    private String EPF_8 ;
    private String EPF_12 ;
    private String ETF_3 ;
    private String Loan ;
    private String Advances ;
    private String Bonus ;
    private String Gross_Salary ;
    private String Total_Deduction ;
    private String Salary ;



    public PayrollDTO(  String date, String name, String basic_Salary, String normal_OT, String double_OT, String total_Leaves, String short_Leaves, String EPF_8, String EPF_12, String ETF_3, String loan, String advances, String bonus, String gross_Salary, String total_Deduction, String salary) {
        setDate(date);
        this.setName(name);

        setDay_worked(basic_Salary);
        setNormal_OT(normal_OT);
        setDouble_OT(double_OT);
        setTotal_Leaves(total_Leaves);
        setShort_Leaves(short_Leaves);
        this.setEPF_8(EPF_8);
        this.setEPF_12(EPF_12);
        this.setETF_3(ETF_3);
        setLoan(loan);
        setAdvances(advances);
        setBonus(bonus);
        setGross_Salary(gross_Salary);
        setTotal_Deduction(total_Deduction);
        setSalary(salary);
    }

    public int getEmpId() {
        return EmpId;
    }

    public void setEmpId(int empId) {
        EmpId = empId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getBasic_Salary() {
        return ba;
    }

    public void setBasic_Salary(String basic_Salary) {
        ba = basic_Salary;
    }

    public String getDay_worked() {
        return Day_worked;
    }

    public void setDay_worked(String day_worked) {
        Day_worked = day_worked;
    }

    public String getNormal_OT() {
        return Normal_OT;
    }

    public void setNormal_OT(String normal_OT) {
        Normal_OT = normal_OT;
    }

    public String getDouble_OT() {
        return Double_OT;
    }

    public void setDouble_OT(String double_OT) {
        Double_OT = double_OT;
    }

    public String getTotal_Leaves() {
        return Total_Leaves;
    }

    public void setTotal_Leaves(String total_Leaves) {
        Total_Leaves = total_Leaves;
    }

    public String getShort_Leaves() {
        return Short_Leaves;
    }

    public void setShort_Leaves(String short_Leaves) {
        Short_Leaves = short_Leaves;
    }

    public String getEPF_8() {
        return EPF_8;
    }

    public void setEPF_8(String EPF_8) {
        this.EPF_8 = EPF_8;
    }

    public String getEPF_12() {
        return EPF_12;
    }

    public void setEPF_12(String EPF_12) {
        this.EPF_12 = EPF_12;
    }

    public String getETF_3() {
        return ETF_3;
    }

    public void setETF_3(String ETF_3) {
        this.ETF_3 = ETF_3;
    }

    public String getLoan() {
        return Loan;
    }

    public void setLoan(String loan) {
        Loan = loan;
    }

    public String getAdvances() {
        return Advances;
    }

    public void setAdvances(String advances) {
        Advances = advances;
    }

    public String getBonus() {
        return Bonus;
    }

    public void setBonus(String bonus) {
        Bonus = bonus;
    }

    public String getGross_Salary() {
        return Gross_Salary;
    }

    public void setGross_Salary(String gross_Salary) {
        Gross_Salary = gross_Salary;
    }

    public String getTotal_Deduction() {
        return Total_Deduction;
    }

    public void setTotal_Deduction(String total_Deduction) {
        Total_Deduction = total_Deduction;
    }

    public String getSalary() {
        return Salary;
    }

    public void setSalary(String salary) {
        Salary = salary;
    }
}
