package lk.ijse.payroll.service.custom;

import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.observer.Subject;
import lk.ijse.payroll.reservation.Reservations;
import lk.ijse.payroll.service.SuperService;

import java.util.ArrayList;

public interface AccountantService extends SuperService,Subject,Reservations {
    public ArrayList<String> getAllBasicSalaryAmount()throws Exception;
    public ArrayList<String> getAllDesignation()throws Exception;
    public ArrayList<String> getAllLoanNames()throws Exception;
    public ArrayList<String> getAllEmployeeIDs()throws Exception;
    public ArrayList<String> getAllEmployeeNames() throws Exception;
    public ArrayList<String> getAllEmployeeNamesForAtt() throws Exception;
    public ArrayList<String> getAllEmployeeNamesForpay() throws Exception;

    boolean saveEmployee(String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, int i, String basicSalary)throws Exception;
    boolean updateEmployee(String id,String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, String basicSalary)throws Exception;
    CustomEmployeeDTO searchEmployee(String id)throws Exception;
    boolean deleteEmployee(String id)throws Exception;

    ArrayList<CustomEmployeeDTO> getAllEmployeesForEmployeeReview()throws Exception;
    ArrayList<CustomEmployeeDTO> getAllEmployeesForEmployeeReviewByID(String id)throws Exception;
    ArrayList<CustomEmployeeDTO> getAllEmployeesForEmployeeReviewByDesignation(String des)throws Exception;


    String searchLoan(String name)throws Exception;
    boolean checkAvailabilityForLoan(String empId, String date)throws Exception;
    boolean saveLoan(String empid, String loan, String date, String installment)throws Exception;
    boolean checkAvailabilityForAdvance(String empid, String date, String amount)throws Exception;

    ArrayList<CustomLoanDTO> getAllLoanForAllLoans()throws Exception;
    ArrayList<CustomAdvanceDTO> getAllAdvancesForAllAdvances()throws Exception;
    ArrayList<CustomLoanDTO> getAllLoanForAllLoansByID(String id)throws Exception;
    ArrayList<CustomLoanDTO> getAllLoanForAllLoansByName(String name)throws Exception;

    ArrayList<CustomAdvanceDTO> searchAdvanceByEmpidAndDate(String empid, String date)throws Exception;
    ArrayList<CustomAdvanceDTO> searchAdvanceByDate(String date)throws Exception;
    ArrayList<CustomAdvanceDTO> searchAdvanceByEmpID(String empid)throws Exception;

    boolean markAttendanceIN(String name, String date, String time, String st)throws Exception;
    boolean markAttendanceOUT(String name, String date, String time, String day)throws Exception;

    boolean saveShortLeave(String emp, String date, int no)throws Exception;
    boolean saveEmployeeLeave(String emp, String date, int no)throws Exception;

    ArrayList<ShortLeaveDTO> getAllShortLeaves()throws Exception;
    ArrayList<LeaveDTO> getAllLeaves()throws Exception;

    String[] getEmpNameAndDesignation(String empid)throws Exception;

    ArrayList<Object[]> getAllAttendance(String empid, String date)throws Exception;
    String getLastMonthAdvance(String date, String empid)throws Exception;
    Object[] getLoanDetails(String date, String empid)throws Exception;

    Object[] calculateSalary(int not, int dot, int leaves, int shrt, double bs, double ep8, double loa, double adv, double bo)throws Exception;
    boolean savePayroll(int empid, String date, String name, double bs, String work, double not, double dot, String leaves, String shrt, double ep8, double ep12, double ep3, double loa, double adv, double bo, double gro, double de, double tot)throws Exception;
    ArrayList<Object[]> getAllPayroll()throws Exception;

    ArrayList<Object[]> searchPayroll(String id)throws Exception;
    ArrayList<Object[]> searchPayroll(String id,String date)throws Exception;
    ArrayList<Object[]> searchPayrollbyDate(String id)throws Exception;
}
