package lk.ijse.payroll.service;


import java.rmi.Remote;

public interface ServiceFactory extends Remote {
    public enum ServiceTypes{
        Admin,Accountant;
    }

    public <T extends SuperService>T getService(ServiceTypes types)throws Exception;

}
