package lk.ijse.payroll.enums;

public enum ReportTypes {
    EmpMonthlyPayroll,BestEmp,EmpLeave,EmpShort,EmpAdv,EmpLoan,AllPayroll,Funds,Emp,EmpHeadCount,r13,r14,r15,r16,r18,r19,r20
}
