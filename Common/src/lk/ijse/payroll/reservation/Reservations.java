package lk.ijse.payroll.reservation;

import java.rmi.Remote;

public interface Reservations extends Remote {
    public boolean reserve(Object id)throws Exception;
    public boolean release(Object id)throws Exception;
}
