package lk.ijse.payroll.server;

import lk.ijse.payroll.service.impl.ServiceFactoryImpl;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ServerStart {
    public static void main(String[] args)throws Exception{
        Registry registry= LocateRegistry.createRegistry(5050);
        registry.bind("MainServer", ServiceFactoryImpl.getInstace());
        System.out.println("Started..!");
    }
}
