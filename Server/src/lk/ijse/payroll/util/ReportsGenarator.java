package lk.ijse.payroll.util;

import lk.ijse.payroll.enums.ReportTypes;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.io.File;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ReportsGenarator {
    public Session session;
    public Connection connection;

    public ReportsGenarator() {
        try {
            connection = DBConnection.getInstance().getConnection();
        }catch (Exception a){
            a.printStackTrace();
        }
    }

    public void setSubSession(Session openSession) {
        this.session=openSession;
    }
    public JasperPrint getReport(ReportTypes reportType, Object... param)throws Exception {
        switch (reportType){
            case EmpLoan: return EmpLoan();
            case Emp:return Employee();
            case Funds:return Funds();
            case EmpAdv:return EmpAdvance();
            case BestEmp:return BestEmp();
            case EmpLeave:return empLeave();
            case EmpShort:return empShort();
            case AllPayroll:return AllPayroll();
            case EmpHeadCount:return EmpHeadCount();
            case EmpMonthlyPayroll:return EmpmonthlyPayroll();
            case r13:return r13((int) param[0]);
            case r14:return r14((int) param[0]);
            case r15:return r15((int) param[0]);
            case r16:return r16((int) param[0]);
            case r18:return r18((int) param[0]);
            case r19:return r19((int) param[0]);
            case r20:return r20((int) param[0]);

            default:
                return null;
        }
    }

    private JasperPrint EmpmonthlyPayroll() throws Exception {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/EmpMonthlyPayroll.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;

    }



    private JasperPrint EmpHeadCount()throws Exception  {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/EmpHeadCount.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;
    }

    private JasperPrint EmpAdvance()throws Exception  {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/r2.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;
    }

    private JasperPrint AllPayroll()throws Exception  {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/r3.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;
    }

    private JasperPrint empShort()throws Exception  {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/r4.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;
    }

    private JasperPrint empLeave()throws Exception  {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/r5.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;
    }

    private JasperPrint BestEmp()throws Exception  {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/r6.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;
    }

    private JasperPrint Funds() throws Exception {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/r7.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;
    }

    private JasperPrint Employee()throws Exception  {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/r8.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;
    }

    private JasperPrint EmpLoan()throws Exception  {
        InputStream resourceAsStream = getClass().getResourceAsStream("/lk/ijse/payroll/reports/r9.jasper");
        HashMap hashMap=new HashMap();
        JasperPrint jasperPrint=JasperFillManager.fillReport(resourceAsStream, hashMap, connection);
        return jasperPrint;
    }
    private JasperPrint r13(int empid) throws JRException {
        JasperDesign jd= JRXmlLoader.load("E:\\Clz document\\PayrollFinal\\Server\\src\\lk\\ijse\\payroll\\reports\\r13.jrxml");
        String SQL="select e.empname,ead.year,ead.month,ead.amount from employee e,empadvances ead where e.empid=ead.empid and ead.empid='"+empid+"' ;" ;
        JRDesignQuery jrDesignQuery=new JRDesignQuery();
        jrDesignQuery.setText(SQL);
        jd.setQuery(jrDesignQuery);

        JasperReport jr=JasperCompileManager.compileReport(jd);
        JasperPrint jp=JasperFillManager.fillReport(jr,null,connection);
        return jp;
    }
    private JasperPrint r14(int empid) throws JRException {
        JasperDesign jd= JRXmlLoader.load("E:\\Clz document\\PayrollFinal\\Server\\src\\lk\\ijse\\payroll\\reports\\r14.jrxml");
        String SQL="select *from payroll where empid='"+empid+"' ;";
        JRDesignQuery jrDesignQuery=new JRDesignQuery();
        jrDesignQuery.setText(SQL);
        jd.setQuery(jrDesignQuery);

        JasperReport jr=JasperCompileManager.compileReport(jd);
        JasperPrint jp=JasperFillManager.fillReport(jr,null,connection);
        return jp;
    }
    private JasperPrint r15(int empid) throws JRException {
        JasperDesign jd= JRXmlLoader.load("E:\\Clz document\\PayrollFinal\\Server\\src\\lk\\ijse\\payroll\\reports\\r15.jrxml");
        String SQL="select e.empname,s.date,s.duration from shortleave s,employee e where s.empid=e.empid and s.empid='"+empid+"' ;";
        JRDesignQuery jrDesignQuery=new JRDesignQuery();
        jrDesignQuery.setText(SQL);
        jd.setQuery(jrDesignQuery);

        JasperReport jr=JasperCompileManager.compileReport(jd);
        JasperPrint jp=JasperFillManager.fillReport(jr,null,connection);
        return jp;
    }
    private JasperPrint r16(int empid) throws JRException {
        JasperDesign jd= JRXmlLoader.load("E:\\Clz document\\PayrollFinal\\Server\\src\\lk\\ijse\\payroll\\reports\\r16.jrxml");
        String SQL="select e.empname,s.date,s.no_of_days from leaves s,employee e where s.empid=e.empid and s.empid='"+empid+"' ;";
        JRDesignQuery jrDesignQuery=new JRDesignQuery();
        jrDesignQuery.setText(SQL);
        jd.setQuery(jrDesignQuery);

        JasperReport jr=JasperCompileManager.compileReport(jd);
        JasperPrint jp=JasperFillManager.fillReport(jr,null,connection);
        return jp;
    }
    private JasperPrint r18(int empid) throws JRException {
        JasperDesign jd= JRXmlLoader.load("E:\\Clz document\\PayrollFinal\\Server\\src\\lk\\ijse\\payroll\\reports\\r18.jrxml");
        String SQL="select p.name,p.date,p.EPF_8, p.EPF_12, p.ETF_3 from payroll p where empid='"+empid+"' ;";
        JRDesignQuery jrDesignQuery=new JRDesignQuery();
        jrDesignQuery.setText(SQL);
        jd.setQuery(jrDesignQuery);

        JasperReport jr=JasperCompileManager.compileReport(jd);
        JasperPrint jp=JasperFillManager.fillReport(jr,null,connection);
        return jp;
    }
    private JasperPrint r19(int empid) throws JRException {
        JasperDesign jd= JRXmlLoader.load("E:\\Clz document\\PayrollFinal\\Server\\src\\lk\\ijse\\payroll\\reports\\r19.jrxml");
        String SQL="select e.empname,e.address,e.gender,e.nic,e.d_o_b,e.phone_no,e.join_date,d.dename,b.amount from employee e,designation d,basicsalary b where e.deid=d.deid and e.bsid=b.bsid and e.empid='"+empid+"' ;";
        JRDesignQuery jrDesignQuery=new JRDesignQuery();
        jrDesignQuery.setText(SQL);
        jd.setQuery(jrDesignQuery);

        JasperReport jr=JasperCompileManager.compileReport(jd);
        JasperPrint jp=JasperFillManager.fillReport(jr,null,connection);
        return jp;
    }
    private JasperPrint r20(int empid) throws JRException {
        JasperDesign jd= JRXmlLoader.load("E:\\Clz document\\PayrollFinal\\Server\\src\\lk\\ijse\\payroll\\reports\\r20.jrxml");
        String SQL="select e.empname,l.loanName,el.date,el.installment,l.amount from employee e,loan l,emploan el where e.empid=el.empid and l.loanid=el.loanid and el.empid='"+empid+"' ;";
        JRDesignQuery jrDesignQuery=new JRDesignQuery();
        jrDesignQuery.setText(SQL);
        jd.setQuery(jrDesignQuery);

        JasperReport jr=JasperCompileManager.compileReport(jd);
        JasperPrint jp=JasperFillManager.fillReport(jr,null,connection);
        return jp;
    }
}
class DBConnection {
    private static DBConnection dbConnection;
    private Connection connection;
    private DBConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/finalPayroll","root","royalcops");
    }
    public static DBConnection getInstance() throws SQLException, ClassNotFoundException {
        if(dbConnection==null){
            dbConnection=new DBConnection();
        }
        return dbConnection;
    }
    public Connection getConnection(){
        return connection;
    }
}