package lk.ijse.payroll.repo;

import lk.ijse.payroll.repo.custom.impl.*;

public class RepoFactory {

    private static RepoFactory repoFactory;

    public static RepoFactory getInstance() {
        if (repoFactory == null) {
            repoFactory = new RepoFactory();
        }
        return repoFactory;
    }

    public enum RepoTypes {
        Attendance,BasicSalary,Designation,EmpAdvances,EmpLoans,Employee,Leaves,Loans,OT,Payroll,ShortLeaves;
    }

    public <T>T getRepo(RepoTypes types) {
        switch (types) {
            case Attendance:return (T) new AttendanceRepoImpl();
            case BasicSalary:return (T) new BasicSalaryRepoImpl();
            case Designation:return (T) new DesignationRepoImpl();
            case EmpAdvances:return (T) new EmpAdvanceRepoImpl();
            case EmpLoans:return (T) new EmpLoansRepoImpl();
            case Employee:return (T) new EmployeeRepoImpl();
            case Loans:return (T) new LoansRepoImpl();
            case Leaves:return (T) new LeavesRepoImpl();
            case OT:return (T) new OTRepoImpl();
            case Payroll:return (T) new PayrollRepoImpl();
            case ShortLeaves:return (T) new ShortLeaveRepoImpl();
            default:
                return null;

        }
    }
}
