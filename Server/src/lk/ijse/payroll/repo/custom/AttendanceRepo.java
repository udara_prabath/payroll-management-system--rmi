package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.entity.Attendance;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

import java.util.ArrayList;

public interface AttendanceRepo extends CrudRepository<Attendance, String> {
    void setSubSession(Session openSession)throws Exception;

    boolean markAttendanceIN(String name, String date, String time, String st)throws Exception;

    boolean markAttendanceOUT(String name, String date, String time, String day,int work)throws Exception;

    public String workedHours(String name,String date,String time)throws Exception;

    ArrayList<Object[]> getAllAttendance(String empid, String date)throws Exception;
}
