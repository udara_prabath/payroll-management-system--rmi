package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.dto.CustomAdvanceDTO;
import lk.ijse.payroll.entity.EmpAdvance;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

import java.util.ArrayList;

public interface EmpAdvanceRepo extends CrudRepository<EmpAdvance, String> {
    void setSubSession(Session openSession);
    String searchyear(String empid)throws Exception;

    String searchMonth(String empid)throws Exception;

    int searchYearByDate(String date)throws Exception;

    int searchMonthByDate(String date)throws Exception;

    boolean saveAdvance(String empid, String date, String amount)throws Exception;

    ArrayList<CustomAdvanceDTO> getAllAdvances()throws Exception;

    ArrayList<CustomAdvanceDTO> searchAllAdvancesByDateAndEmpID(String empid, String date)throws Exception;

    ArrayList<CustomAdvanceDTO> searchAllAdvancesByDate(String date)throws Exception;

    ArrayList<CustomAdvanceDTO> searchAllAdvancesByEmpId(String empid)throws Exception;

    String getLastMonthAdvance(String date, String empid)throws Exception;
}
