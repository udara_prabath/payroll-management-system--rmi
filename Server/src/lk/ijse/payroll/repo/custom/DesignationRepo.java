package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.entity.Designation;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

import java.util.ArrayList;

public interface DesignationRepo extends CrudRepository<Designation, String> {
    void setSubSession(Session openSession);
    ArrayList<String> getAllDesignation()throws Exception;

    ArrayList<Object[]> getAllDes()throws Exception;

    boolean saveDesignation(String text);

    boolean updateDesignation(String text, String text1);

    boolean deleteDesignatino(String text);
}
