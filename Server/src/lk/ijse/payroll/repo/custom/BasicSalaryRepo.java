package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

import java.util.ArrayList;

public interface BasicSalaryRepo extends CrudRepository<BasicSalary, String> {
    ArrayList<String> getAllBasicSalaryAmount()throws Exception;

    void setSubSession(Session openSession);

    ArrayList<Object[]> getAllBasicSalary()throws Exception;

    boolean saveBasicSalary(String text);

    boolean updateBasic(String text, String text1);

    boolean deleteBasic(String text);
}
