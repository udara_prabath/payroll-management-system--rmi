package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.entity.OT;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

public interface OTRepo extends CrudRepository<OT, String> {
    void setSubSession(Session openSession);
}
