package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.entity.Loan;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

import java.util.ArrayList;

public interface LoansRepo extends CrudRepository<Loan, String> {
    void setSubSession(Session openSession);
    ArrayList<String> getAllLoanNames()throws Exception;

    String searchLoanAmount(String name)throws Exception;

    ArrayList<Object[]> getAllLoan()throws Exception;

    boolean saveNeLoan(String text, String text1);

    boolean updateLoan(String text, String text1, String text2);

    boolean deleteLoan(String text);
}
