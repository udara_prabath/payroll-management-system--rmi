package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.dto.CustomLoanDTO;
import lk.ijse.payroll.entity.EmpLoan;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

import java.util.ArrayList;

public interface EmpLoansRepo extends CrudRepository<EmpLoan, String> {
    void setSubSession(Session openSession);
    boolean getEmployeeLastLoanDate(String empId, String date)throws Exception;

    boolean saveLoan(String empid, String loan, String date, String installment)throws Exception;

    ArrayList<CustomLoanDTO> getAllLoans()throws Exception;

    ArrayList<CustomLoanDTO> SearchAllLoansByEmpId(String id)throws Exception;

    ArrayList<CustomLoanDTO> SearchAllLoansByLoanName(String name)throws Exception;

    Object[] getLoanDetails(String date, String empid)throws Exception;
}
