package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.dto.CustomAdvanceDTO;
import lk.ijse.payroll.entity.EmpAdvance;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.EmpAdvanceRepo;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.util.ArrayList;

public class EmpAdvanceRepoImpl extends CrudRepositoryImpl<EmpAdvance, String> implements EmpAdvanceRepo {
    private Session session;

    @Override
    public void setSubSession(Session openSession) {
        this.session=openSession;
    }

    @Override
    public String searchyear(String empid) throws Exception {
        NativeQuery q = session.createNativeQuery("call searchYearByempId(?)");
        q.setParameter(1,empid);
        Object s = q.getSingleResult();
        if (s==null){
            return null;
        }else{
            return (int)s+"";
        }
    }

    @Override
    public String searchMonth(String empid) throws Exception {
        NativeQuery q = session.createNativeQuery("call searchMonthByempId(?)");
        q.setParameter(1,empid);
        Object s = q.getSingleResult();
        if (s==null){
            return null;
        }else{
            return (int)s+"";
        }
    }

    @Override
    public int searchYearByDate(String date) throws Exception {
        NativeQuery q = session.createNativeQuery("call monthBydate(?)");
        q.setParameter(1,date);
        Object s = q.getSingleResult();
        if (s==null){
            return 0;
        }else{
            String s1 = s.toString();
            int i = Integer.parseInt(s1);
            return i;
        }
    }

    @Override
    public int searchMonthByDate(String date) throws Exception {
        NativeQuery q = session.createNativeQuery("call YearBydate(?)");
        q.setParameter(1,date);
        Object s = q.getSingleResult();
        if (s==null){
            return 0;
        }else{
            String s1 = s.toString();
            int i = Integer.parseInt(s1);
            return i;
        }

    }

    @Override
    public boolean saveAdvance(String empid, String date, String amount) throws Exception {
        NativeQuery q = session.createNativeQuery("select addEmpAdvance(?,?,?)");
        q.setParameter(1,empid);q.setParameter(2,date);q.setParameter(3,amount);
        return (int)q.getSingleResult()>0;
    }

    @Override
    public ArrayList<CustomAdvanceDTO> getAllAdvances() throws Exception {
        ArrayList<CustomAdvanceDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,ead.year,ead.month,ead.amount from employee e,empadvances ead where e.empid=ead.empid");
//        q.setParameter(1,name);
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomAdvanceDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+""));
        }
        return dtos;
    }

    @Override
    public ArrayList<CustomAdvanceDTO> searchAllAdvancesByDateAndEmpID(String empid, String date) throws Exception {
        ArrayList<CustomAdvanceDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,ead.year,ead.month,ead.amount from employee e,empadvances ead where e.empid=ead.empid and ead.month=month(?) and ead.year=year(?) and ead.empid=? ");
        q.setParameter(3,empid);q.setParameter(2,date);q.setParameter(1,date);
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomAdvanceDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+""));
        }
        return dtos;
    }

    @Override
    public ArrayList<CustomAdvanceDTO> searchAllAdvancesByDate(String date) throws Exception {
        ArrayList<CustomAdvanceDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,ead.year,ead.month,ead.amount from employee e,empadvances ead where e.empid=ead.empid and ead.month=month(?) and ead.year=year(?) ");
        q.setParameter(1,date);q.setParameter(2,date);
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomAdvanceDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+""));
        }
        return dtos;
    }

    @Override
    public ArrayList<CustomAdvanceDTO> searchAllAdvancesByEmpId(String empid) throws Exception {
        ArrayList<CustomAdvanceDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,ead.year,ead.month,ead.amount from employee e,empadvances ead where e.empid=ead.empid and ead.empid=? ");
        q.setParameter(1,empid);
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomAdvanceDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+""));
        }
        return dtos;
    }

    @Override
    public String getLastMonthAdvance(String date, String empid) throws Exception {
        NativeQuery q = session.createNativeQuery("call searchAdNameAndM_I(?,?) ");
        q.setParameter(1,date);q.setParameter(2,empid);
        Object s = q.getSingleResult();
        return s+"";
    }
}
