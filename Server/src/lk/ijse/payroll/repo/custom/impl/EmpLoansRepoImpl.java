package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.dto.CustomLoanDTO;
import lk.ijse.payroll.entity.EmpLoan;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.EmpLoansRepo;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.util.ArrayList;

public class EmpLoansRepoImpl extends CrudRepositoryImpl<EmpLoan, String> implements EmpLoansRepo {
    private Session session;

    @Override
    public void setSubSession(Session openSession) {
        this.session=openSession;
    }

    @Override
    public boolean getEmployeeLastLoanDate(String empId, String date) throws Exception {
        NativeQuery q = session.createNativeQuery("call searchLoanDate(?,?)");
        q.setParameter(1,empId);q.setParameter(2,date);
        Object s = q.getSingleResult();
        if (s==null){
            return true;
        }else {
            int i= (int) s;
            if (i>365){
                return true;
            }else{
                return false;
            }
        }
    }

    @Override
    public boolean saveLoan(String empid, String loan, String date, String installment) throws Exception {
        NativeQuery q = session.createNativeQuery("call addEmpLoan(?,?,?,?)");
        q.setParameter(1,empid);q.setParameter(2,loan);q.setParameter(3,date);q.setParameter(4,installment);
        return (Integer)q.getSingleResult()>0;
    }

    @Override
    public ArrayList<CustomLoanDTO> getAllLoans() throws Exception {
        ArrayList<CustomLoanDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,l.loanName,el.date,el.installment,l.amount from employee e,loan l,emploan el where e.empid=el.empid and l.loanid=el.loanid ");
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomLoanDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+""));
        }
        return dtos;
    }

    @Override
    public ArrayList<CustomLoanDTO> SearchAllLoansByEmpId(String id) throws Exception {
        ArrayList<CustomLoanDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,l.loanName,el.date,el.installment,l.amount from employee e,loan l,emploan el where e.empid=el.empid and l.loanid=el.loanid and el.empid=?");
        q.setParameter(1,id);
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomLoanDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+""));
        }
        return dtos;
    }

    @Override
    public ArrayList<CustomLoanDTO> SearchAllLoansByLoanName(String name) throws Exception {
        ArrayList<CustomLoanDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,l.loanName,el.date,el.installment,l.amount from employee e,loan l,emploan el where e.empid=el.empid and l.loanid=el.loanid and l.loanName=?");
        q.setParameter(1,name);
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomLoanDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+""));
        }
        return dtos;
    }

    @Override
    public Object[] getLoanDetails(String date, String empid) throws Exception {
        NativeQuery q = session.createNativeQuery("call searchLoanDetails(?,?)");
        q.setParameter(1,date);q.setParameter(2,empid);
        Object s[] = (Object[]) q.getSingleResult();
        return s;
    }
}
