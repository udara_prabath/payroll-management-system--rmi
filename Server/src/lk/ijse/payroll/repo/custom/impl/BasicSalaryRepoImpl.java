package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.BasicSalaryRepo;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.util.ArrayList;

public class BasicSalaryRepoImpl extends CrudRepositoryImpl<BasicSalary, String> implements BasicSalaryRepo {
    private Session session;

    @Override
    public void setSubSession(Session openSession) {
        this.session=openSession;
    }

    @Override
    public ArrayList<Object[]> getAllBasicSalary() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT bsid,amount FROM basicSalary");
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        return list;
    }

    @Override
    public boolean saveBasicSalary(String text) {
        NativeQuery q = session.createNativeQuery("call addBasic(?)");
        q.setParameter(1,text);
        Object a = q.getSingleResult();
        int r= (int) a;
        return r>0;
    }

    @Override
    public boolean updateBasic(String text, String text1) {
        NativeQuery q = session.createNativeQuery("call updateBasic(?,?)");
        q.setParameter(1,text);q.setParameter(2,text1);
        Object a = q.getSingleResult();
        int r= (int) a;
        return r>0;
    }

    @Override
    public boolean deleteBasic(String text) {
        NativeQuery q = session.createNativeQuery("call deleteBasic(?)");
        q.setParameter(1,text);
        Object a = q.getSingleResult();
        int r= (int) a;
        return r>0;
    }

    @Override
    public ArrayList<String> getAllBasicSalaryAmount() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT Amount FROM basicSalary");
        ArrayList<String> amount= (ArrayList<String>) q.getResultList();
        return amount;
    }

}
