package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.dto.LeaveDTO;
import lk.ijse.payroll.dto.ShortLeaveDTO;
import lk.ijse.payroll.entity.Leaves;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.LeavesRepo;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.util.ArrayList;

public class LeavesRepoImpl extends CrudRepositoryImpl<Leaves, String> implements LeavesRepo {
    public Session session;

    @Override
    public void setSubSession(Session openSession) {
        this.session=openSession;
    }

    @Override
    public boolean saveShortLeave(String emp, String date, int no) throws Exception {
        NativeQuery q = session.createNativeQuery("call saveShortLeave(?,?,?) ");
        q.setParameter(1,emp);q.setParameter(2,date);q.setParameter(3,no);
        return (int) q.getSingleResult()>0;
    }

    @Override
    public boolean saveEmployeeLeave(String emp, String date, int no) throws Exception {
        NativeQuery q = session.createNativeQuery("call saveEmployeeLeave(?,?,?) ");
        q.setParameter(1,emp);q.setParameter(2,date);q.setParameter(3,no);
        return (int) q.getSingleResult()>0;
    }

    @Override
    public ArrayList<ShortLeaveDTO> getAllShortLeaves() throws Exception {
        ArrayList<ShortLeaveDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,s.date,s.Duration from employee e,shortleave s where e.empid=s.empid");
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new ShortLeaveDTO(ob[0]+"",ob[1]+"",ob[2]+""));
        }
        return dtos;
    }

    @Override
    public ArrayList<LeaveDTO> getAllLeaves() throws Exception {
        ArrayList<LeaveDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,s.date,s.No_of_days from employee e,leaves s where e.empid=s.empid");
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new LeaveDTO(ob[0]+"",ob[1]+"",ob[2]+""));
        }
        return dtos;
    }
}
