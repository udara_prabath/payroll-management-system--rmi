package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.entity.PayRoll;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.PayrollRepo;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.util.ArrayList;

public class PayrollRepoImpl extends CrudRepositoryImpl<PayRoll, String> implements PayrollRepo {
    Session session;
    @Override
    public void setSubSession(Session openSession) {
        this.session=openSession;
    }

    @Override
    public boolean savePayroll(int empid, String date, String name, double bs, String work, double not, double dot, String leaves, String shrt, double ep8, double ep12, double ep3, double loa, double adv, double bo, double gro, double de, double tot) throws Exception {
        NativeQuery q = session.createNativeQuery("call addpayroll(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
        q.setParameter(1,empid);q.setParameter(2,date);q.setParameter(3,name);q.setParameter(4,bs);q.setParameter(5,work);q.setParameter(6,not);q.setParameter(7,dot);q.setParameter(8,leaves);q.setParameter(9,shrt);q.setParameter(10,ep8);q.setParameter(11,ep12);q.setParameter(12,ep3);q.setParameter(13,loa);q.setParameter(14,adv);q.setParameter(15,bo);q.setParameter(16,gro);q.setParameter(17,de);q.setParameter(18,tot);

        return (int) q.getSingleResult()>0;
    }

    @Override
    public ArrayList<Object[]> getAllPayroll() throws Exception {
        NativeQuery q = session.createNativeQuery("call allPayroll() ");
        ArrayList<Object[]> s = (ArrayList<Object[]>) q.getResultList();
        return s;
    }
    @Override
    public ArrayList<Object[]> getAllPayroll(String id) throws Exception {
        NativeQuery q = session.createNativeQuery("select * from payroll where empid=? ");
        q.setParameter(1,id);
        ArrayList<Object[]> s = (ArrayList<Object[]>) q.getResultList();
        return s;
    }
    @Override
    public ArrayList<Object[]> getAllPayrollbyDate(String date) throws Exception {
        NativeQuery q = session.createNativeQuery("select * from payroll where month(date)=? and year(date)=? ");
        q.setParameter(1,date);q.setParameter(2,date);
        ArrayList<Object[]> s = (ArrayList<Object[]>) q.getResultList();
        return s;
    }
    @Override
    public ArrayList<Object[]> getAllPayroll(String id,String date) throws Exception {
        NativeQuery q = session.createNativeQuery("select * from payroll where empid=? and month(date)=? and year(date)=?");
        q.setParameter(1,id);q.setParameter(2,date);q.setParameter(3,date);
        ArrayList<Object[]> s = (ArrayList<Object[]>) q.getResultList();
        return s;
    }
}
