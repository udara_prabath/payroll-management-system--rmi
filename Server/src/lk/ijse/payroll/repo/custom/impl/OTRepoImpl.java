package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.entity.OT;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.OTRepo;
import org.hibernate.Session;

public class OTRepoImpl extends CrudRepositoryImpl<OT, String> implements OTRepo {
    @Override
    public void setSubSession(Session openSession) {

    }
}
