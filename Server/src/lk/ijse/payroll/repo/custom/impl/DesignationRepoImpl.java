package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.entity.Designation;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.DesignationRepo;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.util.ArrayList;

public class DesignationRepoImpl extends CrudRepositoryImpl<Designation, String> implements DesignationRepo {
    private Session session;

    @Override
    public void setSubSession(Session openSession) {
        this.session=openSession;
    }

    @Override
    public ArrayList<String> getAllDesignation() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT dename FROM Designation");
        ArrayList<String> list= (ArrayList<String>) q.getResultList();
        return list;
    }

    @Override
    public ArrayList<Object[]> getAllDes() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT deid,dename FROM designation");
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        return list;
    }

    @Override
    public boolean saveDesignation(String text) {
        NativeQuery q = session.createNativeQuery("call adddes(?)");
        q.setParameter(1,text);
        Object a = q.getSingleResult();
        int r= (int) a;
        return r>0;
    }

    @Override
    public boolean updateDesignation(String text, String text1) {
        NativeQuery q = session.createNativeQuery("call updatedes(?,?)");
        q.setParameter(1,text);q.setParameter(2,text1);
        Object a = q.getSingleResult();
        int r= (int) a;
        return r>0;
    }

    @Override
    public boolean deleteDesignatino(String text) {
        NativeQuery q = session.createNativeQuery("call deletedes(?)");
        q.setParameter(1,text);
        Object a = q.getSingleResult();
        int r= (int) a;
        return r>0;
    }
}
