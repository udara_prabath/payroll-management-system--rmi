package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.entity.Loan;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.LoansRepo;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.util.ArrayList;

public class LoansRepoImpl extends CrudRepositoryImpl<Loan, String> implements LoansRepo {
    private Session session;

    @Override
    public void setSubSession(Session openSession) {
        this.session=openSession;
    }

    @Override
    public ArrayList<String> getAllLoanNames() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT loanName FROM loan");
        ArrayList<String> list= (ArrayList<String>) q.getResultList();
        return list;
    }

    @Override
    public String searchLoanAmount(String name) throws Exception {
        NativeQuery q = session.createNativeQuery("select amount from loan where loanname=?");
        q.setParameter(1,name);
        Object s = q.getSingleResult();
        String s1 = s.toString();
        return s1;
    }

    @Override
    public ArrayList<Object[]> getAllLoan() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT loanid,loanName,amount FROM loan");
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        return list;
    }

    @Override
    public boolean saveNeLoan(String text, String text1) {
        NativeQuery q = session.createNativeQuery("call addLoan(?,?)");
        q.setParameter(1,text);q.setParameter(2,text1);
        Object a = q.getSingleResult();
        int r= (int) a;
        return r>0;
    }

    @Override
    public boolean updateLoan(String text, String text1, String text2) {
        NativeQuery q = session.createNativeQuery("call updateLoan(?,?,?)");
        q.setParameter(1,text);q.setParameter(2,text1);q.setParameter(3,text2);
        Object a = q.getSingleResult();
        int r= (int) a;
        return r>0;
    }

    @Override
    public boolean deleteLoan(String text) {
        NativeQuery q = session.createNativeQuery("call deleteLoan(?)");
        q.setParameter(1,text);
        Object a = q.getSingleResult();
        int r= (int) a;
        return r>0;
    }
}
