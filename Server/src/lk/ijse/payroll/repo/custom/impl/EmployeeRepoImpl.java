package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.dto.CustomEmployeeDTO;
import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.EmployeeRepo;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.util.ArrayList;

public class EmployeeRepoImpl extends CrudRepositoryImpl<Employee, String> implements EmployeeRepo {
    private Session session;

    @Override
    public void setSubSession(Session openSession) {
        this.session=openSession;
    }

    @Override
    public ArrayList<String> getAllEmployeeIDs() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT empid FROM employee");
        ArrayList<String> list= (ArrayList<String>) q.getResultList();
        return list;
    }

    public ArrayList<String> getAllEmployeeNames() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT empname FROM employee where attendance='undefind' ");
        ArrayList<String> list= (ArrayList<String>) q.getResultList();
        return list;
    }

    @Override
    public ArrayList<String> getAllEmployeeNamesForAtt() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT empname FROM employee where attendance='present' ");
        ArrayList<String> list= (ArrayList<String>) q.getResultList();
        return list;
    }
    @Override
    public ArrayList<String> getAllEmployeeNamesForpay() throws Exception {
        NativeQuery q = session.createNativeQuery("SELECT empid FROM employee where payroll='undefind' ");
        ArrayList<String> list= (ArrayList<String>) q.getResultList();
        return list;
    }


    @Override
    public boolean saveEmployee(String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, int i, String basicSalary) throws Exception {
        NativeQuery q = session.createNativeQuery("call addEmployee(?,?,?,?,?,?,?,?,?,?)");
        q.setParameter(1,name);q.setParameter(2,address);q.setParameter(3,gender);q.setParameter(4,nic);q.setParameter(5,dob);q.setParameter(6,tel);q.setParameter(7,joindate);q.setParameter(8,designation);q.setParameter(9,i);q.setParameter(10,basicSalary);
        int row= (int) q.getSingleResult();
        if (row>0){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean UpdateEmployee(String id, String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, String basicSalary) throws Exception {
        NativeQuery q2 = session.createNativeQuery("call updateEmployee(?,?,?,?,?,?,?,?,?,?)");
        q2.setParameter(1,id);q2.setParameter(2,name);q2.setParameter(3,address);q2.setParameter(4,gender);q2.setParameter(5,nic);q2.setParameter(6,dob);q2.setParameter(7,tel);q2.setParameter(8,joindate);q2.setParameter(9,designation);q2.setParameter(10,basicSalary);
        int r= (int) q2.getSingleResult();
        if(r>0){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public CustomEmployeeDTO searchEmployee(String id) throws Exception {
        NativeQuery q = session.createNativeQuery("select e.empid,e.empname,e.address,e.gender,e.nic,e.d_o_b,e.phone_no,e.join_date,d.dename,b.amount from employee e,designation d,basicsalary b where e.deid=d.deid and e.bsid=b.bsid and e.empid=? ");
        q.setParameter(1,id);
        Object[] ob= (Object[]) q.getSingleResult();
        return new CustomEmployeeDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+"",ob[5]+"",ob[6]+"",ob[7]+"",ob[8]+"",ob[9]+"");
    }

    @Override
    public boolean deleteEmployee(String id) throws Exception {
        NativeQuery q = session.createNativeQuery("call deleteEmp(?) ");
        q.setParameter(1,id);
        return (int) q.getSingleResult()>0;
    }

    @Override
    public ArrayList<CustomEmployeeDTO> searchEmployee() throws Exception {
        ArrayList<CustomEmployeeDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empid,e.empname,d.dename,e.nic,e.join_date,b.amount from employee e,designation d,basicsalary b where e.deid=d.deid and e.bsid=b.bsid");
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomEmployeeDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+"",ob[5]+""));
        }
        return dtos;
    }

    @Override
    public ArrayList<CustomEmployeeDTO> searchCustomEmployeeByID(String id) throws Exception {
        ArrayList<CustomEmployeeDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empid,e.empname,d.dename,e.nic,e.join_date,b.amount from employee e,designation d,basicsalary b where e.deid=d.deid and e.bsid=b.bsid and e.empid=?");
        q.setParameter(1,id);
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomEmployeeDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+"",ob[5]+""));
        }
        return dtos;
    }

    @Override
    public ArrayList<CustomEmployeeDTO> searchCustomEmployeeBydes(String des) throws Exception {
        ArrayList<CustomEmployeeDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empid,e.empname,d.dename,e.nic,e.join_date,b.amount from employee e,designation d,basicsalary b where e.deid=d.deid and e.bsid=b.bsid and d.dename=?");
        q.setParameter(1,des);
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
        for (Object[] ob: list) {
            dtos.add(new CustomEmployeeDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+"",ob[5]+""));
        }
        return dtos;
    }

    @Override
    public String[] getEmpNameAndDesignation(String empid) throws Exception {
        NativeQuery q = session.createNativeQuery("select e.empname,b.amount from employee e,basicsalary b where  e.bsid=b.bsid and e.empid=? ");
        q.setParameter(1,empid);
        Object[] s = (Object[]) q.getSingleResult();
        String[] a=new String[2];
        a[0]=s[0].toString();a[1]=s[1].toString();
        return a;
    }

    @Override
    public Object[] calculateSalary(int not, int dot, int leaves, int shrt, double bs, double ep8, double loa, double adv, double bo) throws Exception {
        NativeQuery q = session.createNativeQuery("call CalculatePayroll(?,?,?,?,?,?,?,?,?)");
        q.setParameter(1,not);q.setParameter(2,dot);q.setParameter(3,leaves);q.setParameter(4,shrt);q.setParameter(5,bs);q.setParameter(6,ep8);q.setParameter(7,loa);q.setParameter(8,adv);q.setParameter(9,bo);
        Object s[] = (Object[]) q.getSingleResult();
        return s;
    }
}
