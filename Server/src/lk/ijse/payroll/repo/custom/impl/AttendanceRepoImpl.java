package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.dto.AttendanceDTO;
import lk.ijse.payroll.entity.Attendance;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.AttendanceRepo;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import java.util.ArrayList;

public class AttendanceRepoImpl extends CrudRepositoryImpl<Attendance, String> implements AttendanceRepo {

    public Session session;
    @Override
    public void setSubSession(Session openSession) throws Exception{
        this.session=openSession;
    }

    @Override
    public boolean markAttendanceIN(String name, String date, String time, String st)throws Exception {
        NativeQuery q = session.createNativeQuery("call markAttendanceIN(?,?,?,?) ");
        q.setParameter(1,name);q.setParameter(2,date);q.setParameter(3,time);q.setParameter(4,st);
        return (int) q.getSingleResult()>0;
    }

    @Override
    public boolean markAttendanceOUT(String name, String date, String time, String day,int work) throws Exception {
        NativeQuery q = session.createNativeQuery("call markAttendanceOUT(?,?,?,?,?) ");
        q.setParameter(1,name);q.setParameter(2,date);q.setParameter(3,time);q.setParameter(4,day);q.setParameter(5,work);
        return (int) q.getSingleResult()>0;
    }

    public String workedHours(String name,String date,String time)throws Exception{
        NativeQuery q = session.createNativeQuery("call getWorkedHours(?,?,?) ");
        q.setParameter(1,name);q.setParameter(2,date);q.setParameter(3,time);
        return (String) q.getSingleResult();
    }

    @Override
    public ArrayList<Object[]> getAllAttendance(String empid, String date) throws Exception {
        ArrayList<AttendanceDTO> dtos=new ArrayList<>();
        NativeQuery q = session.createNativeQuery("select e.empname,a.day,a.Worked_Hourse,o.normal_ot,o.double_ot from employee e,attendance a,ot o where e.empid=a.empid and e.empid=o.empid and year(a.day)=year(?) and month(a.day)=month(?) and year(o.date)=year(?) and month(o.date)=month(?) and e.empid=?");
        q.setParameter(1,date);q.setParameter(2,date);q.setParameter(3,date);q.setParameter(4,date);q.setParameter(5,empid);
        ArrayList<Object[]> list= (ArrayList<Object[]>) q.getResultList();
//        for (Object[] ob: list) {
//            dtos.add(new AttendanceDTO(ob[0]+"",ob[1]+"",ob[2]+"",ob[3]+"",ob[4]+""));
//        }
        return list;
    }
}
