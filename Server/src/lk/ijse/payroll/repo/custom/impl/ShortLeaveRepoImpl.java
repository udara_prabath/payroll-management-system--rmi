package lk.ijse.payroll.repo.custom.impl;

import lk.ijse.payroll.entity.ShortLeaves;
import lk.ijse.payroll.repo.CrudRepositoryImpl;
import lk.ijse.payroll.repo.custom.ShortLeavesRepo;
import org.hibernate.Session;

public class ShortLeaveRepoImpl extends CrudRepositoryImpl<ShortLeaves, String> implements ShortLeavesRepo {
    @Override
    public void setSubSession(Session openSession) {

    }
}
