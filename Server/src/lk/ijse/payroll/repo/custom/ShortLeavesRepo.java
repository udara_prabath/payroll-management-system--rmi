package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.entity.ShortLeaves;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

public interface ShortLeavesRepo extends CrudRepository<ShortLeaves, String> {
    void setSubSession(Session openSession);
}
