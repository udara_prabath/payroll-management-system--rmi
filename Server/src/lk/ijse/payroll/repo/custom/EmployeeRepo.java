package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.dto.CustomEmployeeDTO;
import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

import java.util.ArrayList;

public interface EmployeeRepo extends CrudRepository<Employee, String> {
    void setSubSession(Session openSession);
    ArrayList<String> getAllEmployeeIDs()throws Exception;
    public ArrayList<String> getAllEmployeeNames() throws Exception;
    ArrayList<String> getAllEmployeeNamesForAtt()throws Exception;
    ArrayList<String> getAllEmployeeNamesForpay()throws Exception;
    boolean saveEmployee(String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, int i, String basicSalary)throws Exception;

    boolean UpdateEmployee(String id, String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, String basicSalary)throws Exception;

    CustomEmployeeDTO searchEmployee(String id)throws Exception;

    boolean deleteEmployee(String id)throws Exception;

    ArrayList<CustomEmployeeDTO> searchEmployee()throws Exception;

    ArrayList<CustomEmployeeDTO> searchCustomEmployeeByID(String id)throws Exception;

    ArrayList<CustomEmployeeDTO> searchCustomEmployeeBydes(String des)throws Exception;


    String[] getEmpNameAndDesignation(String empid)throws Exception;

    Object[] calculateSalary(int not, int dot, int leaves, int shrt, double bs, double ep8, double loa, double adv, double bo)throws Exception;
}
