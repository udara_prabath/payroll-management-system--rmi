package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.entity.PayRoll;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

import java.util.ArrayList;

public interface PayrollRepo extends CrudRepository<PayRoll, String> {
    void setSubSession(Session openSession);

    boolean savePayroll(int empid, String date, String name, double bs, String work, double not, double dot, String leaves, String shrt, double ep8, double ep12, double ep3, double loa, double adv, double bo, double gro, double de, double tot)throws Exception;

    ArrayList<Object[]> getAllPayroll()throws Exception;
    ArrayList<Object[]> getAllPayroll(String id)throws Exception;
    public ArrayList<Object[]> getAllPayroll(String id,String date) throws Exception;
    ArrayList<Object[]> getAllPayrollbyDate(String id)throws Exception;
}
