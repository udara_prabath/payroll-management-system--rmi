package lk.ijse.payroll.repo.custom;

import lk.ijse.payroll.dto.LeaveDTO;
import lk.ijse.payroll.dto.ShortLeaveDTO;
import lk.ijse.payroll.entity.Leaves;
import lk.ijse.payroll.repo.CrudRepository;
import org.hibernate.Session;

import java.util.ArrayList;

public interface LeavesRepo extends CrudRepository<Leaves, String> {
    void setSubSession(Session openSession);

    boolean saveShortLeave(String emp, String date, int no)throws Exception;
    boolean saveEmployeeLeave(String emp, String date, int no)throws Exception;

    ArrayList<ShortLeaveDTO> getAllShortLeaves()throws Exception;
    ArrayList<LeaveDTO> getAllLeaves()throws Exception;
}
