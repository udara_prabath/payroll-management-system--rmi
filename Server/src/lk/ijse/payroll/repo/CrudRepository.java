package lk.ijse.payroll.repo;

import java.util.List;

public interface CrudRepository<T,ID> extends SuperRepository{
    void add(T ent)throws Exception;
    void update(T ent)throws Exception;
    void delete(T ent)throws Exception;
    T search(ID id)throws Exception;
    List<T> getAll()throws Exception;

}
