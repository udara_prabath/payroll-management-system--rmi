package lk.ijse.payroll.repo;

import org.hibernate.Session;

public interface SuperRepository {
    void setSession(Session session)throws Exception;
}
