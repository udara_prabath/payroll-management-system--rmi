package lk.ijse.payroll.reservation;

import lk.ijse.payroll.service.SuperService;
import lk.ijse.payroll.service.custom.AdminService;

public class ReservationUtil{
    private static Reservation<SuperService> res = new Reservation();


    public boolean reserve(Object id, SuperService s) throws Exception {
        return res.reserve(id,s,true);
    }


    public boolean release(Object id,SuperService s) throws Exception {
        return res.release(id,s);
    }
}
