package lk.ijse.payroll.reservation;

import java.util.HashMap;

public class Reservation<T> {
    private static class ResMap<T> {

        private T service;
        private boolean isReserved;

        public ResMap(T service, boolean isReserved) {
            this.service = service;
            this.isReserved = isReserved;
        }

        /**
         * @return the service
         */
        public T getService() {
            return service;
        }

        /**
         * @param service the service to set
         */
        public void setService(T service) {
            this.service = service;
        }

        /**
         * @return the isReserved
         */
        public boolean isIsReserved() {
            return isReserved;
        }

        /**
         * @param isReserved the isReserved to set
         */
        public void setIsReserved(boolean isReserved) {
            this.isReserved = isReserved;
        }



    }

    HashMap<Object, ResMap<T>> reservationMap = new HashMap<>();

    public boolean reserve(Object key, T service, boolean isReserved) {
        if (reservationMap.containsKey(key)) {
            return reservationMap.get(key).getService() == service;
        } else {
            reservationMap.put(key, new ResMap<T>(service, isReserved));
            return true;
        }
    }

    public boolean release(Object key, T service) {
        if (reservationMap.containsKey(key) && reservationMap.get(key).getService() == service) {
            reservationMap.remove(key);
            return true;
        }
        return false;
    }

    public boolean checkStatus(Object key, T service) {
        if (reservationMap.containsKey(key) && reservationMap.get(key).getService() == service) {
            return reservationMap.get(key).isIsReserved();
        } else {
            return false;
        }
    }
}
