package lk.ijse.payroll.business;

import lk.ijse.payroll.business.custom.impl.*;

public class BOFactory {
    private static BOFactory boFactory;

    public static BOFactory getInstance() {
        if (boFactory == null) {
            boFactory = new BOFactory();
        }
        return boFactory;
    }
    public enum BOTypes {
        EmpAdvance,BasicSalary,Designation,Employee,Loans,EmpLoan,Attendance,leaves,Payroll
    }

    public <T> T getBO(BOTypes types) {
        switch (types) {
            case EmpAdvance: return (T) new EmpAdvanceBOImpl();
            case BasicSalary: return (T) new BasicSalaryBOimpl();
            case Designation: return (T) new DesignationBOImpl();
            case Employee: return (T) new EmployeeBOImpl();
            case Loans: return (T) new LoanBOImpl();
            case EmpLoan: return (T) new EmpLoanImpl();
            case Attendance: return (T) new AttendanceBOImpl();
            case leaves:return (T) new LeavesBOImpl();
            case Payroll:return (T) new PayrollBOImpl();
            default:
                return null;
        }

    }
}
