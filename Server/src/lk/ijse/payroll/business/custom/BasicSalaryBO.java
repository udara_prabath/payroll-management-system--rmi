package lk.ijse.payroll.business.custom;

import lk.ijse.payroll.business.SuperBO;

import java.util.ArrayList;

public interface BasicSalaryBO extends SuperBO {
    ArrayList<String> getAllBasicSalaryAmount()throws Exception;

    ArrayList<Object[]> getAllbasicSalary()throws Exception;

    boolean saveBasicSalary(String text)throws Exception;

    boolean updateBasic(String text, String text1)throws Exception;

    boolean deleteBasic(String text)throws Exception;
}
