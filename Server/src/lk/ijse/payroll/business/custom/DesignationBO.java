package lk.ijse.payroll.business.custom;

import lk.ijse.payroll.business.SuperBO;

import java.util.ArrayList;

public interface DesignationBO extends SuperBO {
    ArrayList<String> getAllDesignation()throws Exception;

    ArrayList<Object[]> getAllDes()throws Exception;

    boolean saveDesignation(String text)throws Exception;

    boolean updateDesignation(String text, String text1)throws Exception;

    boolean deleteDesignatino(String text)throws Exception;
}
