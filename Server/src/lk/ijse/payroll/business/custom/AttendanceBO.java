package lk.ijse.payroll.business.custom;

import lk.ijse.payroll.business.SuperBO;
import lk.ijse.payroll.dto.AttendanceDTO;

import java.util.ArrayList;

public interface AttendanceBO extends SuperBO {
    boolean markAttendanceIN(String name, String date, String time, String st)throws Exception;

    boolean markAttendanceOUT(String name, String date, String time, String day)throws Exception;

    ArrayList<Object[]> getAllAttendance(String empid, String date)throws Exception;
}
