package lk.ijse.payroll.business.custom.impl;


import lk.ijse.payroll.business.custom.DesignationBO;
import lk.ijse.payroll.repo.RepoFactory;
import lk.ijse.payroll.repo.custom.DesignationRepo;
import lk.ijse.payroll.resources.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;

public class DesignationBOImpl implements DesignationBO {

    DesignationRepo des=RepoFactory.getInstance().getRepo(RepoFactory.RepoTypes.Designation);

    public DesignationBOImpl() {
    }

    @Override
    public ArrayList<String> getAllDesignation() throws Exception {
        ArrayList<String> Des=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            des.setSubSession(openSession);
            openSession.getTransaction().begin();
            Des=des.getAllDesignation();
            openSession.getTransaction().commit();
            return Des;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Object[]> getAllDes() throws Exception {
        ArrayList<Object[]> Des=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            des.setSubSession(openSession);
            openSession.getTransaction().begin();
            Des=des.getAllDes();
            openSession.getTransaction().commit();
            return Des;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean saveDesignation(String text) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            des.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean Des=des.saveDesignation( text);
            openSession.getTransaction().commit();
            return Des;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateDesignation(String text, String text1) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            des.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean Des=des.updateDesignation( text,  text1);
            openSession.getTransaction().commit();
            return Des;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteDesignatino(String text) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            des.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean Des=des.deleteDesignatino( text);
            openSession.getTransaction().commit();
            return Des;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
