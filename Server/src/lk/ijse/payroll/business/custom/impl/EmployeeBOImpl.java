package lk.ijse.payroll.business.custom.impl;

import lk.ijse.payroll.business.custom.EmployeeBO;
import lk.ijse.payroll.dto.CustomEmployeeDTO;
import lk.ijse.payroll.repo.RepoFactory;
import lk.ijse.payroll.repo.custom.EmployeeRepo;
import lk.ijse.payroll.resources.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;

public class EmployeeBOImpl implements EmployeeBO {

    EmployeeRepo emp=RepoFactory.getInstance().getRepo(RepoFactory.RepoTypes.Employee);

    public EmployeeBOImpl() {
    }

    @Override
    public ArrayList<String> getAllEmployeeIDs() throws Exception {
        ArrayList<String> ids=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            ids=emp.getAllEmployeeIDs();
            openSession.getTransaction().commit();
            return ids;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    public ArrayList<String> getAllEmployeeName() throws Exception {
        ArrayList<String> name=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            name=emp.getAllEmployeeNames();
            openSession.getTransaction().commit();
            return name;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<String> getAllEmployeeNamesForAtt() throws Exception {
        ArrayList<String> name=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            name=emp.getAllEmployeeNamesForAtt();
            openSession.getTransaction().commit();
            return name;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    @Override
    public ArrayList<String> getAllEmployeeNamesForpay() throws Exception {
        ArrayList<String> name=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            name=emp.getAllEmployeeNamesForpay();
            openSession.getTransaction().commit();
            return name;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean saveEmployee(String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, int i, String basicSalary) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean save=emp.saveEmployee(name,address,gender,nic,dob,tel,joindate,designation,i,basicSalary);
            openSession.getTransaction().commit();
            return save;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean UpdateEmployee(String id, String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, String basicSalary) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean update=emp.UpdateEmployee(id,name,address,gender,nic,dob,tel,joindate,designation,basicSalary);
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public CustomEmployeeDTO searchEmployee(String id) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            CustomEmployeeDTO emp1=emp.searchEmployee(id);
            openSession.getTransaction().commit();
            return emp1;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteEmployee(String id) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean emp1=emp.deleteEmployee(id);
            openSession.getTransaction().commit();
            return emp1;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public ArrayList<CustomEmployeeDTO> searchEmployee() throws Exception {
        ArrayList<CustomEmployeeDTO> emp1=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            emp1=emp.searchEmployee();
            openSession.getTransaction().commit();
            return emp1;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<CustomEmployeeDTO> searchCustomEmployeeByID(String id) throws Exception {
        ArrayList<CustomEmployeeDTO> emp1=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            emp1=emp.searchCustomEmployeeByID(id);
            openSession.getTransaction().commit();
            return emp1;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<CustomEmployeeDTO> searchCustomEmployeeBydes(String des) throws Exception {
        ArrayList<CustomEmployeeDTO> id=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            id=emp.searchCustomEmployeeBydes(des);
            openSession.getTransaction().commit();
            return id;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String[] getEmpNameAndDesignation(String empid) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            String[] emp1=emp.getEmpNameAndDesignation(empid);
            openSession.getTransaction().commit();
            return emp1;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Object[] calculateSalary(int not, int dot, int leaves, int shrt, double bs, double ep8, double loa, double adv, double bo) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            emp.setSubSession(openSession);
            openSession.getTransaction().begin();
            Object[] a=emp.calculateSalary(not,dot,leaves,shrt,bs,ep8,loa,adv,bo);
            openSession.getTransaction().commit();
            return a;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
