package lk.ijse.payroll.business.custom.impl;

import lk.ijse.payroll.business.custom.LoansBO;
import lk.ijse.payroll.repo.RepoFactory;
import lk.ijse.payroll.repo.custom.LoansRepo;
import lk.ijse.payroll.resources.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;

public class LoanBOImpl implements LoansBO {

    LoansRepo loan=RepoFactory.getInstance().getRepo(RepoFactory.RepoTypes.Loans);

    public LoanBOImpl() {
    }

    @Override
    public ArrayList<String> getAllLoanNames() throws Exception {
        ArrayList<String> Loan=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            loan.setSubSession(openSession);
            openSession.getTransaction().begin();
            Loan=loan.getAllLoanNames();
            openSession.getTransaction().commit();
            return Loan;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String searchLoanAmount(String name) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            loan.setSubSession(openSession);
            openSession.getTransaction().begin();
            String amount = loan.searchLoanAmount(name);
            openSession.getTransaction().commit();
            return amount;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Object[]> getAllLoans() throws Exception {
        ArrayList<Object[]> Loan=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            loan.setSubSession(openSession);
            openSession.getTransaction().begin();
            Loan=loan.getAllLoan();
            openSession.getTransaction().commit();
            return Loan;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean saveNeLoan(String text, String text1) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            loan.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean Loan=loan.saveNeLoan( text,  text1);
            openSession.getTransaction().commit();
            return Loan;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateLoan(String text, String text1, String text2) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            loan.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean Loan=loan.updateLoan( text,  text1,  text2);
            openSession.getTransaction().commit();
            return Loan;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteLoan(String text) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            loan.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean Loan=loan.deleteLoan( text);
            openSession.getTransaction().commit();
            return Loan;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
