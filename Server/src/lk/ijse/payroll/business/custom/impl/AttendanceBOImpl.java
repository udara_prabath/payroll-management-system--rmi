package lk.ijse.payroll.business.custom.impl;

import lk.ijse.payroll.business.custom.AttendanceBO;
import lk.ijse.payroll.dto.AttendanceDTO;
import lk.ijse.payroll.repo.RepoFactory;
import lk.ijse.payroll.repo.custom.AttendanceRepo;
import lk.ijse.payroll.resources.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;

public class AttendanceBOImpl implements AttendanceBO {

    AttendanceRepo att=RepoFactory.getInstance().getRepo(RepoFactory.RepoTypes.Attendance);

    public AttendanceBOImpl() {
    }

    @Override
    public boolean markAttendanceIN(String name, String date, String time, String st) throws Exception{
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            att.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean update=att.markAttendanceIN(name,date,time,st);
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean markAttendanceOUT(String name, String date, String time, String day) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            att.setSubSession(openSession);
            openSession.getTransaction().begin();
            String wh=att.workedHours(name,date,time);
            String[] split = wh.split("[:]");
            boolean update=att.markAttendanceOUT(name,date,time,day,Integer.parseInt(split[0]));
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public ArrayList<Object[]> getAllAttendance(String empid, String date) throws Exception {
        ArrayList<Object[]> dtos;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            att.setSubSession(openSession);
            openSession.getTransaction().begin();
            dtos=att.getAllAttendance(empid,date);
            openSession.getTransaction().commit();
            return dtos;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
