package lk.ijse.payroll.business.custom.impl;

import lk.ijse.payroll.business.custom.EmpLoanBO;
import lk.ijse.payroll.dto.CustomLoanDTO;
import lk.ijse.payroll.repo.RepoFactory;
import lk.ijse.payroll.repo.custom.EmpLoansRepo;
import lk.ijse.payroll.repo.custom.EmployeeRepo;
import lk.ijse.payroll.resources.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;

public class EmpLoanImpl implements EmpLoanBO {

    EmpLoansRepo empLoan=RepoFactory.getInstance().getRepo(RepoFactory.RepoTypes.EmpLoans);

    public EmpLoanImpl() {
    }

    @Override
    public boolean getEmployeeLastLoanDate(String empId, String date) throws Exception {

        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            empLoan.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean can=empLoan.getEmployeeLastLoanDate(empId,date);
            openSession.getTransaction().commit();
            return can;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean saveLoan(String empid, String loan, String date, String installment) throws Exception {

        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            empLoan.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean save=empLoan.saveLoan(empid,loan,date,installment);
            openSession.getTransaction().commit();
            return save;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public ArrayList<CustomLoanDTO> getAllLoans() throws Exception {
        ArrayList<CustomLoanDTO> lns=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            empLoan.setSubSession(openSession);
            openSession.getTransaction().begin();
            lns=empLoan.getAllLoans();
            openSession.getTransaction().commit();
            return lns;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<CustomLoanDTO> SearchAllLoansByEmpId(String id) throws Exception {
        ArrayList<CustomLoanDTO> ids=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            empLoan.setSubSession(openSession);
            openSession.getTransaction().begin();
            ids=empLoan.SearchAllLoansByEmpId(id);
            openSession.getTransaction().commit();
            return ids;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<CustomLoanDTO> SearchAllLoansByLoanName(String name) throws Exception {
        ArrayList<CustomLoanDTO> n=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            empLoan.setSubSession(openSession);
            openSession.getTransaction().begin();
            n=empLoan.SearchAllLoansByLoanName(name);
            openSession.getTransaction().commit();
            return n;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Object[] getLoanDetails(String date, String empid) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            empLoan.setSubSession(openSession);
            openSession.getTransaction().begin();
            Object[] n=empLoan.getLoanDetails(date,empid);
            openSession.getTransaction().commit();
            return n;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
