package lk.ijse.payroll.business.custom.impl;

import lk.ijse.payroll.business.custom.LeavesBO;
import lk.ijse.payroll.dto.LeaveDTO;
import lk.ijse.payroll.dto.ShortLeaveDTO;
import lk.ijse.payroll.repo.RepoFactory;
import lk.ijse.payroll.repo.custom.LeavesRepo;
import lk.ijse.payroll.resources.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;

public class LeavesBOImpl implements LeavesBO {
    LeavesRepo leav=RepoFactory.getInstance().getRepo(RepoFactory.RepoTypes.Leaves);

    public LeavesBOImpl() {
    }

    @Override
    public boolean saveShortLeave(String emp, String date, int no) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            leav.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean update=leav.saveShortLeave(emp,date,no);
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean saveEmployeeLeave(String emp, String date, int no) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            leav.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean update=leav.saveEmployeeLeave(emp,date,no);
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public ArrayList<ShortLeaveDTO> getAllShortLeaves() throws Exception {
        ArrayList<ShortLeaveDTO> a=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            leav.setSubSession(openSession);
            openSession.getTransaction().begin();
            a=leav.getAllShortLeaves();
            openSession.getTransaction().commit();
            return a;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<LeaveDTO> getAllLeaves() throws Exception {
        ArrayList<LeaveDTO> a=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            leav.setSubSession(openSession);
            openSession.getTransaction().begin();
            a=leav.getAllLeaves();
            openSession.getTransaction().commit();
            return a;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
