package lk.ijse.payroll.business.custom.impl;

import lk.ijse.payroll.business.custom.EmpAdvanceBO;
import lk.ijse.payroll.dto.CustomAdvanceDTO;
import lk.ijse.payroll.repo.RepoFactory;
import lk.ijse.payroll.repo.custom.EmpAdvanceRepo;
import lk.ijse.payroll.resources.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;

public class EmpAdvanceBOImpl implements EmpAdvanceBO {

    EmpAdvanceRepo adv=RepoFactory.getInstance().getRepo(RepoFactory.RepoTypes.EmpAdvances);

    public EmpAdvanceBOImpl() {
    }

    @Override
    public boolean saveAdvance(String empid, String date, String amount) throws Exception{

        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            adv.setSubSession(openSession);
            openSession.getTransaction().begin();

            String yr = adv.searchyear(empid);
            String mon = adv.searchMonth(empid);
            int year=adv.searchYearByDate(date);
            int month=adv.searchMonthByDate(date);
            if (yr != null && mon != null) {
                int a = Integer.parseInt(yr);
                int b = Integer.parseInt(mon);
                System.out.println(yr+" "+mon);
                System.out.println(year+" "+month);
                if (a == month && b == year) {
                    return false;
                } else {
                    boolean saved=adv.saveAdvance(empid,date,amount);
                    openSession.getTransaction().commit();
                    return saved;
                }
            } else {
                boolean saved=adv.saveAdvance(empid,date,amount);
                openSession.getTransaction().commit();
                return saved;
            }


        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }


    }

    @Override
    public ArrayList<CustomAdvanceDTO> getAllAdvances() throws Exception {
        ArrayList<CustomAdvanceDTO> advance=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession()) {
            adv.setSubSession(openSession);
            openSession.getTransaction().begin();
            advance=adv.getAllAdvances();
            openSession.getTransaction().commit();
            return advance;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<CustomAdvanceDTO> searchAllAdvancesByDateAndEmpID(String empid, String date) throws Exception {
        ArrayList<CustomAdvanceDTO> advance=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            adv.setSubSession(openSession);
            openSession.getTransaction().begin();
            advance=adv.searchAllAdvancesByDateAndEmpID(empid,date);
            openSession.getTransaction().commit();
            return advance;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<CustomAdvanceDTO> searchAllAdvancesByDate(String date) throws Exception {
        ArrayList<CustomAdvanceDTO> advance=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            adv.setSubSession(openSession);
            openSession.getTransaction().begin();
            advance=adv.searchAllAdvancesByDate(date);
            openSession.getTransaction().commit();
            return advance;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<CustomAdvanceDTO> searchAllAdvancesByEmpId(String empid) throws Exception {
        ArrayList<CustomAdvanceDTO> advance=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            adv.setSubSession(openSession);
            openSession.getTransaction().begin();
            advance=adv.searchAllAdvancesByEmpId(empid);
            openSession.getTransaction().commit();
            return advance;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String getLastMonthAdvance(String date, String empid) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession()) {
            adv.setSubSession(openSession);
            openSession.getTransaction().begin();
            String am=adv.getLastMonthAdvance(date,empid);
            openSession.getTransaction().commit();
            return am;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
