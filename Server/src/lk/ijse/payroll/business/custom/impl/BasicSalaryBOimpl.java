package lk.ijse.payroll.business.custom.impl;

import lk.ijse.payroll.business.custom.BasicSalaryBO;
import lk.ijse.payroll.repo.RepoFactory;
import lk.ijse.payroll.repo.custom.BasicSalaryRepo;
import lk.ijse.payroll.resources.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;

public class BasicSalaryBOimpl implements BasicSalaryBO {
    BasicSalaryRepo basic=RepoFactory.getInstance().getRepo(RepoFactory.RepoTypes.BasicSalary);

    public BasicSalaryBOimpl() {
    }

    @Override
    public ArrayList<String> getAllBasicSalaryAmount() throws Exception {
        ArrayList<String> Basic=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession()) {
            basic.setSubSession(openSession);
            openSession.getTransaction().begin();
            Basic=basic.getAllBasicSalaryAmount();
            openSession.getTransaction().commit();
            return Basic;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public ArrayList<Object[]> getAllbasicSalary() throws Exception {
        ArrayList<Object[]> Basic=null;
        try (Session openSession = HibernateUtil.getSessionFactory().openSession()) {
            basic.setSubSession(openSession);
            openSession.getTransaction().begin();
            Basic=basic.getAllBasicSalary();
            openSession.getTransaction().commit();
            return Basic;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean saveBasicSalary(String text) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession()) {
            basic.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean abc=basic.saveBasicSalary( text);
            openSession.getTransaction().commit();
            return abc;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateBasic(String text, String text1) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession()) {
            basic.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean abc=basic.updateBasic( text,  text1);
            openSession.getTransaction().commit();
            return abc;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteBasic(String text) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession()) {
            basic.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean abc=basic.deleteBasic( text);
            openSession.getTransaction().commit();
            return abc;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
