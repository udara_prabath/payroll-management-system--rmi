package lk.ijse.payroll.business.custom.impl;

import lk.ijse.payroll.business.custom.PayrollBO;
import lk.ijse.payroll.repo.RepoFactory;
import lk.ijse.payroll.repo.custom.PayrollRepo;
import lk.ijse.payroll.resources.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;

public class PayrollBOImpl implements PayrollBO {
    PayrollRepo pay=RepoFactory.getInstance().getRepo(RepoFactory.RepoTypes.Payroll);
    public PayrollBOImpl() {
    }

    @Override
    public boolean savePayroll(int empid, String date, String name, double bs, String work, double not, double dot, String leaves, String shrt, double ep8, double ep12, double ep3, double loa, double adv, double bo, double gro, double de, double tot) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            pay.setSubSession(openSession);
            openSession.getTransaction().begin();
            boolean update=pay.savePayroll(empid,date,name,bs,work,not,dot,leaves,shrt,ep8,ep12,ep3,loa,adv,bo,gro,de,tot);
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public ArrayList<Object[]> getAllPayroll() throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            pay.setSubSession(openSession);
            openSession.getTransaction().begin();
            ArrayList<Object[]> update=pay.getAllPayroll();
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    @Override
    public ArrayList<Object[]> getAllPayroll(String id) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            pay.setSubSession(openSession);
            openSession.getTransaction().begin();
            ArrayList<Object[]> update=pay.getAllPayroll(id);
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    @Override
    public ArrayList<Object[]> getAllPayrollbyDate(String id) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            pay.setSubSession(openSession);
            openSession.getTransaction().begin();
            ArrayList<Object[]> update=pay.getAllPayrollbyDate(id);
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    @Override
    public ArrayList<Object[]> getAllPayroll(String id,String date) throws Exception {
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            pay.setSubSession(openSession);
            openSession.getTransaction().begin();
            ArrayList<Object[]> update=pay.getAllPayroll(id,date);
            openSession.getTransaction().commit();
            return update;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
