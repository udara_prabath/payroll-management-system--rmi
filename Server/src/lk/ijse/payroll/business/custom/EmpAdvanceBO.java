package lk.ijse.payroll.business.custom;

import lk.ijse.payroll.business.SuperBO;
import lk.ijse.payroll.dto.CustomAdvanceDTO;

import java.util.ArrayList;

public interface EmpAdvanceBO extends SuperBO {
    boolean saveAdvance(String empid, String date, String amount)throws Exception;

    ArrayList<CustomAdvanceDTO> getAllAdvances()throws Exception;

    ArrayList<CustomAdvanceDTO> searchAllAdvancesByDateAndEmpID(String empid, String date)throws Exception;

    ArrayList<CustomAdvanceDTO> searchAllAdvancesByDate(String date)throws Exception;

    ArrayList<CustomAdvanceDTO> searchAllAdvancesByEmpId(String empid)throws Exception;

    String getLastMonthAdvance(String date, String empid)throws Exception;
}
