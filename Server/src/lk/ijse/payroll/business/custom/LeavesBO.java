package lk.ijse.payroll.business.custom;

import lk.ijse.payroll.business.SuperBO;
import lk.ijse.payroll.dto.LeaveDTO;
import lk.ijse.payroll.dto.ShortLeaveDTO;

import java.util.ArrayList;

public interface LeavesBO extends SuperBO {
    boolean saveShortLeave(String emp, String date, int no)throws Exception;
    boolean saveEmployeeLeave(String emp, String date, int no)throws Exception;

    ArrayList<ShortLeaveDTO> getAllShortLeaves()throws Exception;
    ArrayList<LeaveDTO> getAllLeaves()throws Exception;
}
