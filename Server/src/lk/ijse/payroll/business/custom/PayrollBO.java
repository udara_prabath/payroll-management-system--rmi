package lk.ijse.payroll.business.custom;

import lk.ijse.payroll.business.SuperBO;

import java.util.ArrayList;

public interface PayrollBO extends SuperBO {
    boolean savePayroll(int empid, String date, String name, double bs, String work, double not, double dot, String leaves, String shrt, double ep8, double ep12, double ep3, double loa, double adv, double bo, double gro, double de, double tot)throws Exception;

    ArrayList<Object[]> getAllPayroll()throws Exception;
    ArrayList<Object[]> getAllPayroll(String id)throws Exception;
    ArrayList<Object[]> getAllPayrollbyDate(String id)throws Exception;
    public ArrayList<Object[]> getAllPayroll(String id,String date) throws Exception;
}
