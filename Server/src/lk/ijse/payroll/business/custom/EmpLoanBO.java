package lk.ijse.payroll.business.custom;

import lk.ijse.payroll.business.SuperBO;
import lk.ijse.payroll.dto.CustomLoanDTO;

import java.util.ArrayList;

public interface EmpLoanBO extends SuperBO {
    boolean getEmployeeLastLoanDate(String empId, String date)throws Exception;

    boolean saveLoan(String empid, String loan, String date, String installment)throws Exception;

    ArrayList<CustomLoanDTO> getAllLoans()throws Exception;

    ArrayList<CustomLoanDTO> SearchAllLoansByEmpId(String id)throws Exception;

    ArrayList<CustomLoanDTO> SearchAllLoansByLoanName(String name)throws Exception;

    Object[] getLoanDetails(String date, String empid)throws Exception;
}
