package lk.ijse.payroll.business.custom;

import lk.ijse.payroll.business.SuperBO;

import java.util.ArrayList;

public interface LoansBO extends SuperBO {
    ArrayList<String> getAllLoanNames()throws Exception;

    String searchLoanAmount(String name)throws Exception;

    ArrayList<Object[]> getAllLoans()throws Exception;

    boolean saveNeLoan(String text, String text1)throws Exception;

    boolean updateLoan(String text, String text1, String text2)throws Exception;

    boolean deleteLoan(String text)throws Exception;
}
