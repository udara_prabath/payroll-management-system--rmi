package lk.ijse.payroll.entity;

import javax.persistence.*;

@Entity
@Table(name = "PayRoll")
public class PayRoll {
    @Id
    @GeneratedValue
    private int payid;
    @ManyToOne()
    @JoinColumn(name = "empid")
    private Employee employee;
    private String date;
    private String name;
    private double basicSalary;
    private String Day_worked;
    private double Normal_OT;
    private double doublr_ot;
    private String total_leaves;
    private String short_leaves;
    private double epf_8;
    private double epf_12;
    private double etf_3;
    private double loan;
    private double advance;
    private double Bonus;
    private double Gross_Salary;
    private double Total_Deduction;
    private double Salary;

    public PayRoll() {
    }

    public PayRoll(Employee employee, String date, String name, double basicSalary, String day_worked, double normal_OT, double doublr_ot, String total_leaves, String short_leaves, double epf_8, double epf_12, double etf_3, double loan, double advance, double bonus, double gross_Salary, double total_Deduction, double salary) {
        this.employee = employee;
        this.date = date;
        this.name = name;
        this.basicSalary = basicSalary;
        Day_worked = day_worked;
        Normal_OT = normal_OT;
        this.doublr_ot = doublr_ot;
        this.total_leaves = total_leaves;
        this.short_leaves = short_leaves;
        this.epf_8 = epf_8;
        this.epf_12 = epf_12;
        this.etf_3 = etf_3;
        this.loan = loan;
        this.advance = advance;
        Bonus = bonus;
        Gross_Salary = gross_Salary;
        Total_Deduction = total_Deduction;
        Salary = salary;
    }

    public int getPayid() {
        return payid;
    }

    public void setPayid(int payid) {
        this.payid = payid;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(double basicSalary) {
        this.basicSalary = basicSalary;
    }

    public String getDay_worked() {
        return Day_worked;
    }

    public void setDay_worked(String day_worked) {
        Day_worked = day_worked;
    }

    public double getNormal_OT() {
        return Normal_OT;
    }

    public void setNormal_OT(double normal_OT) {
        Normal_OT = normal_OT;
    }

    public double getDoublr_ot() {
        return doublr_ot;
    }

    public void setDoublr_ot(double doublr_ot) {
        this.doublr_ot = doublr_ot;
    }

    public String getTotal_leaves() {
        return total_leaves;
    }

    public void setTotal_leaves(String total_leaves) {
        this.total_leaves = total_leaves;
    }

    public String getShort_leaves() {
        return short_leaves;
    }

    public void setShort_leaves(String short_leaves) {
        this.short_leaves = short_leaves;
    }

    public double getEpf_8() {
        return epf_8;
    }

    public void setEpf_8(double epf_8) {
        this.epf_8 = epf_8;
    }

    public double getEpf_12() {
        return epf_12;
    }

    public void setEpf_12(double epf_12) {
        this.epf_12 = epf_12;
    }

    public double getEtf_3() {
        return etf_3;
    }

    public void setEtf_3(double etf_3) {
        this.etf_3 = etf_3;
    }

    public double getLoan() {
        return loan;
    }

    public void setLoan(double loan) {
        this.loan = loan;
    }

    public double getAdvance() {
        return advance;
    }

    public void setAdvance(double advance) {
        this.advance = advance;
    }

    public double getBonus() {
        return Bonus;
    }

    public void setBonus(double bonus) {
        Bonus = bonus;
    }

    public double getGross_Salary() {
        return Gross_Salary;
    }

    public void setGross_Salary(double gross_Salary) {
        Gross_Salary = gross_Salary;
    }

    public double getTotal_Deduction() {
        return Total_Deduction;
    }

    public void setTotal_Deduction(double total_Deduction) {
        Total_Deduction = total_Deduction;
    }

    public double getSalary() {
        return Salary;
    }

    public void setSalary(double salary) {
        Salary = salary;
    }
}
