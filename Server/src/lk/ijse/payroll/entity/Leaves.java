package lk.ijse.payroll.entity;

import javax.persistence.*;

@Entity
@Table(name = "leaves")
public class Leaves {
    @Id
    @GeneratedValue
    private int lid;
    @ManyToOne()
    @JoinColumn(name = "empid")
    private Employee employee;
    private String date;
    private int no_of_days;

    public Leaves() {

    }

    public Leaves(Employee employee, String date, int no_of_days) {
        this.employee = employee;
        this.date = date;
        this.no_of_days = no_of_days;
    }

    public int getLid() {
        return lid;
    }

    public void setLid(int lid) {
        this.lid = lid;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNo_of_days() {
        return no_of_days;
    }

    public void setNo_of_days(int no_of_days) {
        this.no_of_days = no_of_days;
    }
}
