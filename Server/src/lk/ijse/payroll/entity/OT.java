package lk.ijse.payroll.entity;

import javax.persistence.*;

@Entity
@Table(name = "OT")
public class OT {
    @Id
    @GeneratedValue
    private int otid;

    public OT(Employee employee, String date, int normal_ot, int double_ot, double amount) {
        this.employee = employee;
        this.date = date;
        this.normal_ot = normal_ot;
        this.double_ot = double_ot;
        this.amount = amount;
    }

    @ManyToOne()
    @JoinColumn(name = "empid")
    private Employee employee;
    private String date;
    private int normal_ot;
    private int double_ot;
    private double amount;

    public OT() {
    }


    public int getOtid() {
        return otid;
    }

    public void setOtid(int otid) {
        this.otid = otid;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNormal_ot() {
        return normal_ot;
    }

    public void setNormal_ot(int normal_ot) {
        this.normal_ot = normal_ot;
    }

    public int getDouble_ot() {
        return double_ot;
    }

    public void setDouble_ot(int double_ot) {
        this.double_ot = double_ot;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
