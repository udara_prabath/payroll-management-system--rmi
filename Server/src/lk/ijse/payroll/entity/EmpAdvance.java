package lk.ijse.payroll.entity;

import javax.persistence.*;

@Entity
@Table(name = "EmpAdvance")
public class EmpAdvance {
    @Id
    @GeneratedValue
    private int EAdId;
    @ManyToOne()
    @JoinColumn(name = "empid")
    private Employee employee;
    private String month;
    private String year;
    private double amount;

    public EmpAdvance() {
    }

    public EmpAdvance(Employee employee, String month, String year, double amount) {
        this.employee = employee;
        this.month = month;
        this.year = year;
        this.amount = amount;
    }

    public int getEAdId() {
        return EAdId;
    }

    public void setEAdId(int EAdId) {
        this.EAdId = EAdId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
