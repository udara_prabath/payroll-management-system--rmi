package lk.ijse.payroll.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Loan")
public class Loan {
    @Id
    @GeneratedValue
    private int loanid;
    private String loanName;
    private double amount;
    @OneToMany(mappedBy = "loan",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<EmpLoan> empLoans=new ArrayList<>();

    public Loan() {
    }

    public Loan(String loanName, double amount, List<EmpLoan> empLoans) {
        this.loanName = loanName;
        this.amount = amount;
        this.empLoans = empLoans;
    }

    public int getLoanid() {
        return loanid;
    }

    public void setLoanid(int loanid) {
        this.loanid = loanid;
    }

    public String getLoanName() {
        return loanName;
    }

    public void setLoanName(String loanName) {
        this.loanName = loanName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<EmpLoan> getEmpLoans() {
        return empLoans;
    }

    public void setEmpLoans(List<EmpLoan> empLoans) {
        this.empLoans = empLoans;
    }
}
