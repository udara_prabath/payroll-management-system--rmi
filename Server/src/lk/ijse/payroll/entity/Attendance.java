package lk.ijse.payroll.entity;

import javax.persistence.*;

@Entity
@Table(name = "attendance")
public class Attendance {
    @Id
    @GeneratedValue
    private int attid;
    @ManyToOne()
    @JoinColumn(name = "empid")
    private Employee employee;
    private String day;
    private String timein;
    private String timeout;
    private int worked_hours;

    public Attendance() {
    }

    public Attendance(Employee employee, String day, String timein, String timeout, int worked_hours) {
        this.employee = employee;
        this.day = day;
        this.timein = timein;
        this.timeout = timeout;
        this.worked_hours = worked_hours;
    }

    public int getAttid() {
        return attid;
    }

    public void setAttid(int attid) {
        this.attid = attid;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTimein() {
        return timein;
    }

    public void setTimein(String timein) {
        this.timein = timein;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public int getWorked_hours() {
        return worked_hours;
    }

    public void setWorked_hours(int worked_hours) {
        this.worked_hours = worked_hours;
    }
}
