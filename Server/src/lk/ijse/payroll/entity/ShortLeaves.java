package lk.ijse.payroll.entity;

import javax.persistence.*;

@Entity
@Table(name = "ShortLeaves")
public class ShortLeaves {
    @Id
    @GeneratedValue
    private int slid;
    @ManyToOne()
    @JoinColumn(name = "empid")
    private Employee employee;
    private String date;
    private int duration;

    public ShortLeaves() {

    }

    public ShortLeaves(Employee employee, String date, int duration) {
        this.employee = employee;
        this.date = date;
        this.duration = duration;
    }

    public int getSlid() {
        return slid;
    }

    public void setSlid(int slid) {
        this.slid = slid;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
