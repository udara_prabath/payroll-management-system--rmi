package lk.ijse.payroll.entity;

import javax.persistence.*;

@Entity
@Table(name = "EmpLoan")
public class EmpLoan {
    @Id
    @GeneratedValue
    private int elid;
    @ManyToOne()
    @JoinColumn(name = "empid")
    private Employee employee;
    @ManyToOne()
    @JoinColumn(name = "loanid")
    private Loan loan;
    private String date;
    private double install;

    public EmpLoan() {
    }

    public EmpLoan(Employee employee, Loan loan, String date, double install) {
        this.employee = employee;
        this.loan = loan;
        this.date = date;
        this.install = install;
    }

    public int getElid() {
        return elid;
    }

    public void setElid(int elid) {
        this.elid = elid;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getInstall() {
        return install;
    }

    public void setInstall(double install) {
        this.install = install;
    }
}
