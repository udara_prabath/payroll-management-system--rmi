package lk.ijse.payroll.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Employee")
public class Employee {
    @Id
    @GeneratedValue
    private int empId;
    private String empName;
    private String address;
    private String gender;
    private String nic;
    private String DOB;
    private int tel;
    private String joinDate;
    @ManyToOne()
    @JoinColumn(name = "deid")
    private Designation designation;
    private int totLeaves;
    @ManyToOne()
    @JoinColumn(name = "bsid")
    private BasicSalary basicSalary;
    private String attendance;
    private String payroll;

    @OneToMany(mappedBy = "employee",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<EmpLoan> empLoans=new ArrayList<>();
    @OneToMany(mappedBy = "employee",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<EmpAdvance> empAdvances=new ArrayList<>();
    @OneToMany(mappedBy = "employee",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<Attendance> attendances=new ArrayList<>();
    @OneToMany(mappedBy = "employee",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<OT> ots=new ArrayList<>();
    @OneToMany(mappedBy = "employee",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<Leaves> leaves=new ArrayList<>();
    @OneToMany(mappedBy = "employee",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<ShortLeaves> shortLeaves=new ArrayList<>();
    @OneToMany(mappedBy = "employee",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<PayRoll> payRolls=new ArrayList<>();

    public Employee() {
    }

    public Employee(String empName, String address, String gender, String nic, String DOB, int tel, String joinDate, Designation designation, int totLeaves, BasicSalary basicSalary, String attendance, String payroll, List<EmpLoan> empLoans, List<EmpAdvance> empAdvances, List<Attendance> attendances, List<OT> ots, List<Leaves> leaves, List<ShortLeaves> shortLeaves, List<PayRoll> payRolls) {
        this.empName = empName;
        this.address = address;
        this.gender = gender;
        this.nic = nic;
        this.DOB = DOB;
        this.tel = tel;
        this.joinDate = joinDate;
        this.designation = designation;
        this.totLeaves = totLeaves;
        this.basicSalary = basicSalary;
        this.attendance = attendance;
        this.payroll = payroll;
        this.empLoans = empLoans;
        this.empAdvances = empAdvances;
        this.attendances = attendances;
        this.ots = ots;
        this.leaves = leaves;
        this.shortLeaves = shortLeaves;
        this.payRolls = payRolls;
    }

    public List<EmpAdvance> getEmpAdvances() {
        return empAdvances;
    }

    public void setEmpAdvances(List<EmpAdvance> empAdvances) {
        this.empAdvances = empAdvances;
    }

    public List<Attendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(List<Attendance> attendances) {
        this.attendances = attendances;
    }

    public List<OT> getOts() {
        return ots;
    }

    public void setOts(List<OT> ots) {
        this.ots = ots;
    }

    public List<Leaves> getLeaves() {
        return leaves;
    }

    public void setLeaves(List<Leaves> leaves) {
        this.leaves = leaves;
    }

    public List<ShortLeaves> getShortLeaves() {
        return shortLeaves;
    }

    public void setShortLeaves(List<ShortLeaves> shortLeaves) {
        this.shortLeaves = shortLeaves;
    }

    public List<PayRoll> getPayRolls() {
        return payRolls;
    }

    public void setPayRolls(List<PayRoll> payRolls) {
        this.payRolls = payRolls;
    }

    public List<EmpLoan> getEmpLoans() {
        return empLoans;
    }

    public void setEmpLoans(List<EmpLoan> empLoans) {
        this.empLoans = empLoans;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public int getTel() {
        return tel;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    public int getTotLeaves() {
        return totLeaves;
    }

    public void setTotLeaves(int totLeaves) {
        this.totLeaves = totLeaves;
    }

    public BasicSalary getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(BasicSalary basicSalary) {
        this.basicSalary = basicSalary;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getPayroll() {
        return payroll;
    }

    public void setPayroll(String payroll) {
        this.payroll = payroll;
    }
}
