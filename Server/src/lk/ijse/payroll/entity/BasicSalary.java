package lk.ijse.payroll.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BasicSalary")
public class BasicSalary {
    @Id
    @GeneratedValue
    private int bsid;
    private double amount;
    @OneToMany(mappedBy = "basicSalary",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<Employee> employees=new ArrayList<>();

    public BasicSalary() {

    }

    public BasicSalary(double amount, List<Employee> employees) {
        this.amount = amount;
        this.employees = employees;
    }

    public int getBsid() {
        return bsid;
    }

    public void setBsid(int bsid) {
        this.bsid = bsid;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
