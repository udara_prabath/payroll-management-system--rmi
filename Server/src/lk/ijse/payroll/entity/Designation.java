package lk.ijse.payroll.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Designation")
public class Designation {
    @Id
    @GeneratedValue
    private int deid;
    private String deName;
    @OneToMany(mappedBy = "designation",cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private List<Employee>employees=new ArrayList<>();

    public Designation() {
    }

    public Designation(String deName, List<Employee> employees) {
        this.deName = deName;
        this.employees = employees;
    }

    public int getDeid() {
        return deid;
    }

    public void setDeid(int deid) {
        this.deid = deid;
    }

    public String getDeName() {
        return deName;
    }

    public void setDeName(String deName) {
        this.deName = deName;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
