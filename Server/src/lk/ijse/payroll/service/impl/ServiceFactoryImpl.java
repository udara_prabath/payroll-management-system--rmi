package lk.ijse.payroll.service.impl;

import lk.ijse.payroll.service.ServiceFactory;
import lk.ijse.payroll.service.SuperService;
import lk.ijse.payroll.service.custom.impl.AccountantServiceImpl;
import lk.ijse.payroll.service.custom.impl.AdminServiceImpl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ServiceFactoryImpl extends UnicastRemoteObject implements ServiceFactory {

    private static ServiceFactoryImpl serviceFactory;

    private ServiceFactoryImpl() throws RemoteException {
    }

    public static ServiceFactoryImpl getInstace() throws Exception {
        if (serviceFactory == null) {
            serviceFactory = new ServiceFactoryImpl();
        }
        return serviceFactory;

    }

    @Override
    public <T extends SuperService> T getService(ServiceTypes types) throws Exception {
        switch (types) {
            case Admin:
                return (T) new AdminServiceImpl();
            case Accountant:
                return (T) new AccountantServiceImpl();
            default:
                return null;

        }
    }
}
