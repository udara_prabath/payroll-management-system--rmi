package lk.ijse.payroll.service.custom.impl;

import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.*;
import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.enums.ReportTypes;
import lk.ijse.payroll.observer.Observer;
import lk.ijse.payroll.reservation.ReservationUtil;
import lk.ijse.payroll.resources.HibernateUtil;
import lk.ijse.payroll.service.custom.AdminService;
import lk.ijse.payroll.util.ReportsGenarator;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class AdminServiceImpl extends UnicastRemoteObject implements AdminService {
    private static ArrayList<Observer>observers=new ArrayList<>();
    private static ReservationUtil adminReservation = new ReservationUtil();
    public ReportsGenarator rp=new ReportsGenarator();

    EmpAdvanceBO advance=BOFactory.getInstance().getBO(BOFactory.BOTypes.EmpAdvance);
    BasicSalaryBO basicSalary=BOFactory.getInstance().getBO(BOFactory.BOTypes.BasicSalary);
    DesignationBO designation=BOFactory.getInstance().getBO(BOFactory.BOTypes.Designation);
    EmployeeBO employee=BOFactory.getInstance().getBO(BOFactory.BOTypes.Employee);
    LoansBO loans=BOFactory.getInstance().getBO(BOFactory.BOTypes.Loans);
    EmpLoanBO empLoan=BOFactory.getInstance().getBO(BOFactory.BOTypes.EmpLoan);
    AttendanceBO att=BOFactory.getInstance().getBO(BOFactory.BOTypes.Attendance);
    LeavesBO leav=BOFactory.getInstance().getBO(BOFactory.BOTypes.leaves);
    PayrollBO pay=BOFactory.getInstance().getBO(BOFactory.BOTypes.Payroll);


    public AdminServiceImpl() throws RemoteException {
    }

    @Override
    public boolean register(Observer observer) throws Exception {
        observers.add(observer);
        System.out.println("Admin "+getClientHost()+" Connected");
        return true;
    }
    @Override
    public boolean unregister(Observer observer) throws Exception {
        observers.remove(observer);
        System.out.println("Admin "+getClientHost()+" Disconnected");
        return true;
    }
    @Override
    public void notyfyAllObservers() throws Exception {
        for (Observer obs : observers) {
            new Thread(()->{
                try {
                    obs.update();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();

        }
    }
    public boolean reserve(Object id) throws Exception {
        return adminReservation.reserve(id,this);
    }
    public boolean release(Object id) throws Exception {
        return adminReservation.release(id,this);
    }


    @Override
    public ArrayList<String> getAllBasicSalaryAmount() throws Exception {
        return basicSalary.getAllBasicSalaryAmount();
    }

    @Override
    public ArrayList<String> getAllDesignation() throws Exception {
        return designation.getAllDesignation();
    }

    @Override
    public ArrayList<String> getAllLoanNames() throws Exception {
        return loans.getAllLoanNames();
    }

    @Override
    public ArrayList<String> getAllEmployeeIDs() throws Exception {
        return employee.getAllEmployeeIDs();
    }
    public ArrayList<String> getAllEmployeeNames() throws Exception {
        return employee.getAllEmployeeName();
    }

    @Override
    public ArrayList<String> getAllEmployeeNamesForAtt() throws Exception {
        return employee.getAllEmployeeNamesForAtt();
    }

    @Override
    public ArrayList<String> getAllEmployeeNamesForpay() throws Exception {
        return employee.getAllEmployeeNamesForpay();
    }

    @Override
    public boolean saveEmployee(String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, int i, String basicSalary) throws Exception {
        return employee.saveEmployee(name,address,gender,nic,dob,tel,joindate,designation,i,basicSalary);
    }

    @Override
    public boolean updateEmployee(String id, String name, String address, String gender, String nic, String dob, String tel, String joindate, String designation, String basicSalary) throws Exception {
        return employee.UpdateEmployee(id,name,address,gender,nic,dob,tel,joindate,designation,basicSalary);
    }

    @Override
    public CustomEmployeeDTO searchEmployee(String id) throws Exception {
        return employee.searchEmployee(id);
    }

    @Override
    public boolean deleteEmployee(String id) throws Exception {
        return employee.deleteEmployee(id);
    }

    @Override
    public ArrayList<CustomEmployeeDTO> getAllEmployeesForEmployeeReview() throws Exception {
        return employee.searchEmployee();
    }

    @Override
    public ArrayList<CustomEmployeeDTO> getAllEmployeesForEmployeeReviewByID(String id) throws Exception {
        return employee.searchCustomEmployeeByID(id);
    }

    @Override
    public ArrayList<CustomEmployeeDTO> getAllEmployeesForEmployeeReviewByDesignation(String des) throws Exception {
        return employee.searchCustomEmployeeBydes(des);
    }

    @Override
    public String searchLoan(String name) throws Exception {
        return loans.searchLoanAmount(name);
    }

    @Override
    public boolean checkAvailabilityForLoan(String empId, String date) throws Exception {
        return empLoan.getEmployeeLastLoanDate(empId,date);
    }

    @Override
    public boolean saveLoan(String empid, String loan, String date, String installment) throws Exception {
        return empLoan.saveLoan(empid,loan,date,installment);
    }

    @Override
    public boolean checkAvailabilityForAdvance(String empid, String date, String amount) throws Exception {
        return advance.saveAdvance(empid,date,amount);
    }

    @Override
    public ArrayList<CustomLoanDTO> getAllLoanForAllLoans() throws Exception {
        return empLoan.getAllLoans();
    }

    @Override
    public ArrayList<CustomAdvanceDTO> getAllAdvancesForAllAdvances() throws Exception {
        return advance.getAllAdvances();
    }

    @Override
    public ArrayList<CustomLoanDTO> getAllLoanForAllLoansByID(String id) throws Exception {
        return empLoan.SearchAllLoansByEmpId(id);
    }

    @Override
    public ArrayList<CustomLoanDTO> getAllLoanForAllLoansByName(String name) throws Exception {
        return empLoan.SearchAllLoansByLoanName(name);
    }

    @Override
    public ArrayList<CustomAdvanceDTO> searchAdvanceByEmpidAndDate(String empid, String date) throws Exception {
        return advance.searchAllAdvancesByDateAndEmpID(empid,date);
    }

    @Override
    public ArrayList<CustomAdvanceDTO> searchAdvanceByDate(String date) throws Exception {
        return advance.searchAllAdvancesByDate(date);
    }

    @Override
    public ArrayList<CustomAdvanceDTO> searchAdvanceByEmpID(String empid) throws Exception {
        return advance.searchAllAdvancesByEmpId(empid);
    }

    @Override
    public boolean markAttendanceIN(String name, String date, String time, String st) throws Exception {
        return att.markAttendanceIN(name,date,time,st);
    }

    @Override
    public boolean markAttendanceOUT(String name, String date, String time, String day) throws Exception {
        return att.markAttendanceOUT(name,date,time,day);
    }

    @Override
    public boolean saveShortLeave(String emp, String date, int no) throws Exception {
        return leav.saveShortLeave(emp,date,no);
    }

    @Override
    public boolean saveEmployeeLeave(String emp, String date, int no) throws Exception {
        return leav.saveEmployeeLeave(emp,date,no);
    }

    @Override
    public ArrayList<ShortLeaveDTO> getAllShortLeaves() throws Exception {
        return leav.getAllShortLeaves();
    }

    @Override
    public ArrayList<LeaveDTO> getAllLeaves() throws Exception {
        return leav.getAllLeaves();
    }

    @Override
    public String[] getEmpNameAndDesignation(String empid) throws Exception {
        return employee.getEmpNameAndDesignation(empid);
    }

    @Override
    public ArrayList<Object[]> getAllAttendance(String empid, String date) throws Exception {
        return att.getAllAttendance(empid,date);
    }

    @Override
    public String getLastMonthAdvance(String date, String empid) throws Exception {
        return advance.getLastMonthAdvance(date,empid);
    }

    @Override
    public Object[] getLoanDetails(String date, String empid) throws Exception {
        return empLoan.getLoanDetails(date,empid);
    }

    @Override
    public Object[] calculateSalary(int not, int dot, int leaves, int shrt, double bs, double ep8, double loa, double adv, double bo) throws Exception {
        return employee.calculateSalary(not,dot,leaves,shrt,bs,ep8,loa,adv,bo);
    }

    @Override
    public boolean savePayroll(int empid, String date, String name, double bs, String work, double not, double dot, String leaves, String shrt, double ep8, double ep12, double ep3, double loa, double adv, double bo, double gro, double de, double tot) throws Exception {
        return pay.savePayroll(empid,date,name,bs,work,not,dot,leaves,shrt,ep8,ep12,ep3,loa,adv,bo,gro,de,tot);
    }

    @Override
    public ArrayList<Object[]> getAllPayroll() throws Exception {
        return pay.getAllPayroll();
    }
    @Override
    public ArrayList<Object[]> searchPayroll(String id) throws Exception {
        return pay.getAllPayroll(id);
    }
    @Override
    public ArrayList<Object[]> searchPayrollbyDate(String id) throws Exception {
        return pay.getAllPayrollbyDate(id);
    }

    @Override
    public ArrayList<Object[]> getAllBasicSalary() throws Exception {
        return basicSalary.getAllbasicSalary();
    }

    @Override
    public ArrayList<Object[]> getAllDesignatinos() throws Exception {
        return designation.getAllDes();
    }

    @Override
    public ArrayList<Object[]> getAllLoans() throws Exception {
        return loans.getAllLoans();
    }

    @Override
    public boolean saveBasicSalary(String text) throws Exception {
        return basicSalary.saveBasicSalary( text);
    }

    @Override
    public boolean updateBasic(String text, String text1) throws Exception {
        return basicSalary.updateBasic( text,  text1);
    }

    @Override
    public boolean saveDesignation(String text) throws Exception {
        return designation.saveDesignation( text);
    }

    @Override
    public boolean updateDesignation(String text, String text1) throws Exception {
        return designation.updateDesignation( text,  text1);
    }

    @Override
    public boolean saveNeLoan(String text, String text1) throws Exception {
        return loans.saveNeLoan( text,  text1);
    }

    @Override
    public boolean updateLoan(String text, String text1, String text2) throws Exception {
        return loans.updateLoan( text,  text1,  text2);
    }

    @Override
    public boolean deleteBasic(String text) throws Exception {
        return basicSalary.deleteBasic( text);
    }

    @Override
    public boolean deleteDesignatino(String text) throws Exception {
        return designation.deleteDesignatino( text);
    }

    @Override
    public boolean deleteLoan(String text) throws Exception {
        return loans.deleteLoan( text);
    }

    @Override
    public ArrayList<Object[]> searchPayroll(String id,String date) throws Exception {
        return pay.getAllPayroll(id,date);
    }

    @Override
    public JasperPrint getReport(ReportTypes reportType, Object... param) throws Exception{
        try (Session openSession = HibernateUtil.getSessionFactory().openSession();) {
            openSession.getTransaction().begin();
            rp.setSubSession(openSession);
            JasperPrint report = rp.getReport(reportType, param);
            openSession.getTransaction().commit();
            return report;
        } catch (HibernateException ex) {
            ex.printStackTrace();
            return null;
        }

    }


}
